@extends('layouts.member')

@section('content')

    @if($errors->any())
        @php toast()->error('Falha ao modificar responsavel.', 'Error'); @endphp
    @endif

    <section class="section">
        <div class="section-header mb-10">
            <span class="fasfa-user"> </span>
            <h1> Perfil do responsável</h1>
        </div>

        <div class="section-body mt-4">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    {!! Form::model($model, ['method' => 'PUT',
                                             'novalidate',
                                             'role' =>'form',
                                             'class' => 'form',
                                             'enctype' => 'multipart/form-data',
                                             'route' => ['guardians.accounts.update', 'id' => $model->id]
                                             ]) !!}

                    <div class="card">
                        <div class="card-body">
                            {{ Form::hidden('user_id', $model->user_id) }}
                            {{ Form::hidden('user[subsidiary_id]', $model->user->subsidiary_id) }}
                            {{ Form::hidden('user[active]', $model->user->active) }}
                            @include('member.accounts._personal')
                            @include('member.accounts._access')
                            @include('member.accounts._address')
                            @include('member.accounts._files', ['model' => 'responsável'])
                        </div>
                        <div class="card-footer">
                            <div class="form-group mb-0">
                                <button type="submit"
                                        class="btn btn-lg btn-success btn-icon float-right"
                                        title="Atualizar dados"><i
                                            class="fas fa-check"></i>
                                    Salvar
                                </button>
                                <a href="{{ route('guardians.dashboard.index') }}"
                                   class="btn btn-danger btn-lg btn-icon float-right mr-2"
                                   title="Voltar"><i class="fas fa-angle-left"></i>
                                    Voltar
                                </a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

@endsection
