@extends('layouts.member')

@section('content')

    <section class="section">
        <div class="section-body mt-4">
            <div class="row">
                @include('member.dashboard._messages')
            </div>

            @if($dependents->count() > 0)
                <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Dependentes</h4>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-pills" id="myTab" role="tablist">
                                @foreach($dependents as $student)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $loop->index == 0 ? 'active': '' }}"
                                           id="tab-{{ $student->id }}"
                                           data-toggle="tab"
                                           href="#content-{{ $student->id }}"
                                           role="tab"
                                           aria-controls="content-{{ $student->id }}"
                                           aria-selected="false">{{ $student->user->nickname }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                @foreach($dependents as $student)
                                    <div class="tab-pane fade {{ $loop->index == 0 ? 'active show': '' }}"
                                         id="content-{{ $student->id }}"
                                         role="tabpanel"
                                         aria-labelledby="contact-tab">
                                        <div class="row bg-secondary pt-4 rounded">
                                            <div class="col-lg-6 col-sm-12">
                                                @if($student->registration->frequencies->count() > 0)
                                                    <div class="card shadow">
                                                        <div class="card-header">
                                                            <h4>Treinos da
                                                                semana</h4>
                                                        </div>
                                                        <div class="card-body mb-0 p-2">
                                                            <ul class="list-group">
                                                                @foreach($student->registration->frequencies->reverse() as $frequency)
                                                                    <li class="list-group-item">
                                                                        {{ $frequency->classroom->name }}
                                                                        <spam class="float-right">{{  $frequency->date->format('d/m H:i') }}</spam>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>

                                            <div class="col-lg-6 col-sm-12">
                                                @if($student->registration->installments->count() > 0)
                                                    <div class="card shadow">
                                                        <div class="card-header">
                                                            <h4>Mensalidades</h4>
                                                        </div>
                                                        <div class="card-body mb-0 p-2">
                                                            <ul class="list-group">
                                                                @foreach($student->registration->installments->reverse() as $installment)
                                                                    <li class="list-group-item" data-id="{{ $installment->id }}">
                                                                        {{ $installment->due_date->format('d/m/Y') }} - {{ money_parse_by_decimal($installment->amount, 'BRL') }} {!! installmentPaymentBadge($installment) !!}
                                                                        <span class="float-right ">
                                                                            @if(!empty($installment->charge_link) && empty($installment->payment_date))
                                                                                    <a href="{{ $installment->charge_link }}" target="_blank">
                                                                                        <i class="fas fa-file-invoice"></i>
                                                                                    </a>
                                                                            @else
                                                                                <i class="fas fa-ban"></i>
                                                                            @endif
                                                                        </span>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
@endsection


