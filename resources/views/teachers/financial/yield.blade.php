@extends('layouts.member')

@section('content')
    <section class="section">
        <div class="section-header mb-4">
            <h1>Rendimentos</h1>
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'teachers.financial.yield', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-md-1 col-sm-12 mb-0 form-group">
                                {!! Form::label('date_range_due_date', 'Data Vencimento: ', ['class' => 'label']) !!}
                            </div>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                {{ Form::text('date_range_due_date', '', ['class' => 'form-control js-daterangepicker']) }}
                            </div>
                            <div class="col-md-1 col-sm-12 mb-0 form-group">
                                {!! Form::label('date_range_payment', 'Data Pagamento: ', ['class' => 'label']) !!}
                            </div>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                {{ Form::text('date_range_payment', '', ['class' => 'form-control js-daterangepicker']) }}
                            </div>
                            <div class="col-md-4 col-sm-12 mb-0 form-group">
                                {{ Form::select('classroom_id[]', $classrooms, null, ['class' => 'form-control', 'title' => 'Turmas', 'multiple' => 'multiple']) }}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de Rendimentos Por Périodo</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-a">
                                        <th>Status</th>
                                        <th>Aluno</th>
                                        <th>Vencimento</th>
                                        <th>Data Pagamento</th>
                                        <th>% Split</th>
                                        <th>Valor Parcela</th>
                                        <th>Valor Recebido</th>
                                    </tr>
                                    @php
                                        $registros = 0;
                                        $totalParcela = 0;
                                        $totalPago = 0;
                                        $totalReceber = 0;
                                        $totalRecebido = 0;
                                    @endphp
                                    @forelse($yields as $yield)

                                    @php
                                        $splitPercent = $yield->classroom_teacher_split_value;
                                        $recebido = !empty($yield->payment_date) ? ($yield->amount_paid - 3) * ($splitPercent / 100) : 0;

                                        $registros++;
                                    @endphp

                                        <tr class="td-border">
                                            <td width="1" >
                                                @if(empty($yield->payment_date))
                                                    @if(today()->gt($yield->due_date))
                                                        <i title="Atrasado"
                                                            class="fas fa-exclamation-triangle text-danger"></i>
                                                    @else
                                                        <i title="Pedente"
                                                            class="fas fa-question-circle text-warning"></i>
                                                    @endif
                                                @else
                                                    <i title="Pago" class="fas fa-check-circle text-success"></i>
                                                @endif
                                            </td>
                                            <td width="1" >{{ $yield->student}}</td>
                                            <td width="1" >{{ \Carbon\Carbon::createFromFormat('Y-m-d', $yield->due_date)->format('d/m/Y') }}</td>
                                            <td width="1" >
                                                {{ $yield->payment_date ? \Carbon\Carbon::createFromFormat('Y-m-d', $yield->payment_date)->format('d/m/Y') : '' }}
                                            </td>
                                            <td width="3" >% {{ $splitPercent }}</td>
                                            <td width="1" >{{ $yield->amount }}</td>
                                            <td width="1" >{{ number_format($recebido, 2, ',', '.') }}</td>
                                        </tr>
                                        @php
                                            $totalParcela += $yield->amount;
                                            $totalRecebido += $recebido;

                                        @endphp
                                    @empty
                                        <tr class="tr-item td-border">
                                            <td colspan="10"
                                                class="text-center">Nenhum registro encontrado
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td colspan="5" >
                                                <b>Total: {{ $registros }}</b>
                                            </td>
                                            <td>{{ number_format($totalParcela, 2, ',', '.') }}</td>
                                            <td>{{ number_format($totalRecebido, 2, ',', '.') }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

