@extends('layouts.member')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Saldo</h1>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="invoice">
                                <div class="invoice-print">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div>
                                                        <strong>Conta de destino:</strong><br>
                                                        Banco: {{ $teacher->bank_number }}<br>
                                                        Agência: {{ $teacher->agency_number }}<br>
                                                        Conta: {{ $teacher->account_number }}<br>
                                                        Operação: {{ $teacher->account_complement_number ?? "-" }}
                                                    </div>
                                                </div>
                                                <div class="col-md-6  text-md-right">
                                                    <div>
                                                        <strong>Dados do titular:</strong><br>
                                                        Unidade {{ $teacher->user->name }}<br>
                                                        {{ $teacher->user->name }}<br>
                                                        {{ maskPhone($teacher->user->phone) }}<br>
                                                        {{ $teacher->user->email }}<br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-12">
                                            <div class="section-title">Dados financeiros</div>
                                            <p class="section-lead">Valores obtidos em tempo real</p>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-md">
                                                    <tbody><tr>
                                                            <th data-width="40" style="width: 40px;">#</th>
                                                            <th>Item</th>
                                                            <th class="text-right">Totais</th>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Saldo retido</td>
                                                            <td class="text-right">{{ money_parse_by_decimal((string)$result->withheldBalance, 'BRL') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Saldo liberado</td>
                                                            <td class="text-right">{{ money_parse_by_decimal((string)$result->transferableBalance, 'BRL') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Saldo total</td>
                                                            <td class="text-right">{{ money_parse_by_decimal((string) $result->balance ?? 0, 'BRL') }}</td>
                                                        </tr>
                                                    </tbody></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
