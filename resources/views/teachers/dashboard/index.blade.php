@extends('layouts.member')

@section('content')

    <section class="section">
        <div class="section-body mt-4">
            <div class="row">
                @include('member.dashboard._messages')
            </div>

            <div class="row">
                <div class="col-lg-6 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Turmas</h4>
                        </div>
                        <div class="card-body mb-0 p-2">
                            <ul class="list-group">
                                @forelse($teacher as $classroom)
                                    <li class="list-group-item">{{ $classroom->name }}
                                        <span class="float-right">
                                            <a href="{{ route('teachers.students.index', ['classroom' => $classroom->id]) }}"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               title=""
                                               data-original-title="Alunos">
                                                <i class="icon fas fa-user-friends lg"></i>
                                            </a>
                                            <a href="{{ route('teachers.frequencies.update', ['classroom' => $classroom->id]) }}"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               title=""
                                               data-original-title="Frequências">
                                                <i class="icon far fa-list-alt lg"></i>
                                            </a>
                                        </span>
                                    </li>
                                @empty
                                    <li class="list-group-item">
                                        Nenhum registro encontrado
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


