@extends('layouts.member')

@section('content')

    {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['teachers.messages.store']]) !!}
    <section class="section">
        <div class="section-header">
            <h1>Nova Mensagem</h1>
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                @include('teachers.messages._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
