@if($errors->any())
    @php toast()->error('Falha ao cadastrar mensagem', 'Erro') @endphp
@endif

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-8">
                    <div class="form-group mb-2">
                        {!! Form::label('subject', 'Assunto', ['class' => 'label-required']) !!}
                        {!! Form::text('subject', old('subject'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
                        @if($errors->has('subject'))
                            <span class="text-danger">{{ $errors->first('subject') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('classroom', 'Turma') !!}
                        {!! Form::select('classroom_id', $classrooms, old('classroom'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('classroom'))
                            <span class="text-danger">{{ $errors->first('classroom') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('expiration', 'Data de expiração', ['class' => '']) !!}
                        {!! Form::text('expiration', old('expiration'), ['class' => 'form-control js-datetimepicker']) !!}
                        @if($errors->has('expiration'))
                            <span class="text-danger">{{ $errors->first('expiration') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-2">
                        {!! Form::label('body', 'Mensagem', ['class' => 'label-required']) !!}
                        {!! Form::textarea('body', old('body'), ['class' => 'form-control js-redactor']) !!}
                        @if($errors->has('body'))
                            <span class="text-danger">{{ $errors->first('body') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <a href="{{ route('teachers.dashboard.index') }}"
               class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i
                        class="fas fa-angle-left"></i> Voltar</a>
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg"><i
                        class="fas fa-check"></i> Salvar
            </button>
        </div>
    </div>
</div>
