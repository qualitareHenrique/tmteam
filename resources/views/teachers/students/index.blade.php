@extends('layouts.member')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Alunos da {{ $classroom->name }}</h1>
            <div class="section-header-button">
                <a href="{{ route('teachers.students.print', $classroom->id) }}"
                   class="btn btn-primary btn-icon btn-lg"
                   title="Imprimir"
                   target="_blank"><i class="fas fa-print"></i>
                    Imprimir
                </a>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <b>Lista de alunos matriculados</b>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped overflow mb-0">
                                <tbody>
                                <tr class="tr-a border">
                                    <th>#</th>
                                    <th>Alunos</th>
                                    <th>Faixa</th>
                                    <th>Grau</th>
                                    <th>Aulas</th>
                                    <th>Graduado em</th>
                                </tr>
                                @forelse($registrations as $registration)
                                    @php $graduation = $registration->currentGraduation($classroom->modality_id); @endphp
                                    <tr class="tr-item border">
                                        <td class="td-item">{{ $loop->iteration }}</td>
                                        <td class="td-item"><img
                                                    class="border-100"
                                                    src="{{ $registration->student->user->avatarUrl }}"
                                                    alt="{{ $registration->student->user->nickname }}"
                                                    width="40"> {{ $registration->student->user->name }}</td>
                                        <td class="td-item">{{ $graduation ? $graduation->band->name : '' }}</td>
                                        <td class="td-item">{{ $graduation ? $graduation->degree->name : '' }}</td>
                                        <td class="td-item">{{ $graduation ? "{$graduation->lessons_taken} de {$graduation->degree->lessons}" : '' }}</td>
                                        <td class="td-item">{{ $graduation ? $graduation->graduated_on->format('d/m/Y') : '' }}</td>
                                    </tr>
                                @empty
                                    <tr class="tr-item border">
                                        <td colspan="3" class="td-item text-center">
                                            Nenhum registro encontrado
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('teachers.dashboard.index') }}"
                               class="btn btn-danger btn-lg btn-icon float-right mr-2"
                               title="Voltar"><i
                                        class="fas fa-angle-left"></i>
                                Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
