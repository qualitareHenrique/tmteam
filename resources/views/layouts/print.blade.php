<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#2c4e87">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}
    @include('layouts.common._favicons')

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ mix('css/global.css') }}">

    <style>
        body{ background-color: white}
    </style>
</head>
<body>
<div>
    @yield('content')

    <script type="application/javascript">
        window.print();
    </script>
</div>
</body>
</html>
