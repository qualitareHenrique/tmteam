<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <a href="{{ route("$currentAccess.dashboard.index") }}"
       class="navbar-brand sidebar-gone-hide">
        <img height="50" width="50" src="{{ asset( $avatar ) }}">
        <span class="text-dark"></span>
    </a>
    <div class="navbar-nav">
    </div>
    <div class="nav-collapse">
    </div>
    <form class="form-inline ml-auto">
    </form>
    <ul class="navbar-nav navbar-right">

        @include('layouts.common._notifications', ['color' => 'dark'])

        <li class="dropdown">
            <a href="#" data-toggle="dropdown"
               class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ auth()->user()->avatarUrl }}"
                     class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block text-dark">
                    {{ auth()->user()->nickname }}
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">

                @include('layouts.common._roles')

                <div class="dropdown-divider"></div>
                <a href="{{ route("$currentAccess.accounts.edit") }}"
                   class="dropdown-item has-icon">
                    <i class="far fa-user"></i> Perfil
                </a>

                @if(!empty(auth()->user()->teacher()->first()))
                    <div class="dropdown-divider"></div>
                    <a href="{{ route("teachers.financial.balance") }}"
                    class="dropdown-item has-icon">
                        <i class="fas fa-wallet lga"></i> Saldo
                    </a>

                    <div class="dropdown-divider"></div>
                    <a href="{{ route("teachers.financial.yield") }}"
                    class="dropdown-item has-icon">
                        <i class="fas fa-cash-register lga"></i> Rendimentos
                    </a>
                @endif

                <div class="dropdown-divider"></div>
                <a href="{{ route('auth.session.logout') }}"
                   class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Sair
                </a>
            </div>
        </li>
    </ul>
</nav>
