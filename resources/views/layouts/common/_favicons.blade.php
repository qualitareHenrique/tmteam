<link rel="icon" href="{{ asset('img/favicons/favicon.ico') }}" type="image/x-icon">
<link rel="icon" sizes="16x16" href="{{ asset('img/favicons/favicon-16x16.png') }}" type="image/png">
<link rel="icon" sizes="32x32" href="{{ asset('img/favicons/favicon-32x32.png') }}" type="image/png">

<!-- icon in the highest resolution we need it for -->
<link rel="icon" sizes="192x192" href="{{ asset('img/favicons/android-chrome-192x192.png') }}">

<!-- reuse same icon for Safari -->
<link rel="apple-touch-icon" href="{{ asset('img/favicons/apple-touch-icon.png') }}">

<!-- multiple icons for IE -->
<meta name="msapplication-square70x70logo" content="{{ asset('img/favicons/mstile-70x70.png') }}">
<meta name="msapplication-square150x150logo" content="{{ asset('img/favicons/mstile-150x150.png') }}">
<meta name="msapplication-wide310x150logo" content="{{ asset('img/favicons/mstile-310x150.png') }}">