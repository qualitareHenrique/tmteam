<li class="dropdown dropdown-list-toggle">
    <a href="#"
       data-toggle="dropdown"
       class="nav-link notification-toggle nav-link-lg {{ count(auth()->user()->notifications) ? 'beep' : '' }}">
        <i class="fas fa-bell text-{{ $color ?? 'white' }}"></i>
    </a>
    <div class="dropdown-menu dropdown-list dropdown-menu-right shadow rounded">
        <div class="dropdown-header bg-primary text-white">Notificações</div>
        <div class="dropdown-list-content dropdown-list-icons">
            @foreach(auth()->user()->notifications as $notification)
                <div class="dropdown-item dropdown-item-unread notification {{ $notification->read() ? 'bg-light' : '' }}">
                    <div class="dropdown-item-icon bg-primary">
                        <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                    <div class="dropdown-item-desc w-100">
                        <div class="time text-primary">{{ $notification->data['subject'] }}
                            <a href="#"
                               class="js-notification-delete"
                               data-link="{{ route('core.notifications.destroy', ['notification' => $notification->id]) }}">
                                <i class="fas fa-times float-right"></i></a>
                        </div>
                        <a href="#"
                           class="js-notification-redirect"
                           data-link="{{ $notification->data['link'] }}"
                           data-read-link="{{ route('core.notifications.read', ['notification' => $notification->id]) }}">
                            {{ $notification->data['message'] }}
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="dropdown-footer text-center"><p></p>
        </div>
    </div>
</li>
