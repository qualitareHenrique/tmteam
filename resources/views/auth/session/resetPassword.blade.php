@extends('layouts.public')

@section('content')

    <div class="box">
        <h3 class="box-title">Cadastrar <br> nova senha</h3>
        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'auth.password.processReset', 'method' => 'POST']) !!}
        {!! Form::hidden('token', $token) !!}
        {!! Form::hidden('email', $email) !!}
        <div class="group-form">
            {!! Form::label('password', 'Nova senha', ['class' => 'label-required']) !!}
            {!! Form::password('password', ['class' => '', 'autofocus']) !!}
            @if($errors->has('password'))
                <span class="span text-danger">{{ $errors->first('password') }}</span>
            @endif
        </div>
        <div class="group-form">
            {!! Form::label('password_confirmation', 'Confirmar senha', []) !!}
            {!! Form::password('password_confirmation', ['class' => '', 'autofocus']) !!}
            @if($errors->has('password_confirmation'))
                <span class="span text-danger">{{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>
        <div class="group-form">
            <button type="submit">Confirmar</button>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
