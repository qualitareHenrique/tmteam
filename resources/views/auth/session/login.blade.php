@extends('layouts.public')

@section('content')

    <div class="box">
        <h1 class="box-title">Iniciar Sessão</h1>
        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'auth.session.login', 'method' => 'POST']) !!}
        <div class="group-form">
            {!! Form::label('email', 'E-mail', []) !!}
            {!! Form::text('email', old('email'), ['class' => 'form-control', 'autofocus']) !!}
            @if($errors->has('email'))
                <span class="span text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>
        <div class="group-form">
            {!! Form::label('password', 'Senha', []) !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
            <a href="{{ route('auth.password.forgot') }}">
                Esqueceu a senha?
            </a>
            @if($errors->has('password'))
                <span class="span text-danger">{{ $errors->first('password') }}</span>
            @endif
        </div>
        <div class="group-form">
            <button type="submit">Entrar</button>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
