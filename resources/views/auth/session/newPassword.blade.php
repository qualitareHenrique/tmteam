@extends('layouts.public')

@section('content')

    <div class="box">
        <h1 class="box-title">Nova Senha</h1>
        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'auth.password.processForgot', 'method' => 'POST']) !!}
        <div class="group-form">
            {!! Form::label('email', 'E-mail', []) !!}
            {!! Form::text('email', old('email'), ['class' => '', 'autofocus']) !!}
            @if($errors->has('email'))
                <span class="span text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>
        <div class="group-form">
            <button type="submit">Enviar</button>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
