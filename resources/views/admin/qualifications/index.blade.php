@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Alunos qualificados</h1>
            <div class="section-header-button">
                <a href="{{ route('admin.qualifications.print') }}"
                   class="btn btn-primary btn-icon btn-lg"
                   title="Imprimir"
                   target="_blank"><i class="fas fa-print"></i>
                    Imprimir
                </a>
            </div>
            {!! Breadcrumbs::render('qualifications.index') !!}
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de Graduações cadastradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Aluno</th>
                                            <th>Modalidade</th>
                                            <th>Faixa</th>
                                            <th>Grau</th>
                                            <th>Aulas</th>
                                            <th>Graduação</th>
                                            <th></th>
                                        </tr>
                                        @php $total = 0; @endphp
                                        @forelse($results as $graduation)
                                            @php $total++; @endphp
                                            <tr class="tr-item td-border">
                                                <td class="td-item no-wrap">
                                                    <img class="border-100"
                                                         src="{{ $graduation->registration->student->user->avatarUrl }}"
                                                         alt="{{ $graduation->name }}"
                                                         title="{{ $graduation->name }}"
                                                         width="40"> {{ $graduation->name }}
                                                </td>
                                                <td class="td-item">{{ $graduation->modality }}</td>
                                                <td class="td-item">{{ $graduation->band }}</td>
                                                <td class="td-item">{{ $graduation->degree }}</td>
                                                <td class="td-item">{{ $graduation->lessons }}</td>
                                                <td class="td-item">{{ $graduation->graduated_on->diffForHumans() }}</td>
                                                <td class="td-item">
                                                    <a href="{{ route('admin.students.graduations.create', ['registration' => $graduation->registration_id]) . "?" . http_build_query(['last' => $graduation->id]) }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       data-original-title="Nova graduação">
                                                        <i class="icon fas fa-graduation-cap lg"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="7"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td colspan="7" class="td-item">
                                                <b>Total: {!! $total !!}</b>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

