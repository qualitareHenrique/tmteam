@extends('layouts.default')

@section('content')

    {!! Form::model($classroom, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.classrooms.update', 'id' => $classroom->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.classrooms.index') }}"
                   class="btn btn-icon" title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Turma</h1>
            {!! Breadcrumbs::render('classrooms.edit', $classroom) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.classrooms._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
