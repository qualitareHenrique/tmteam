@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Turmas</h1>
            @can('add_classrooms')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.classrooms.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('classrooms.index') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.classrooms.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-md-6 col-sm-12 mb-0 form-group">
                                {!! Form::text('name', Request::get('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('status', [0 => 'Desativado', 1 => 'Ativo'], Request::get('status'), ['class' => 'form-control', 'title' => 'Status ']) !!}
                            </div>
                            @can('view_subsidiaries')
                                <div class="col-md-2 col-sm-12 mb-0 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                            @endcan
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de turmas cadastradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Nome</th>
                                            <th>Modalidade</th>
                                            <th>Qtd. Alunos</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                        @forelse($results as $result)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">{{ $result->name }}</td>
                                                <td class="td-item">{{ $result->modality->name }}</td>
                                                <td class="td-item">{{ $result->registrations ? $result->registrations->count() : 0 }}</td>
                                                <td class="td-item">{!! isActiveBadge($result->active) !!}</td>
                                                <td class="td-item text-right">
                                                    <a href="https://chart.googleapis.com/chart?chs=450x450&cht=qr&chl={{ route('students.frequencies.create', ['id' => $result->id]) }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title=""
                                                       data-original-title="QrCode"
                                                       target="_blank"> <i
                                                                class="icon fas fa-qrcode lg"></i>
                                                    </a>
                                                    @can('edit_classrooms_submodules')
                                                        <a href="{{ route('admin.classrooms.frequencies.index', ['classroom_id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Frequências">
                                                            <i class="icon far fa-list-alt lg"></i></a>
                                                    @endcan

                                                    @if($result->active)
                                                        @can('edit_classrooms')
                                                            <a href="{{ route('admin.classrooms.deactivate', ['id' => $result->id]) }}"
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               title=""
                                                               data-original-title="Desativar">
                                                                <i class="icon fas fa-ban lg"></i></a>
                                                        @endcan
                                                    @else
                                                        @can('edit_classrooms')
                                                            <a href="{{ route('admin.classrooms.activate', ['id' => $result->id]) }}"
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               title=""
                                                               data-original-title="Ativar">
                                                                <i class="icon fas  fa-check lg"></i></a>
                                                        @endcan
                                                    @endif
                                                    @can('edit_classrooms')
                                                        <a href="{{ route('admin.classrooms.edit', ['id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>
                                                    @endcan

                                                    @can('delete_classrooms')
                                                        <a href="#"
                                                           class="js-confirm-delete"
                                                           data-link="{{ route('admin.classrooms.destroy', ['id' => $result->id]) }}"
                                                           data-title="{{ $result->name }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Excluir">
                                                            <i class="icon far fa-trash-alt lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="4"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="3">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

