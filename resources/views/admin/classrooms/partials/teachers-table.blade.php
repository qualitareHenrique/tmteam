<div class="card">
    <div class="card-header">
        <h4>
            <i class="far fa-edit lga"></i>
            Professores
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <table id="classroom-teatcher-table" class="table">
                    <tbody>
                        <tr class="tr-a">
                            <th>Nome</th>
                            <th>Split</th>
                            <th></th>
                        </tr>
                        @include('admin.classrooms.partials.teachers-row')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>
    </div>
</div>