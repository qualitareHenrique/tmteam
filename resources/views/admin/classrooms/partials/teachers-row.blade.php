@if(!empty($classroomTeachers))
    @foreach($classroomTeachers as $classroomTeacher)
        <tr class="tr-item td-border">
            <td class="td-item">{{ $classroomTeacher->teacher->user->name }}</td>
            <td class="td-item">% {{ $classroomTeacher->split_value }}</td>
            <td class="td-item text-right" data-id="{{ $classroomTeacher->id }}">
                <a href="{{ route('admin.classrooms-teachers.edit', ['id' => $classroomTeacher->id]) }}" title="Editar"><i class="icon far fa-edit lg"></i></a>
                <a onclick="deleteClassroomTeacher(this)" href="" title="Excluir"><i class="icon far fa-trash-alt lg"></i></a>
            </td>
        </tr>
    @endforeach
@endif