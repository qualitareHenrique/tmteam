<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
                        {!! Form::text('name', $classroom->name ?? old('name'), ['class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('modality_id', 'Modalidade', ['class' => 'label-required']) !!}
                        {!! Form::select('modality_id', $modalities, $classroom->modality_id ?? old('modality_id'), ['class' => 'form-control']) !!}
                        @if($errors->has('modality_id'))
                            <span class="text-danger">{{ $errors->first('modality_id') }}</span>
                        @endif
                    </div>
                </div>

                @can('view_subsidiaries')
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-4">
                            {!! Form::label('subsidiary_id', 'Unidade', ['class' => 'label-required']) !!}
                            {!! Form::select('subsidiary_id', $subsidiaries, $classroom->subsidiary_id ?? old('subsidiary_id'), ['class' => 'form-control']) !!}
                            @if($errors->has('subsidiary_id'))
                                <span class="text-danger">{{ $errors->first('subsidiary_id') }}</span>
                            @endif
                        </div>
                    </div>
                @else
                    {!! Form::hidden('subsidiary_id', $classroom->subsidiary_id ?? auth()->user()->subsidiary_id) !!}
                @endcan

                @if(!empty($classroom->id))
                    {!! Form::hidden('classroom_id', $classroom->id) !!}
                    <div class="col-sm-12 col-md-12">
                        <div class="row form-group">
                            <div class="col-sm-2 col-xs-2 col-md-4">
                                {!! Form::label('teachers', 'Adicionar Professor', ['class' => 'label-required']) !!}
                                {!! Form::select('teacher_id', $teachers, null, ['class' => 'form-control'])!!}
                            </div>
                            <div class="btn-add-teacher col-md-2 col-xs-12 col-sm-12 ">
                                <button type="button" 
                                    onclick="updateTeacherInput(this)" 
                                    class="btn btn-icon btn-plus btn-success"
                                >
                                    <i class="fas fa-plus lg-icon"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            @if(!empty($classroom->id))
                @include('admin.classrooms.partials.teachers-table')
            @endif
            <div class="card-footer text-right">
                <button type="submit"
                        class="btn btn-icon icon-left btn-success btn-lg">
                    <i class="fas fa-check"></i> Salvar
                </button>
            </div>
        </div>
        </div>
    </div>
</div>
