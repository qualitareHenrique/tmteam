<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="stepwizard py-4">
                <div class="stepwizard-row setup-panel row">
                    <div class="stepwizard-step col text-center">
                    <a class="btn btn-secondary rounded-circle" disabled="disabled">1</a>
                        <p>Aluno</p>
                    </div>
                    <div class="stepwizard-step col text-center">
                    <a class="btn btn-secondary rounded-circle" disabled="disabled">2</a>
                        <p>Responsável</p>
                    </div>
                    <div class="stepwizard-step col text-center">
                    <a class="btn btn-primary btn-secondary rounded-circle">3</a>
                        <p>Matrícula</p>
                    </div>
                    <div class="stepwizard-step col text-center">
                    <a class="btn btn-secondary rounded-circle" disabled="disabled">4</a>
                        <p>Graduação</p>
                    </div>
                </div>
        </div>
        <div class="card-header">
            <h4><i class="far fa-user-circle lga"></i> Matrícula</h4>
        </div>
        <div class="card-body collapse show" id="form-registration">
            <div class="row">
                @php $disabled = (bool) $student->registration; @endphp
                {!! Form::hidden('type', 1) !!}
                {!! Form::text('registration[id]', $student->registration->id ?? null, ['hidden' => true]) !!}
                {!! Form::text('registration[subsidiary_id]', $student->user->subsidiary_id ?? auth()->user()->subsidiary_id, ['class' => 'subsidiary', 'hidden' => true]) !!}
                <div class="col-sm-12 col-lg-12">
                    <div class="form-group mb-4">
                        {!! Form::label("registration[classrooms][]", 'Turmas', ['class' => 'label-required']) !!}
                        {!! Form::select("registration[classrooms][]",
                                        $classrooms->pluck('name', 'id'),
                                        empty($student->registration) ? old('registration[classrooms]') : $student->registration->classrooms->pluck('id'),
                                        ['class' => 'form-control classroom', 'disabled' => $disabled, 'multiple' => true, 'title' => 'Selecione', 'onchange' => 'calcRegistrationValues()'],
                                        classroomsToDataAttr($classrooms)) !!}
                        @if($errors->has('registration.classrooms'))
                            <span class="text-danger">{{ $errors->first('registration.classrooms') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-lg-4">
                    <div class="form-group mb-4">
                        {!! Form::label('registration[acquisition]', 'Valor da matrícula', ['class' => '']) !!}
                        {!! Form::number('registration[acquisition]', $student->registration->acquisition ?? old('registration[amount]'), ['class' => 'form-control ',  'step' => '0.01', 'placeholder' => '0,00', 'disabled' => $disabled]) !!}
                        @if($errors->has('registration.acquisition'))
                            <span class="text-danger">{{ $errors->first('registration.acquisition') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-lg-4">
                    <div class="form-group mb-4">
                        {!! Form::label('registration[amount]', 'Valor da mensalidade', ['class' => '']) !!}
                        {!! Form::number('registration[amount]', $student->registration->amount ?? old('registration[amount]'), ['class' => 'form-control ',  'step' => '0.01', 'placeholder' => '0,00', 'disabled' => $disabled]) !!}
                        @if($errors->has('registration[amount]'))
                            <span class="text-danger">{{ $errors->first('registration.amount') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('registration[effective_date]', 'Data de início da vingência', ['class' => '']) !!}
                        {!! Form::text('registration[effective_date]', $student->registration->effective_date ?? date('Y-m-d', time()) , ['class' => 'form-control js-datepicker', 'disabled' => $disabled]) !!}
                        @if($errors->has('registration.effective_date'))
                            <span class="text-danger">{{ $errors->first('registration.effective_date') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('registration[advance_discount]', 'Desconto até vencimento', ['class' => '']) !!}
                        {!! Form::number('registration[advance_discount]', $student->registration->advance_discount ?? old('registration[advance_discount]'), ['class' => 'form-control ',  'step' => '0.00', 'placeholder' => '0,00', 'disabled' => $disabled]) !!}
                        @if($errors->has('registration.advance_discount'))
                            <span class="text-danger">{{ $errors->first('registration.advance_discount') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('charge_fee', 'Taxas', []) !!}
                        {!! Form::number('charge_fee', $installment->charge_fee ?? old('charge_fee'), ['class' => 'form-control',  'step' => '0.01', 'placeholder' => '0,00', 'disabled' => $disabled]) !!}
                        @if($errors->has('charge_fee'))
                            <span class="text-danger">{{ $errors->first('charge_fee') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('discount_amount', 'Desconto', ['class' => '']) !!}
                        {!! Form::number('discount_amount', $installment->discount_amount ?? old('discount'), ['class' => 'form-control',  'step' => '0.01', 'placeholder' => '0,00', 'disabled' => $disabled]) !!}
                        @if($errors->has('discount_amount'))
                            <span class="text-danger">{{ $errors->first('discount_amount') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('registration[payment_type]', 'Tipo de Pagamento', ['class' => 'label-required']) !!}
                        {!! Form::select('registration[payment_type]', enumOptions('payment_type'), $student->registration->payment_type ?? '', ['class' => 'form-control', 'title' => 'Selecione', 'disabled' => $disabled]) !!}
                        @if($errors->has('registration.payment_type'))
                            <span class="text-danger">{{ $errors->first('registration.payment_type') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('registration[payment_day]', 'Dia de pagamento', ['class' => '']) !!}
                        {!! Form::number('registration[payment_day]', $student->registration->payment_day ?? date('j', time()), ['class' => 'form-control', 'min' => 1, 'max' => 31, 'disabled' => $disabled]) !!}
                        @if($errors->has('registration.payment_day'))
                            <span class="text-danger">{{ $errors->first('registration.payment_day') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('due_date', 'Data de vencimento', ['class' => 'label-required']) !!}
                        {!! Form::text('due_date', old('due_date'), ['class' => 'form-control js-datepicker', 'disabled' => $disabled]) !!}
                        @if($errors->has('due_date'))
                            <span class="text-danger">{{ $errors->first('due_date') }}</span>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
