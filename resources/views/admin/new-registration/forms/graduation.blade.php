<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="stepwizard py-4">
                <div class="stepwizard-row setup-panel row">
                    <div class="stepwizard-step col text-center"> 
                    <a class="btn btn-secondary rounded-circle" disabled="disabled">1</a>
                        <p>Aluno</p>
                    </div>
                    <div class="stepwizard-step col text-center"> 
                    <a class="btn btn-secondary rounded-circle" disabled="disabled">2</a>
                        <p>Responsável</p>
                    </div>
                    <div class="stepwizard-step col text-center"> 
                    <a class="btn btn-secondary rounded-circle" disabled="disabled">3</a>
                        <p>Matrícula</p>
                    </div>
                    <div class="stepwizard-step col text-center"> 
                    <a class="btn btn-primary btn-secondary rounded-circle">4</a>
                        <p>Graduação</p>
                    </div>
                </div>
        </div>
        <div class="card-header">
            <h4><i class="far fa-user-circle lga"></i> Graduação - {{ $classroom->name}}</h4>
        </div>
        <div class="card-body collapse show" id="form-graduation">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::hidden('registration_id', $registration->id) !!}
                        {!! Form::label('modality_id', 'Modalidade', ['class' => 'label-required']) !!}
                        {!! Form::select('modality_id', $modality, $modality[0]['id'] ?? old('modality_id'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('modality_id'))
                            <span class="text-danger">{{ $errors->first('modality_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('band_id', 'Faixa', ['class' => 'label-required']) !!}
                        {!! Form::select('band_id', $bands->pluck('name', 'id'), old('band_id'), ['class' => 'form-control', 'title' => 'Selecione'], bandsToDataAttribute($bands)) !!}
                        @if($errors->has('band_id'))
                            <span class="text-danger">{{ $errors->first('band_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('degree_id', 'Grau', ['class' => 'label-required']) !!}
                        {!! Form::select('degree_id', $degrees->pluck('name', 'id'), old('degree_id'),['class' => 'form-control', 'title' => 'Selecione'], degreesToDataAttribute($degrees)) !!}
                        @if($errors->has('degree_id'))
                            <span class="text-danger">{{ $errors->first('degree_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('teacher_id', 'Professor', ['class' => '']) !!}
                        {!! Form::select('teacher_id', $teachers, old('teacher_id'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('teacher_id'))
                            <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('lessons_taken', 'Aulas assistidas', ['class' => 'label-required']) !!}
                        {!! Form::number('lessons_taken',  old('lessons_taken'), ['class' => 'form-control'] ) !!}
                        @if($errors->has('lessons_taken'))
                            <span class="text-danger">{{ $errors->first('lessons_taken') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('graduated_on', 'Data de graduação', ['class' => 'label-required']) !!}
                        {!! Form::text('graduated_on', old('graduated_on'), ['class' => 'form-control js-datepicker'] ) !!}
                        @if($errors->has('graduated_on'))
                            <span class="text-danger">{{ $errors->first('lessons_taken') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>     

@push('scripts')
    <script type="application/javascript">
        filterBands();

        $('#modality_id').on('change', function () {
            filterBands(true);
        });

        $('#band_id').on('change', function () {
            filterDegrees(true);
        });

        function filterBands(clearSelected = false){
            var field = $('#modality_id');
            var modality = parseInt(field.val());

            $('#band_id>option').each(function () {

                var band_modality = parseInt($(this).data('modality'));

                if(!isNaN(band_modality)){
                    $(this).toggle(band_modality === modality);
                }

                if (clearSelected) {
                    this.selected = false;
                }
            });

            setTimeout(function (){
                $('#band_id').selectpicker('refresh');
                filterDegrees();
            }, 200);
        }

        function filterDegrees(clearSelected = false)
        {
            var field = $('#band_id');
            var band = parseInt(field.val());

            $('#degree_id>option').each(function () {

                var degree_band = parseInt($(this).data('band'));

                if(!isNaN(degree_band)){
                    $(this).toggle(degree_band === band);
                }

                if (clearSelected) {
                    this.selected = false;
                }
            });

            setTimeout(function (){
                $('#degree_id').selectpicker('refresh');
            }, 200);
        }
    </script>
@endpush
