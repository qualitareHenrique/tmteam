<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="stepwizard py-4">
            <div class="stepwizard-row setup-panel row">
                <div class="stepwizard-step col text-center"> 
                <a class="btn btn-secondary rounded-circle" disabled="disabled">1</a>
                    <p>Aluno</p>
                </div>
                <div class="stepwizard-step col text-center"> 
                <a class="btn btn-primary btn-secondary rounded-circle">2</a>
                    <p>Responsável</p>
                </div>
                <div class="stepwizard-step col text-center"> 
                <a class="btn btn-secondary rounded-circle" disabled="disabled">3</a>
                    <p>Matrícula</p>
                </div>
                <div class="stepwizard-step col text-center"> 
                <a class="btn btn-secondary rounded-circle" disabled="disabled">4</a>
                    <p>Graduação</p>
                </div>
            </div>
        </div>
        <div class="card-header">
            <h4><i class="far fa-user-circle lga"></i> Responsável</h4>
        </div>
        <div class="card-body collapse show" id="form-guardian">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::hidden('user_id', $guardian->user->id ?? old('user_id')) !!}
                        {!! Form::hidden('student_id', $student->id ?? old('student_id')) !!}
                        {!! Form::hidden('registration_id', $registration->id ?? old('registration_id')) !!}
                        {!! Form::hidden('user[subsidiary_id]', $student->user->subsidiary_id ?? old('subsidiary_id')) !!}
                        {!! Form::label('user[cpf]', 'CPF', ['class' => '']) !!}
                        {!! Form::text('user[cpf]',
                            $guardian->user->cpf ?? old('user[cpf]'),
                            ['id' => 'student-cpf', 'class' => 'form-control js-mask-cpf cpf',
                                'data-link' => route('admin.persons.show', [''])]) 
                        !!}
                        @if($errors->has('user.cpf'))
                            <span class="text-danger">{{ $errors->first('user.cpf') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="form-group mb-4">
                        {!! Form::label('user[name]', 'Nome', ['class' => '']) !!}
                        {!! Form::text('user[name]', $guardian->user->name ?? old('user[name]'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.name'))
                            <span class="text-danger">{{ $errors->first('user.name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[nickname]', 'Apelido', ['class' => '']) !!}
                        {!! Form::text('user[nickname]', $guardian->user->nickname ?? old('user[nickname]'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.nickname'))
                            <span class="text-danger">{{ $errors->first('user.nickname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('cbjj', 'CBJJ', ['class' => '']) !!}
                        {!! Form::text('cbjj', $guardian->cbjj ?? old('cbjj'), ['class' => 'form-control']) !!}
                        @if($errors->has('cbjj'))
                            <span class="text-danger">{{ $errors->first('cbjj') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[email]', 'E-mail', ['class' => '']) !!}
                        {!! Form::text('user[email]',  $guardian->user->email ?? old('user[email]'), ['class' => 'form-control']) !!}
                        @if($errors->has('user.email'))
                            <span class="text-danger">{{ $errors->first('user.email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('user[phone]', 'Telefone', ['class' => '']) !!}
                        {!! Form::text('user[phone]',  $guardian->user->phone ?? old('user[phone]'), ['class' => 'form-control js-mask-phone']) !!}
                        @if($errors->has('user.phone'))
                            <span class="text-danger">{{ $errors->first('user.phone') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[birthdate]', 'Data de Nascimento', ['class' => '']) !!}
                        {!! Form::text('user[birthdate]', !empty($guardian) ? $guardian->user->birthdate : old('user[birthdate]'), ['class' => 'form-control js-datepicker birthdate']) !!}
                        @if($errors->has('user.birthdate'))
                            <span class="text-danger">{{ $errors->first('user.birthdate') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('user[sex]', 'Sexo', ['class' => '']) !!}
                        {!! Form::select('user[sex]', [1 => 'Masculino', 2 => 'Feminino'], $guardian->user->sex ?? old('user.sex'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('user.sex'))
                            <span class="text-danger">{{ $errors->first('user.sex') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    {!! Form::label('user[avatar]', 'Foto do Responsavél', []) !!}
                    <div class="file-upload-wrapper">
                        {{ Form::file('user[avatar]', ['class' => 'file-upload-field']) }}
                        @if($errors->has('user.avatar'))
                            <span class="text-danger">{{ $errors->first('user.avatar') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
