@extends('layouts.default')
@section('content')
    @if($errors->any())
        @php toast()->error('Verifique todos os campos e tente novamente.', 'Error'); @endphp
    @endif
    @yield('content')
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

            $('input[name="user[cpf]"]').blur(function () {
                let cpf = $(this).val().replace(/\D/g, '');
                let url = $(this).data('link');

                $.ajax({
                    type: "GET",
                    url: url + `/${cpf}`,
                    dataType: 'json',
                    success: function (response) {

                        $('input[name="user_id"]').val(response.data.id);
                        $('input[name="user[name]"]').val(response.data.name);
                        $('input[name="user[email]"]').val(response.data.email);
                        $('input[name="user[phone]"]').val(response.data.phone);
                        $('input[name="user[birthdate]"]').val(response.data.birthdate);
                        $('select[name="user[sex]"]').val(response.data.sex);
                        $('input[name="user[age]"]').val(response.data.age);

                        $('.cep').val(response.data.cep);
                        $('.rua').val(response.data.street);
                        $('.complemento').val(response.data.complement);
                        $('.bairro').val(response.data.neighborhood);
                        $('.numero').val(response.data.number);
                        $('.cidade').val(response.data.city);
                        $('.uf').val(response.data.state);

                        let nickname = (response.data.name.split(" "))[0];
                        $('input[name="user[nickname]"]').val(nickname);
                    }
                });
            });

            $('input[name="user[name]"]').blur(function () {
                let nickname = ($(this).val().split(" "))[0];

                $('input[name="user[nickname]"]').val(nickname);
            });
        });

        function calcRegistrationValues() {
            let value = 0.00;

            $('select.classroom>option:selected').each(function (index, item) {
                if ($(item).val() > 0) {
                    value += parseFloat($(item).data('value'));
                }
            });

            $('input[name="registration[acquisition]"]').val(value);
            $('input[name="registration[amount]"]').val(value);
        }

        function filterClassroomBySelect(select) {
            let option = $(select).find('option:selected')[0];
            let subsidiary = $(option).val();

            filterClassroom(subsidiary, true);
        }

        function filterClassroom(subsidiary, clearAll = false) {
            $('.classroom > option').each(function () {
                let subsidiary_id = $(this).data('subsidiary');

                if(subsidiary_id){
                    $(this).css('display', subsidiary_id == subsidiary ? 'block' : 'none');
                    $(this).prop('disabled', subsidiary_id == subsidiary ? false : true);
                }
            });

            if(clearAll){
                $('.classroom').selectpicker('deselectAll');
            }

            $('.classroom').selectpicker('refresh');
        }
    </script>
@endpush
