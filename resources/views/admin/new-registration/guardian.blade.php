@extends('admin.new-registration.index')

@section('content')

{!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.new-registration.save-guardian', 'studentId' => $student->id], 'enctype' => 'multipart/form-data']) !!}
<section class="section">
    <div class="section-header mb-0">
        <div class="section-header-back">
            <a href="{{ route('admin.dashboard.index') }}" class="btn btn-icon" title="Voltar"><i class="fas fa-arrow-left"></i> </a>
        </div>

        <h1>Responsável</h1>
        {!! Breadcrumbs::render('new-registration.guardian', $student->id) !!}
    </div>

    <div class="clearfix"></div>

    <div class="section-body mt-4">
        <div class="row">
            @include('admin.new-registration.forms.guardian')
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-lg-12 col-sm-12 text-right mb-2">
        <button type="submit"
                class="btn btn-icon icon-left btn-success btn-lg">
            <i class="fas fa-arrow-right"></i> Avançar
        </button>
    </div>
</section>
{!! Form::close() !!}

@endsection