@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Permissões</h1>

            @can('add_permissions')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.permissions.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"> <i class="fas fa-plus"></i> Adicionar</a>
                </div>
            @endcan

            {!! Breadcrumbs::render('permissions.index') !!}
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card mb-0">
                    <div class="card-body">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.permissions.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-12 mb-0 form-group">
                                {!! Form::text('name', Request::get('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 mb-0 form-group">
                                {!! Form::text('guard', Request::get('guard'), ['class' => 'form-control', 'placeholder' => 'Acesso']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-12">
                    <h6 class="mb-3">Listagem de permissões cadastradas</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-item td-border">
                                            <th>Nome</th>
                                            <th>Detalhes</th>
                                            <th>Acesso</th>
                                            <th></th>
                                        </tr>
                                        @forelse($results as $result)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">{{ $result->name }}</td>
                                                <td class="td-item">{{ $result->details }}</td>
                                                <td class="td-item">{{ $result->guard_name }}</td>
                                                <td class="no-wrap text-right">

                                                    @can('edit_permissions')
                                                        <a href="{{ route('admin.permissions.edit', ['id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>
                                                    @endcan

                                                    @can('delete_posts')
                                                        <a href="#"
                                                           class="js-confirm-delete"
                                                           data-link="{{ route('admin.permissions.destroy', ['id' => $result->id]) }}"
                                                           data-title="{{ $result->title }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Excluir">
                                                            <i class="icon far fa-trash-alt lg"></i></a>
                                                    @endcan

                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="4"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="3">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
