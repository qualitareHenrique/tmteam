@extends('layouts.default')

@section('content')

    {!! Form::model($permission, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.permissions.update', 'id' => $permission->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.permissions.index') }}"
                   class="btn btn-icon" title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Permissão</h1>
            {!! Breadcrumbs::render('permissions.edit', $permission) !!}
        </div>
        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.permissions._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
