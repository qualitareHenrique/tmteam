@extends('layouts.default')

@section('content')

  {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.guardians.store', 'enctype' =>"multipart/form-data"]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <div class="section-header-back">
        <a href="{{ route('admin.guardians.index') }}"
           class="btn btn-icon"
           title="Voltar">
          <i class="fas fa-arrow-left"></i>
        </a>
      </div>
      <h1>Novo responsável</h1>
      {!! Breadcrumbs::render('guardians.create') !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">

      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body mt-4">
      <div class="row">
        @include('admin.guardians._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
