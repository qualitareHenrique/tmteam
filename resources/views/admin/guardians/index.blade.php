@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Responsáveis</h1>
            @can('add_guardians')
                <div class="section-header-button">
                    <a href="{{ route('admin.guardians.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('guardians.index') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.guardians.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-md-6 col-sm-12 mb-0 form-group">
                                {!! Form::text('name', Request::get('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('status', [0 => 'Desativado', 1 => 'Ativo'], Request::get('status'), ['class' => 'form-control', 'title' => 'Status ']) !!}
                            </div>
                            <div class="col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade ']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de responsáveis cadastrados</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Nome</th>
                                            <th>Contato</th>
                                            <th>Status</th>
                                            <th>Unidade</th>
                                            <th></th>
                                        </tr>
                                        @forelse($results as $result)
                                            <tr class="tr-item td-border">
                                                <td class="td-item"><img
                                                            class="border-100"
                                                            src="{{ $result->user->avatarUrl }}"
                                                            alt="{{ $result->user->name }}"
                                                            width="40"> {{ $result->user->name }}
                                                </td>
                                                <td class="td-item">{{ maskPhone($result->user->phone) }}</td>
                                                <td class="td-item">{!! isActiveBadge($result->user->active) !!}</td>
                                                <td class="td-item">{{ $result->user->subsidiary->name }}</td>
                                                <td class="td-item text-right">
                                                    @can('edit_guardians')
                                                        <a href="{{ route('admin.guardians.edit', ['id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>
                                                    @endcan

                                                    @can('delete_guardians')
                                                        <a href="#"
                                                           class="js-confirm-delete"
                                                           data-link="{{ route('admin.guardians.destroy', ['id' => $result->id]) }}"
                                                           data-title="{{ $result->name }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Excluir">
                                                            <i class="icon far fa-trash-alt lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="4"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="3">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

