@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Faturas</h1>
            <div class="section-header-button mr-2">
                <button class="btn btn-primary btn-lg"
                        data-toggle="modal"
                        data-target="#installmentsModal"><i
                            class="fas fa-plus"></i> Parcelas
                </button>
            </div>
            {!! Breadcrumbs::render('balance_records.index') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.invoices.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-md-4 col-sm-12 mb-0 form-group">
                                {!! Form::text('name', Request::get('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-md-2 col-sm-12 mb-1 form-group">
                                {!! Form::text('due_date', Request::get('due_date'), ['class' => 'form-control js-datepicker', 'placeholder' => 'Vencimento']) !!}
                            </div>
                            <div class="col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('status', [0 => 'A faturar', 1 => 'Faturado', 2 => 'Pago'], Request::get('paid'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                            </div>
                            @can('view_subsidiaries')
                                <div class="col-lg-2 col-md-2 col-sm-12 mb-1 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                            @endcan
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 ml-auto form-group">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        @if(session()->has('gateway_messages'))
            <div class="col-12 mb-4 mt-4">
                <div class="bg-warning text-white p-4 rounded text-dark">
                    <div class="h5">Ops, houve uma falha</div>
                    <ul>
                        @foreach(session()->get('gateway_messages') as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de lançamentos elegíveis</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.invoices.store', 'method' => 'POST', 'id' => 'form-installment']) !!}
                                    <tbody>
                                        <tr class="tr-a">
                                            <th><input class="ml-1"
                                                       type="checkbox"
                                                       onclick="checkAll(this)"
                                                       title="Selecionar todos">
                                            </th>
                                            <th></th>
                                            <th>Impressão</th>
                                            <th>Situação</th>
                                            <th>Descrição</th>
                                            <th>Valor</th>
                                            <th>Vencimento</th>
                                            <th></th>
                                        </tr>
                                        @forelse($records as $record)
                                            <tr class="tr-item td-border">
                                                @php
                                                    $billed = in_array($record->status, [2, 3]);
                                                    $user = $record->chargeable->financier->user ?? null;
                                                    $valid  = !empty($user->cpf) && $user->age >= 18;
                                                @endphp
                                                <td class="td-item">{!! Form::checkbox('balance_records[]',
                                                                                    $record->id,
                                                                                    false,
                                                                                    ['class'=> 'can-check', 'disabled' => $billed || !$valid]
                                                                                    ) !!}
                                                </td>
                                                <td class="td-item text-left">
                                                    <a href="{{ route('admin.students.edit', $record->chargeable->student->id)  }}"
                                                       target="_blank"
                                                       title="{{ $record->chargeable->student->user->name }}">
                                                    <span class="badge {{ $valid ? 'badge-success' : 'badge-danger' }}">
                                                        <i class="fas fa-user"> </i> {{ $record->chargeable->student->user->nickname }}
                                                    </span>
                                                    </a>
                                                </td>
                                                <td class="td-item text-left">
                                                    @if($record->status == \App\Enums\BalanceRecordStatusEnum::BILLED)
                                                        <a href="{!! $record->charge_link !!}"
                                                           target="_blank"><i
                                                                    class="fas fa-barcode lg-icon"></i></a>
                                                    @else
                                                        <i class="fas fa-ban lg-icon"></i>
                                                    @endif
                                                </td>
                                                <td class="td-item">{!! invoiceStatusBadge($record->status) !!}</td>
                                                <td class="td-item">{{ $record->description }}</td>
                                                <td class="td-item">{{ money_parse_by_decimal($record->amount, 'BRL') }}</td>
                                                <td class="td-item">{{ $record->due_date->format('d/m/Y') }}</td>
                                                <td>
                                                <div class="dropdown">
                                                        <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" data-boundary="window" title="Ações" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </a>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                            @if(Gate::check('cancel_invoices') && \App\Enums\BalanceRecordStatusEnum::BILLED === $record->status)
                                                                <a onclick="cancelInvoice(this)"
                                                                    data-link="{{ route('admin.juno.cancel', ['invoice' => $record->id]) }}"
                                                                    title="Cancelar Cobrança" class="dropdown-item cancel-invoice" href="#">
                                                                <i class="icon lg fas fa-ban"></i>
                                                                    Cancelar Cobrança
                                                                </a>
                                                            @endif
                                                            @if(Gate::check('manual_payment') && \App\Enums\BalanceRecordStatusEnum::isPayable($record->status))
                                                                <a onclick="manualPaymentInvoice(this)"
                                                                    data-link="{{ route('admin.juno.manual-payment', ['invoice' => $record->id]) }}"
                                                                    title="Baixa Manual" class="dropdown-item manual-payment" href="#">
                                                                <i class="icon lg fas fa-hand-point-down"></i>
                                                                    Baixa Manual
                                                                </a>
                                                            @endif
                                                        </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="7"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item">
                                            <td colspan="7">
                                                <button type="submit"
                                                        class="btn btn-warning">
                                                    <i
                                                            class="fas fa-file-invoice lg-icon"></i>
                                                    Faturar
                                                </button>
                                            </td>
                                        </tr>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $records->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="6">
                                                <div class="float-right">{!! $records->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    {!! Form::close() !!}
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.invoices._generate-installments')

@endsection

@push('scripts')
    <script>
        function checkAll(checkbox) {
            $('.can-check').each(function (index, item) {
                if (!$(item).attr('disabled')) {
                    $(item).prop('checked', $(checkbox).is(':checked'));
                }
            })
        }

        function cancelInvoice(element) {
            swal({
                title: 'Deseja realmente cancelar essa cobrança?',
                type: 'warning',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                showCancelButton: true,
                confirmButtonColor: '#3f51b5',
                cancelButtonColor: '#ff4081',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: $(element).data('link'),
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        success: function (data) {
                            swal('Sucesso!', data.success, 'success');

                            setTimeout(function () {
                                window.location.reload();
                            }, 500);
                        },
                        error: function (data) {
                            swal('Falha na solicitação', data.error, 'error');
                        }
                    });
                    return false;
                }
                swal('Ação cancelada', 'a cobrança não foi cancelada.', 'warning');
            });
        }

        function manualPaymentInvoice(element) {
            var manualPayment = 0;
            var cancelGateway = 0;

            swal.mixin({
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                showCancelButton: true,
                confirmButtonColor: '#3f51b5',
                cancelButtonColor: '#ff4081',
            }).queue([
                {
                    text: 'Deseja realizar a baixa desse pagamento?',
                    preConfirm: function (value) {
                        manualPayment = value;
                    }
                },
                {
                    text: 'Deseja cancelar a cobrança no gateway caso esteja em aberto?',
                    preConfirm: function (value) {
                        cancelGateway = value;
                    }
                }
            ]).then((result) => {
                if (!manualPayment) {
                    swal('Ação cancelada', 'A cobrança não foi baixada.', 'warning');
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: $(element).data('link'),
                    data: {'cancelGateway': cancelGateway},
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (data) {
                        swal('Sucesso!', data.success, 'success');
                        setTimeout(function () {
                            window.location.reload();
                        }, 500);
                    },
                    error: function (data) {
                        swal('Falha na solicitação', data.error, 'error');
                    }
                });
            });
        }
    </script>
@endpush

