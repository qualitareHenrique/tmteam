@component('mail::message')
Olá, {{ $user->nickname }}

Segue a segunda via dos seus boletos

<style>
    table {
        margin: 12px;
    }
    th {
        font-size: 20px;
    }
    td {
        font-size: 16px;
        padding-right: 20px;
        padding-left:  20px;
    }
    .btn {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 12px 24px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 12px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
        background-color: white; 
        color: black; 
        border: 2px solid #008CBA;
    }
    .btn:hover {
        background-color: #008CBA;
        color: white;
    }
</style>

<table>
    <tr>
        <th>Vencimento</th>
        <th>Link</th>
    </tr>
    @foreach($recordes as $record)
        @if($record->charge_link)
            <tr>
                <td>
                    {{ $carbon->createFromFormat('Y-m-d H:i:s', $record->due_date)->format('d/m/Y') }}
                </td>
                <td style="max-width: 350px">
                    <a class="btn" href="{{ $record->charge_link }}">
                        Acessar
                    </a>
                </td>
            </tr>
        @endif
    @endforeach
</table>

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent