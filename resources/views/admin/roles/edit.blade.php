@extends('layouts.default')

@section('content')

    {!! Form::model($role, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.roles.update', 'id' => $role->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.roles.index') }}" class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Grupo</h1>
            {!! Breadcrumbs::render('roles.edit', $role) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.roles._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
