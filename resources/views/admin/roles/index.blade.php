@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Grupos</h1>
            @can('add_users')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.roles.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"> <i class="fas fa-plus"></i> Adicionar</a>
                </div>
            @endcan

            {!! Breadcrumbs::render('roles.index') !!}
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-12">
                    <h6 class="mb-3">Listagem de grupos cadastrados</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-item td-border">
                                        <th>Nome</th>
                                        <th>Detalhes</th>
                                        <th>Acesso</th>
                                        <th></th>
                                    </tr>
                                    @forelse($results as $result)
                                        <tr class="tr-item td-border">
                                            <td class="td-item">{{ $result->name }}</td>
                                            <td class="td-item">{{ $result->details }}</td>
                                            <td class="td-item">{{ $result->guard_name }}</td>
                                            <td class="no-wrap text-right">
                                                @can('edit_roles')
                                                    <a href="{{ route('admin.roles.edit', ['id' => $result->id]) }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title=""
                                                       data-original-title="Editar">
                                                        <i class="icon far fa-edit lg"></i></a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="4"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="3">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
