<div class="col-8">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="fas fa-key"></i>
                Permissões
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-4">
                        {!! Form::label('permissions[]', 'Permissões', ['class' => '']) !!}
                        {!! Form::select('permissions[]', $permissions, old('permissions'), ['class' => 'form-control js-box', 'multiple']) !!}
                        @if($errors->has('permissions'))
                            <span class="text-danger">{{ $errors->first('permissions') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-4">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="fas fa-edit"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-4">
                        {!! Form::label('details', 'Permissão', ['class' => '']) !!}
                        {!! Form::text('details', old('details'), ['class' => 'form-control']) !!}
                        @if($errors->has('details'))
                            <span class="text-danger">{{ $errors->first('details') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="form-group mb-4">
                        {!! Form::label('name', 'Chave', ['class' => '']) !!}
                        {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12 text-right mb-2">
    <button type="submit"
            class="btn btn-icon icon-left btn-success btn-lg"><i
                class="fas fa-check"></i> Salvar
    </button>
</div>


