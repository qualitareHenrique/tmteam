<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('description', 'Descrição', ['class' => 'label-required']) !!}
                        {!! Form::text('description', $balanceRecord->descripiton ?? old('description'), ['class' => 'form-control']) !!}
                        @if($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('type', 'Tipo', ['class' => 'label-required']) !!}
                        {!! Form::select('type', enumOptions('balance_record'), $balanceRecord->type ?? old('type'), ['class' => 'form-control']) !!}
                        @if($errors->has('type'))
                            <span class="text-danger">{{ $errors->first('type') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('amount', 'Valor', ['class' => 'label-required']) !!}
                        {!! Form::number('amount', $balanceRecord->amount ?? old('amount'), ['class' => 'form-control', 'step' => '0.01', 'placeholder' => '0,00']) !!}
                        @if($errors->has('amount'))
                            <span class="text-danger">{{ $errors->first('amount') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('amount_paid', 'Valor pago', ['class' => '']) !!}
                        {!! Form::number('amount_paid', $balanceRecord->amount_paid ?? old('amount_paid'), ['class' => 'form-control', 'step' => '0.01', 'placeholder' => '0,00']) !!}
                        @if($errors->has('amount_paid'))
                            <span class="text-danger">{{ $errors->first('amount_paid') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('discount_amount', 'Desconto', ['class' => '']) !!}
                        {!! Form::number('discount_amount', $balanceRecord->discount_amount ?? old('discount'), ['class' => 'form-control', 'step' => '0.00', 'placeholder' => '0,00']) !!}
                        @if($errors->has('discount_amount'))
                            <span class="text-danger">{{ $errors->first('discount_amount') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('charge_fee', 'Taxas', []) !!}
                        {!! Form::number('charge_fee', $balanceRecord->charge_fee ?? old('charge_fee'), ['class' => 'form-control', 'step' => '0.01', 'placeholder' => '0,00']) !!}
                        @if($errors->has('charge_fee'))
                            <span class="text-danger">{{ $errors->first('charge_fee') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('due_date', 'Data de vencimento', ['class' => 'label-required']) !!}
                        {!! Form::text('due_date', !empty($balanceRecord->due_date) ? $balanceRecord->due_date : old('due_date'), ['class' => 'form-control js-datepicker']) !!}
                        @if($errors->has('due_date'))
                            <span class="text-danger">{{ $errors->first('due_date') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('payment_date', 'Data de pagamento', ['class' => '']) !!}
                        {!! Form::text('payment_date', !empty($balanceRecord->payment_date) ? $balanceRecord->payment_date : old('payment_date'), ['class' => 'form-control js-datepicker']) !!}
                        @if($errors->has('payment_date'))
                            <span class="text-danger">{{ $errors->first('payment_date') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('payment_type', 'Espécie de pagamento', ['class' => 'label-required']) !!}
                        {!! Form::select('payment_type', enumOptions('payment_type'), $balanceRecord->payment_type ?? old('payment_type'), ['class' => 'form-control']) !!}
                        @if($errors->has('payment_type'))
                            <span class="text-danger">{{ $errors->first('payment_type') }}</span>
                        @endif
                    </div>
                </div>

                @can('view_subsidiaries')
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-2">
                            {!! Form::label('subsidiary_id', 'Unidade', ['class' => 'label-required']) !!}
                            {!! Form::select('subsidiary_id', $subsidiaries, $balanceRecord->subsidiry_id ?? old('subsidiry_id'), ['class' => 'form-control']) !!}
                            @if($errors->has('subsidiary_id'))
                                <span class="text-danger">{{ $errors->first('subsidiary_id') }}</span>
                            @endif
                        </div>
                    </div>
                @else
                    {!! Form::hidden('subsidiary_id', Auth::user()->subsidiary_id) !!}
                @endcan

                @if(empty($balanceRecord))
                    <div class="col-lg-12 mt-0 mb-2">
                        <div class="section-title mt-2">Parcelamento</div>
                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-2">
                            {!! Form::label('repeat', 'Período', ['class' => 'label-required']) !!}
                            {!! Form::select('repeat', enumOptions('period_types'), old('repeat'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                            @if($errors->has('repeat'))
                                <span class="text-danger">{{ $errors->first('repeat') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group mb-2">
                            {!! Form::label('repeat_times', 'Parcelas', ['class' => 'label-required']) !!}
                            {!! Form::number('repeat_times', old('repeat_times'), ['class' => 'form-control', 'placeholder' => '0']) !!}
                            @if($errors->has('repeat_times'))
                                <span class="text-danger">{{ $errors->first('repeat_times') }}</span>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg"><i
                        class="fas fa-check"></i> Salvar
            </button>
        </div>
    </div>
</div>
