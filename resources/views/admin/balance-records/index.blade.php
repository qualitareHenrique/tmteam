@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Lançamentos</h1>
            @can('add_balance_records')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.balance-records.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('balance_records.index') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.balance-records.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-2 col-md-4 col-sm-12 mb-0 form-group">
                                {!! Form::text('name', Request::get('name'), ['class' => 'form-control', 'placeholder' => 'Aluno']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('situation', enumOptions('payment_situation'), Request::get('situation'), ['class' => 'form-control', 'title' => 'Situação']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('type', enumOptions('balance_record'), Request::get('type'), ['class' => 'form-control', 'title' => 'Tipo']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-1 form-group">
                                {!! Form::text('competence', Request::get('competence'), ['class' => 'form-control js-mask-monthyear', 'placeholder' => 'Competência']) !!}
                            </div>
                            @can('view_subsidiaries')
                                <div class="col-lg-2 col-md-2 col-sm-12 mb-1 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                            @endcan
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de lançamentos cadastrados</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Status</th>
                                            <th>Tipo</th>
                                            <th>Aluno</th>
                                            <th>Descrição</th>
                                            <th title="Valor cobrado - pago">Valor</th>
                                            <th title="Data de vencimento - pagamento">Data</th>
                                            <th>Ações</th>
                                        </tr>
                                        @forelse($records as $record)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">
                                                    @if(empty($record->payment_date))
                                                        @if(today()->gt($record->due_date))
                                                            <i title="Atrasado"
                                                               class="fas fa-exclamation-triangle text-danger"></i>
                                                        @else
                                                            <i title="Pedente"
                                                               class="fas fa-question-circle text-warning"></i>
                                                        @endif
                                                    @else
                                                        <i title="Pago"
                                                           class="fas fa-check-circle text-success"></i>
                                                    @endif
                                                </td>
                                                <td class="td-item">{{ enumOptions('balance_record')[$record->type] }}</td>
                                                <td class="td-item">{{ $record->chargeable->student->user->name ?? '' }}</td>
                                                <td class="td-item">{{ $record->description }}</td>
                                                <td class="td-item">{{ money_parse_by_decimal($record->amount, 'BRL') }}
                                                    - {{ money_parse_by_decimal($record->amount_paid ?? '0.00', 'BRL') }}</td>
                                                <td class="td-item">{{ $record->due_date->format('d/m/Y') }} - {{ $record->payment_date ? $record->payment_date->format('d/m/Y') : '##/##/####' }}</td>
                                                <td class="td-item text-center text-muted">

                                                    <div class="dropdown">
                                                        <a href="#"
                                                           role="button"
                                                           id="dropdownMenuLink"
                                                           data-toggle="dropdown"
                                                           data-boundary="window"
                                                           title="Ações"
                                                           aria-haspopup="true"
                                                           aria-expanded="false">
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </a>

                                                        <div class="dropdown-menu"
                                                             aria-labelledby="dropdownMenuLink">

                                                            @can('edit_balance_records')
                                                                <a href="{{ route('admin.balance-records.edit', ['record' => $record->id]) }}"
                                                                   title="Editar"
                                                                   class="dropdown-item">
                                                                    <i class="icon far fa-edit lg"></i>
                                                                    Editar
                                                                </a>
                                                            @endcan

                                                            @can('delete_balance_records')
                                                                <a href="#"
                                                                   data-link="{{ route('admin.balance-records.destroy', ['record' => $record->id]) }}"
                                                                   data-title="{{ $record->description }}"
                                                                   title="Excluir"
                                                                   class="dropdown-item js-confirm-delete">
                                                                    <i class="icon far fa-trash-alt lg"></i>
                                                                    Excluir
                                                                </a>
                                                            @endcan
                                                        </div>
                                                    </div>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="8"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $records->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="7">
                                                <div class="float-right">{!! $records->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

