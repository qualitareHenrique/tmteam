@extends('layouts.default')

@section('content')

    {!! Form::model($message, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.messages.update', 'id' => $message->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.messages.index') }}"
                   class="btn btn-icon" title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar mensagem</h1>
            {!! Breadcrumbs::render('messages.edit', $message) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.messages._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}
@endsection
