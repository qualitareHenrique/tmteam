@extends('layouts.default')

@section('content')

    {!! Form::model($subsidiary, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.subsidiaries.update', 'id' => $subsidiary->id], 'enctype' => 'multipart/form-data']) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.subsidiaries.index') }}" class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar unidade</h1>
            {!! Breadcrumbs::render('subsidiaries.edit', $subsidiary) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.subsidiaries._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
