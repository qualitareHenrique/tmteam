@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <div class="section-header-back">
                <a href="{{ route('admin.subsidiaries.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Documentos</h1>
            {!! Breadcrumbs::render('subsidiaries.index') !!}
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de documentos necessários</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Descrição</th>
                                            <th>Status</th>
                                        </tr>
                                        @foreach($results as $result)
                                            <tr class="tr-item td-border hover-click"
                                                onclick="{{ $result->approvalStatus != 'VERIFYING' ? 'openModal(this)' : ''  }}"
                                                data-link="{{ route('admin.subsidiaries.documents.store', ['id' => $subsidiary->id]) }}"
                                                data-document="{{ $result->id }}">
                                                <td class="td-item">{{ $result->description }}</td>
                                                <td class="td-item">{{ $result->approvalStatus }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade"
             tabindex="-1"
             role="dialog"
             id="documentModal">
            <div class="modal-dialog modal-dialog-centered"
                 role="document">
                <div class="modal-content">
                    {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'enctype' => 'multipart/form-data', 'id' => 'document-form']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">Enviar documento</h5>
                        <button type="button"
                                class="close"
                                data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row border p-4">
                            {{ Form::file('document', ['required' => true]) }}
                            {{ Form::hidden('document_id') }}
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal">Cancelar
                        </button>
                        <button type="submit"
                                class="btn btn-success">Enviar
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        function openModal(element) {
            let action = $(element).data('link');
            let document = $(element).data('document');

            $('input[name="document_id"]').val(document);
            $('form#document-form').attr('action', action);

            $('#documentModal').modal('toggle');
        }
    </script>
@endpush
