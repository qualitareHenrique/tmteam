@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'POST', 'novalidate',  'role' => 'form', 'class' => 'form', 'route' => 'admin.subsidiaries.store', 'enctype' => 'multipart/form-data', ]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.subsidiaries.index') }}" class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Nova unidade</h1>
            {!! Breadcrumbs::render('subsidiaries.create') !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.subsidiaries._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
