@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <h1>Categorias</h1>
            @can('add_students_submodules')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.age-categories.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('ages_categories.index') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.age-categories.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12 mb-0 form-group">
                                {!! Form::text('nome', Request::get('nome'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('status', [0 => 'Desativado', 1 => 'Ativo'], old(''), ['class' => 'form-control', 'title' => 'Status ']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de Categorias cadastrados</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-a">
                                        <th>Nome</th>
                                        <th>Faixa Etária</th>
                                        <th></th>
                                    </tr>
                                    @forelse($results as $result)
                                        <tr class="tr-item td-border">
                                            <td class="td-item">{{ $result->name }}</td>
                                            <td class="td-item">{{ $result->ageName }}</td>
                                            <td class="td-item text-right">

                                                @can('edit_bands_submodules')
                                                    <a href="{{ route('admin.age-categories.edit', ['id' => $result->id]) }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title=""
                                                       data-original-title="Editar">
                                                        <i class="icon far fa-edit lg"></i></a>
                                                @endcan

                                                @can('delete_bands_submodules')
                                                    <a href="#"
                                                       class="js-confirm-delete"
                                                       data-link="{{ route('admin.age-categories.destroy', ['id' => $result->id]) }}"
                                                       data-title="{{ $result->name }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title=""
                                                       data-original-title="Excluir">
                                                        <i class="icon far fa-trash-alt lg"></i></a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="3"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="2">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

