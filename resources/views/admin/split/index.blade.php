@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-4">
            <h1>Split</h1>
            {!! Breadcrumbs::render('reports.balance') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.split.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 mb-1 form-group">
                                {!! Form::text('competence', Request::get('competence'), ['class' => 'form-control js-monthpicker', 'title' => 'Competência']) !!}
                            </div>
                            @can('view_subsidiaries')
                                <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                            @endcan
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Relatório de split de pagamentos</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-a">
                                        <th>Competência</th>
                                        <th>Esperado</th>
                                        <th>Recebido</th>
                                    </tr>
                                    @forelse($reports as $report)
                                        <tr class="tr-item td-border">
                                            <td class="td-item">{{ "$report->competence_month/$report->competence_year" }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal($report->expected, 'BRL') }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal($report->received, 'BRL') }}</td>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="tr-item td-border">
                                            <td colspan="3"
                                                class="text-center">Nenhum
                                                registro encontrado
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $reports->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="9">
                                                <div class="float-right">{!! $reports->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

