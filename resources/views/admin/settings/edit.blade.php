@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.settings.update']]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <h1>Editar configurações</h1>
            {!! Breadcrumbs::render('settings.edit') !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                <i class="fas fa-cogs lga"></i>
                                Configurações
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 mt-0 mb-2">
                                    <div class="section-title">Financeiro</div>
                                </div>

                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group mb-4">
                                        {!! Form::label('fine', 'Multa por atraso', ['class' => 'label-required']) !!}
                                        <div class="input-group"
                                             data-toggle="tooltip"
                                             data-placement="top"
                                             data-original-title="Multa para pagamento após o vencimento">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-weight-bold">%</span>
                                            </div>
                                            {!! Form::number('fine', $settings['fine'], ['class' => 'form-control', 'step' => '.01']) !!}
                                        </div>
                                        @if($errors->has('fine'))
                                            <span class="text-danger">{{ $errors->first('fine') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group mb-4">
                                        {!! Form::label('interest', 'Juros ao mês', ['class' => 'label-required']) !!}
                                        <div class="input-group"
                                             data-toggle="tooltip"
                                             data-placement="top"
                                             data-original-title="O valor inserido é dividido pelo número de dias para cobrança de juros diária">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-weight-bold">%</span>
                                            </div>
                                            {!! Form::number('interest', $settings['interest'], ['class' => 'form-control', 'step' => '.01']) !!}

                                        </div>
                                        @if($errors->has('interest'))
                                            <span class="text-danger">{{ $errors->first('interest') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group mb-4">
                                        {!! Form::label('split_amount', 'Valor do split', ['class' => 'label-required']) !!}
                                        <div class="input-group"
                                             data-toggle="tooltip"
                                             data-placement="top"
                                             data-original-title="Valor a ser cobrado da unidade para cada boleto pago">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text font-weight-bold">%</span>
                                            </div>
                                            {!! Form::number('split_amount', $settings['split_amount'], ['class' => 'form-control', 'step' => '.01']) !!}
                                            @if($errors->has('split_amount'))
                                                <span class="text-danger">{{ $errors->first('split_amount') }}</span>
                                            @endif
                                            @if($errors->has('split_type'))
                                                <span class="text-danger">{{ $errors->first('split_type') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group mb-4">
                                        {!! Form::label('min_overdue_days', 'Período min para inadimplência', ['class' => 'label-required']) !!}
                                        <div class="input-group"
                                             data-toggle="tooltip"
                                             data-placement="top"
                                             data-original-title="Prazo mínino para emitir pedência por inadimplência">
                                            {!! Form::number('min_overdue_days', $settings['min_overdue_days'], ['class' => 'form-control']) !!}
                                            <div class="input-group-append">
                                                <span class="input-group-text font-weight-bold">dias</span>
                                            </div>
                                        </div>
                                        @if($errors->has('min_overdue_days'))
                                            <span class="text-danger">{{ $errors->first('min_overdue_days') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group mb-4">
                                        {!! Form::label('max_overdue_days', 'Período max de inadimplência', ['class' => 'label-required']) !!}
                                        <div class="input-group"
                                             data-toggle="tooltip"
                                             data-placement="top"
                                             data-original-title="Número máximo de dias permitido para pagamento após o vencimento. Também é utilizado como prazo de cancelamento da matrícula"
                                        >
                                            {!! Form::number('max_overdue_days', $settings['max_overdue_days'], ['class' => 'form-control']) !!}
                                            <div class="input-group-append">
                                                <span class="input-group-text font-weight-bold">dias</span>
                                            </div>
                                        </div>
                                        @if($errors->has('max_overdue_days'))
                                            <span class="text-danger">{{ $errors->first('max_overdue_days') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit"
                                    class="btn btn-icon icon-left btn-success btn-lg"><i
                                        class="fas fa-check"></i> Salvar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
