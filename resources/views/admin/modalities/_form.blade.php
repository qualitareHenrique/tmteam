<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
                        {!! Form::text('name', $modality->name ?? old('name'), ['class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('price', 'Preço', ['class' => 'label-required']) !!}
                        {!! Form::number('price', $modality->price ?? old('price'), ['class' => 'form-control', 'step'=> '.01', 'placeholder' => '0,00']) !!}
                        @if($errors->has('price'))
                            <span class="text-danger">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('active_graduation', 'Ativar gradução', ['class' => 'label-required']) !!}
                        {!! Form::select('active_graduation', [0 => 'Não', 1 => 'Sim'], !empty($modality->active_graduation), ['class' => 'form-control']) !!}
                        @if($errors->has('active_graduation'))
                            <span class="text-danger">{{ $errors->first('active_graduation') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg"><i
                        class="fas fa-check"></i> Salvar
            </button>
        </div>
    </div>
</div>
