@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Auditoria</h1>
            {!! Breadcrumbs::render('audits.index') !!}
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card mb-0">
                    <div class="card-body">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.audits.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::text('created_at', Request::get('created_at'), ['class' => 'form-control js-datepicker', 'placeholder' => 'Data']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 mb-0 form-group">
                                {!! Form::select('user', $users, Request::get('user'), ['class' => 'form-control', 'title' => 'Usuário']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('type', $types, Request::get('type'), ['class' => 'form-control', 'title' => 'Tipo']) !!}
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('event', $actions, Request::get('event'), ['class' => 'form-control', 'title' => 'Ação ']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-12">
                    <h6 class="mb-3">Listagem de moficações do sistema</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-item td-border">
                                            <th>Usuário</th>
                                            <th>Tipo</th>
                                            <th>Ação</th>
                                            <th>Horário</th>
                                            <th>Ações</th>
                                        </tr>
                                        @forelse($audits as $audit)
                                            <tr class="tr-item td-border">
                                                <td class="td-item">
                                                    {{ $audit->user->nickname ?? '' }}
                                                </td>
                                                <td class="td-item">{{ $audit->type_name }}</td>
                                                <td class="td-item">{{ $audit->action_name }}</td>
                                                <td class="td-item">{{ $audit->created_at->format('d/m/Y H:i') }}</td>
                                                <td class="no-wrap">
                                                    @can('view_audits')
                                                        <a href="{{ route('admin.audits.show', ['id' => $audit->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Visualizar">
                                                            <i class="icon far fa-eye lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="5"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border  bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $audits->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="4">
                                                <div class="float-right">{!! $audits->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
