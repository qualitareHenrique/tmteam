@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-0">
            <div class="section-header-back">
                <a href="{{ route('admin.audits.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Visualizar</h1>
            {!! Breadcrumbs::render('audits.show', $audit) !!}
        </div>
        <div class="clearfix"></div>
        <div class="section-body mt-4">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                <i class="fas fa-user mr-2 lga"></i>
                                Identificação
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="mb-2">
                                <table class="table table-md">
                                    <tbody>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Nome</b></td>
                                        <td class="td-item">
                                            {{ $audit->user->nickname ?? '' }}
                                        </td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>E-mail</b></td>
                                        <td class="td-item">
                                            {{ $audit->user->email ?? '' }}
                                        </td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Origem</b></td>
                                        <td class="td-item">{{ $audit->url }}</td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Data</b></td>
                                        <td class="td-item">
                                            {{ date('d/m/Y H:i:s', strtotime($audit->created_at)) }}
                                        </td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Ip</b></td>
                                        <td class="td-item">{{ $audit->ip_address }}</td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Tipo</b></td>
                                        <td class="td-item">{{ $audit->type_name }}</td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Ação</b></td>
                                        <td class="td-item">{{ $audit->action_name }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                <i class="fas fa-cog mr-2 lga"></i>
                                Modificação
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h6>Valores anteriores</h6>
                                    <div class="mb-2">
                                        <table class="table table-md">
                                            <tbody>
                                            @foreach($audit->old_values as $oldkey => $oldValue)
                                                <tr class="tr-item td-border">
                                                    <td class="td-item">
                                                        <b>{{ $oldkey }}</b>
                                                    </td>
                                                    <td class="td-item">
                                                        {{ $oldValue }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h6>Valores modidicados</h6>
                                    <div class="mb-2">
                                        <table class="table table-md">
                                            <tbody>
                                            @foreach($audit->new_values as $newKey => $newValue)
                                                <tr class="tr-item td-border">
                                                    <td class="td-item">
                                                        <b>{{ $newKey }}</b>
                                                    </td>
                                                    <td class="td-item">
                                                        {{ $newValue }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
