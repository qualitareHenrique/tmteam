@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Juno</h1>
            {!! Breadcrumbs::render('roles.index') !!}
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-12">
                    <h6 class="mb-3">Informaçeõs de balanço do gateway de pagamentos</h6>
                    <div class="card">
                        <div class="card-body">
                            <div class="invoice">
                                <div class="invoice-print">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div>
                                                        <strong>Conta de destino:</strong><br>
                                                        Banco: {{ $subsidiary->bank_number }}<br>
                                                        Agência: {{ $subsidiary->agency_number }}<br>
                                                        Conta: {{ $subsidiary->account_number }}<br>
                                                        Operação: {{ $subsidiary->account_complement_number ?? "-" }}
                                                    </div>
                                                </div>
                                                <div class="col-md-6  text-md-right">
                                                    <div>
                                                        <strong>Dados do titular:</strong><br>
                                                        Unidade {{ $subsidiary->name }}<br>
                                                        {{ $subsidiary->manager->name }}<br>
                                                        {{ maskPhone($subsidiary->manager->phone) }}<br>
                                                        {{ $subsidiary->manager->email }}<br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-12">
                                            <div class="section-title">Dados financeiros</div>
                                            <p class="section-lead">Valores obtidos em tempo real</p>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-md">
                                                    <tbody><tr>
                                                            <th data-width="40" style="width: 40px;">#</th>
                                                            <th>Item</th>
                                                            <th class="text-right">Totais</th>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Saldo retido</td>
                                                            <td class="text-right">{{ money_parse_by_decimal((string)$result->withheldBalance, 'BRL') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Saldo liberado</td>
                                                            <td class="text-right">{{ money_parse_by_decimal((string)$result->transferableBalance, 'BRL') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Saldo total</td>
                                                            <td class="text-right">{{ money_parse_by_decimal((string)$result->balance, 'BRL') }}</td>
                                                        </tr>
                                                    </tbody></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row-reverse">
                                    {{ Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.transfer.store']) }}
                                    <div class="form-group">
                                        <div class="input-group">
                                            {{ Form::number('requested_amount', $result->transferableBalance, ['class' => 'form-control', 'step' => '.1', "max" => $result->transferableBalance, 'required' => 'true']) }}
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-icon icon-left">
                                                    <i class="fas fa-piggy-bank"></i> Solicitar pagamento
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
