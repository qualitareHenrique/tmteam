<div class="modal fade" tabindex="-1" role="dialog" id="skipCheckModal">
    <div class="modal-dialog" role="document">
        {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form']) !!}
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Regularizar temporariamente</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group mb-2">
                            {!! Form::label('skip_checking_until', 'Liberar até', ['class' => 'label-required']) !!}
                            {!! Form::text('skip_checking_until', date('Y-m-d', strtotime('tomorrow + 1 day')), ['class' => 'form-control js-datepicker', 'required' => true]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Cancelar
                </button>
                <button type="submit" class="btn btn-success">Enviar</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@push('scripts')
    <script>
        $('#skipCheckModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var link = button.data('link');
            var modal = $(this);
            modal.find('form').attr('action', link);


            modal.find('#skip_checking_until').trigger('focus');
        })
    </script>
@endpush


