@extends('layouts.default')

@section('content')

    {!! Form::model($installment, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.students.installments.update', 'registration' => $registration->id, 'id' => $installment->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.students.installments.index', ['registration' => $registration->id]) }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar parcela</h1>
            {!! Breadcrumbs::render('students_installments.edit', $registration, $installment) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.students.installments._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
