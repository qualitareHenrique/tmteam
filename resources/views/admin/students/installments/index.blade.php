@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-3">
            <div class="section-header-back">
                <a href="{{ route('admin.students.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Parcelas</h1>
            @can('add_balance_records')
                <div class="section-header-button">
                    <a href="{{ route('admin.students.installments.create', ['registration' => $registration->id]) }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('students_installments.index', $registration) !!}
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de parcelas do aluno - {{ $registration->student->user->name }}</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-a">
                                        <th width="10"></th>
                                        <th>Descrição</th>
                                        <th>Tipo</th>
                                        <th>Valor</th>
                                        <th>Vencimento</th>
                                        <th></th>
                                    </tr>

                                    @forelse($records as $record)
                                        <tr class="tr-item td-border">
                                            <td class="td-item">
                                                @if(empty($record->payment_date))
                                                    @if(today()->gt($record->due_date))
                                                        <i class="fas fa-exclamation-triangle text-danger"></i>
                                                    @else
                                                        <i class="fas fa-question-circle text-warning"></i>
                                                    @endif
                                                @else
                                                    <i class="fas fa-check-circle text-success"></i>
                                                @endif
                                            </td>
                                            <td class="td-item">{{ $record->description }}</td>
                                            <td class="td-item">{{ $types[$record->payment_type] }}</td>
                                            <td class="td-item">{{ money_parse_by_decimal($record->amount, 'BRL') }}</td>
                                            <td class="td-item">{{ $record->due_date->format('d/m/Y') }}</td>
                                            <td class="td-item text-right">
                                                    @if(Gate::check('cancel_invoices') && \App\Enums\BalanceRecordStatusEnum::BILLED === $record->status)
                                                    <a onclick="cancelInvoice(this)"
                                                       data-link="{{ route('admin.juno.cancel', ['invoice' => $record->id]) }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="Cancelar Cobrança"
                                                       data-original-title="Cancelar Cobrança">
                                                        <i class="icon lg fas fa-ban lg"></i>
                                                    </a>
                                                    @endif

                                                @can('edit_balance_records')
                                                    <a href="{{ route('admin.students.installments.edit', ['regtistration' => $registration->id, 'id' => $record->id]) }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="Editar"
                                                       data-original-title="Editar">
                                                        <i class="icon far fa-edit lg"></i></a>
                                                @endcan

                                                @can('delete_balance_records')
                                                    <a href="#"
                                                       class="js-confirm-delete"
                                                       data-link="{{ route('admin.students.installments.destroy', ['regtistration' => $registration->id, 'id' => $record->id]) }}"
                                                       data-title="{{ $record->description }}"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       data-original-ti
                                                       title="Excluir">
                                                        <i class="icon far fa-trash-alt lg"></i></a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="tr-item td-border">
                                            <td colspan="5"
                                                class="td-item text-center">
                                                Nenhum registro encontrado
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $records->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="4">
                                                <div class="float-right">{!! $records->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                    @can('resend_overdues')
                        <div class="col-lg-12 col-sm-6 text-right mb-2">
                            <button
                                    onclick="resendOverdues(this, {{ $registration->student->id }}, {{ $resendTypes::WHATSAPP }}, {{ $registration->student->user->phone }})"
                                    data-link="{{ route('admin.reports.resend-overdues') }}"
                                    type="button" class="btn btn-icon icon-left btn-success btn-lg">
                                    Reenviar Whatsapp
                            </button>
                            <button
                                    onclick="resendOverdues(this, {{ $registration->student->id }}, {{ $resendTypes::EMAIL }})"
                                    data-link="{{ route('admin.reports.resend-overdues') }}"
                                    type="button" class="btn btn-icon icon-left btn-warning btn-lg">
                                    Reenviar Email
                            </button>
                        </div>
                    @endcan

                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script>
    function cancelInvoice(element) {
        swal({
            title: 'Deseja realmente cancelar essa cobrança?',
            type: 'warning',
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            showCancelButton: true,
            confirmButtonColor: '#3f51b5',
            cancelButtonColor: '#ff4081',
        }).then((result) => {
        if (result.value) {
            $.ajax({
            type: "POST",
            url: $(element).data('link'),
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
        dataType: 'json',
        success: function (data) {
        swal('Sucesso!', data.success, 'success');

        setTimeout(function () {
        window.location.reload();
        }, 500);
        },
        error: function (data) {
        swal('Falha na solicitação', data.error, 'error');
        }
        });
        return false;
        }
        swal('Ação cancelada', 'a cobrança não foi cancelada.', 'warning');
        });
        }
</script>
@endpush


