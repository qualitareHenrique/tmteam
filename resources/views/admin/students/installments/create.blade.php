@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.students.installments.store', 'registration' => $registration->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.students.installments.index', ['registration' => $registration->id]) }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Nova parcela</h1>
            {!! Breadcrumbs::render('students_installments.create', $registration) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.students.installments._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
