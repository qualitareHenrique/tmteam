@extends('layouts.print')

@section('content')

    <style>
        @media print{@page {size: landscape}}
    </style>

    <div style="background-image: url('/img/certificate.png'); background-repeat: no-repeat;">
        <div style="width: 1169px; height: 826px; padding: 280px 100px">
            <p style="font-size: 36px; padding: 0 50px; text-align: center; line-height: 1.0;">
                A Academia Checkmat Geisel Brazilian Jiu-Jitsu, certifica que o(a) atleta
            </p>
            <p style="font-size: 48px; text-align: center; line-height: 1.6;">
                {{ $registration->student->user->name }}
            </p>
            <p style="font-size: 36px; padding: 0 50px; text-align: center; line-height: 1.0;">
                encontra-se apto(a) ao uso da <b>Faixa {{ $graduation->band->name }}</b>
            </p>
            <p style="font-size: 36px; padding: 0 5px; text-align: center; line-height: 1.0;">
                por reconhecimento e mérito em {{ $graduation->graduated_on->formatLocalized('%d de %B de %Y') }}
            </p>
        </div>
    </div>

@endsection