@if($errors->any())
    @php toast()->error('Falha ao registrar graduação'); @endphp
@endif

<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::hidden('registration_id', $registration->id) !!}
                        {!! Form::label('modality_id', 'Modalidade', ['class' => 'label-required']) !!}
                        {!! Form::select('modality_id', $modalities, $graduation->modality_id ?? old('modality_id'),
                                                        ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('modality_id'))
                            <span class="text-danger">{{ $errors->first('modality_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('band_id', 'Faixa', ['class' => 'label-required']) !!}
                        {!! Form::select('band_id', $bands->pluck('name', 'id'),
                                                    $graduation->band_id ?? old('band_id'),
                                                    ['class' => 'form-control', 'title' => 'Selecione'],
                                                    bandsToDataAttribute($bands)) !!}
                        @if($errors->has('band_id'))
                            <span class="text-danger">{{ $errors->first('band_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('degree_id', 'Grau', ['class' => 'label-required']) !!}
                        {!! Form::select('degree_id', $degrees->pluck('name', 'id'),
                                                    $graduation->degree_id ?? old('degree_id'),
                                                    ['class' => 'form-control', 'title' => 'Selecione'],
                                                    degreesToDataAttribute($degrees)) !!}
                        @if($errors->has('degree_id'))
                            <span class="text-danger">{{ $errors->first('degree_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('teacher_id', 'Professor', ['class' => '']) !!}
                        {!! Form::select('teacher_id', $teachers,
                                                    $graduation->teacher_id ?? old('teacher_id'),
                                                    ['class' => 'form-control', 'title' => 'Selecione']) !!}
                        @if($errors->has('teacher_id'))
                            <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('lessons_taken', 'Aulas assistidas', ['class' => 'label-required']) !!}
                        {!! Form::number('lessons_taken', $graduation->lessons_taken ?? old('lessons_taken'),
                                                          ['class' => 'form-control'] ) !!}
                        @if($errors->has('lessons_taken'))
                            <span class="text-danger">{{ $errors->first('lessons_taken') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('graduated_on', 'Data de graduação', ['class' => 'label-required']) !!}
                        {!! Form::text('graduated_on', !empty($graduation->graduated_on) ? $graduation->graduated_on : old('graduated_on'),
                                                       ['class' => 'form-control js-datepicker'] ) !!}
                        @if($errors->has('graduated_on'))
                            <span class="text-danger">{{ $errors->first('lessons_taken') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg"><i
                        class="fas fa-check"></i> Salvar
            </button>
        </div>
    </div>
</div>

@push('scripts')
    <script type="application/javascript">
        filterBands();

        $('#modality_id').on('change', function () {
            filterBands(true);
        });

        $('#band_id').on('change', function () {
            filterDegrees(true);
        });

        function filterBands(clearSelected = false){
            var field = $('#modality_id');
            var modality = parseInt(field.val());

            $('#band_id>option').each(function () {

                var band_modality = parseInt($(this).data('modality'));

                if(!isNaN(band_modality)){
                    $(this).toggle(band_modality === modality);
                }

                if (clearSelected) {
                    this.selected = false;
                }
            });

            setTimeout(function (){
                $('#band_id').selectpicker('refresh');
                filterDegrees();
            }, 200);
        }

        function filterDegrees(clearSelected = false)
        {
            var field = $('#band_id');
            var band = parseInt(field.val());

            $('#degree_id>option').each(function () {

                var degree_band = parseInt($(this).data('band'));

                if(!isNaN(degree_band)){
                    $(this).toggle(degree_band === band);
                }

                if (clearSelected) {
                    this.selected = false;
                }
            });

            setTimeout(function (){
                $('#degree_id').selectpicker('refresh');
            }, 200);
        }
    </script>
@endpush
