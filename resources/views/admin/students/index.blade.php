@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-4">
            <h1>Alunos</h1>
            @can('add_students')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.students.create') }}"
                       class="btn btn-success btn-icon btn-lg"
                       title="Adicionar"><i class="fas fa-plus"></i>
                        Adicionar</a>
                </div>
            @endcan
            @can('view_students')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.students.export') }}"
                       class="btn btn-primary btn-icon btn-lg"
                       title="Exportar"><i class="fas fa-file-excel fa-2x"></i>
                        Gerar excel</a>
                </div>
            @endcan
            {!! Breadcrumbs::render('students.index') !!}
        </div>
        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.students.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-md-6 col-sm-12 mb-0 form-group">
                                {!! Form::text('name', Request::get('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('status', enumOptions('registration_status'), Request::get('status'), ['class' => 'form-control', 'title' => 'Situação ']) !!}
                            </div>
                            <div class="col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade ']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de alunos cadastrados</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-a">
                                            <th>Nome</th>
                                            <th class="text-center">Categoria
                                            </th>
                                            <th class="text-center">Usuário</th>
                                            <th class="text-center">Unidade</th>
                                            <th class="text-center">Ações</th>
                                        </tr>
                                        @forelse($students as $student)
                                            <tr class="tr-item td-border">
                                                <td class="no-wrap media p-4"
                                                    style="min-height: 100px">
                                                    <img class="border-100 mr-3 rounded-circle"
                                                        src="{{ $student->user->avatarUrl }}"
                                                         alt="{{ $student->user->name }}"
                                                         width="50">
                                                    <div class="media-body">
                                                        <div class="media-title">
                                                            {{ $student->user->name }}
                                                        </div>
                                                        <div>
                                                            {!! registrationStatusBadge($student->registration->status ?? 0) !!}

                                                            @if(!empty($avatar = $student->user->getFirstMedia('avatar')))
                                                                <a href="{{ route('admin.students.attachments.show', ['media' => $avatar->id]) }}"
                                                                   data-toggle="tooltip"
                                                                   data-placement="top"
                                                                   title="Validar avatar"
                                                                   data-original-title="Validar anexo">
                                                                    <span class="badge {{ mediaValidationClass($avatar->validation_status) }}"><i class="fas fa-file-image"></i> Avatar</span>
                                                                </a>
                                                            @endif

                                                            @if(!empty($student->registration) && !empty($fisic = $student->registration->getFirstMedia('fisic')))
                                                                <a href="{{ route('admin.students.attachments.show', ['media' => $fisic->id]) }}"
                                                                   data-toggle="tooltip"
                                                                   data-placement="top"
                                                                   title="Validar atestado médico"
                                                                   data-original-title="Validar anexo">
                                                                    <span class="badge {{ mediaValidationClass($fisic->validation_status) }}"><i class="fas fa-file-medical-alt"></i> Atestado</span>
                                                                </a>
                                                            @endif
                                                        </div>

                                                    </div>
                                                </td>
                                                <td class="text-center">{{ $student->ageCategory->name ?? '' }}</td>
                                                <td class="text-center">{!! isActiveBadge($student->user->active) !!}</td>
                                                <td class="text-center">{{ $student->registration->subsidiary->name ?? '' }}</td>
                                                <td class="text-center text-muted">

                                                    <div class="dropdown">
                                                        <a href="#"
                                                           role="button"
                                                           id="dropdownMenuLink"
                                                           data-toggle="dropdown"
                                                           data-boundary="window"
                                                           title="Ações"
                                                           aria-haspopup="true"
                                                           aria-expanded="false">
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </a>

                                                        <div class="dropdown-menu"
                                                             aria-labelledby="dropdownMenuLink">

                                                            @can('edit_students')
                                                                <a href="{{ route('admin.students.edit', ['id' => $student->id]) }}"
                                                                   title="Editar"
                                                                   class="dropdown-item">
                                                                    <i class="icon lg far fa-edit"></i>
                                                                    Editar
                                                                </a>
                                                            @endcan

                                                            @if(!empty($student->registration))

                                                                @can('view_balance_records')
                                                                    <a href="{{ route('admin.students.installments.index', ['registration' => $student->registration->id]) }}"
                                                                       title="Parcelas"
                                                                       class="dropdown-item">
                                                                        <i class="icon lg fas fa-file-invoice"></i>
                                                                        Parcelas
                                                                    </a>
                                                                @endcan

                                                                @can('view_graduations')
                                                                    <a href="{{ route('admin.students.graduations.index', ['registration' => $student->registration->id]) }}"
                                                                       title="Graduações"
                                                                       class="dropdown-item">
                                                                        <i class="icon lg fas fa-medal"></i>
                                                                        Graduações
                                                                    </a>
                                                                @endcan

                                                                @can('view_students')
                                                                    <a href="{{ route('admin.students.issues.index', ['registration' => $student->registration->id]) }}"
                                                                       title="Pendências"
                                                                       class="dropdown-item">
                                                                        <i class="icon lg fas fa-exclamation-triangle"></i>
                                                                        Pendências
                                                                    </a>
                                                                @endcan

                                                                @can('edit_students')
                                                                    <a href="#"
                                                                       data-link="{{ route('admin.students.skip-check', ['registration' => $student->registration->id]) }}"
                                                                       data-toggle="modal"
                                                                       data-target="#skipCheckModal"
                                                                       title="Regularização parcial"
                                                                       class="dropdown-item">
                                                                        <i class="icon lg fas fa-user-check"></i>
                                                                        Regularização
                                                                        parcial
                                                                    </a>
                                                                    @if(!empty($student->registration) && !empty($fisic = $student->registration->getFirstMedia('fisic')))
                                                                        <a href="#"
                                                                           data-link="{{ route('admin.students.attachments.update', ['id' => $fisic->id]) }}"
                                                                           data-toggle="modal"
                                                                           data-target="#startDateModal"
                                                                           title="Data de atestado"
                                                                           class="dropdown-item">
                                                                            <i class="icon lg fas fa-calendar-alt"></i>
                                                                            Data de atestado
                                                                        </a>
                                                                    @endif
                                                                @endcan

                                                                @if(Gate::check('deactivate_registrations') && $student->registration->status !== $status::CANCELLED)
                                                                    <a data-link="{{ route('admin.students.deactivate-registration', ['registration' => $student->registration->id]) }}"
                                                                       data-toggle="modal" title="Desativar Matrícula" class="dropdown-item student-deactivate-registration" href="#">
                                                                        <i class="icon lg fas fa-ban"></i>
                                                                        Desativar Matrícula
                                                                    </a>
                                                                @endif

                                                                @if(Gate::check('reactivate_registrations') && $student->registration->status === $status::CANCELLED)
                                                                    <a data-link="{{ route('admin.students.reactivate-registration', ['registration' => $student->registration->id]) }}"
                                                                       data-toggle="modal" title="Reativar Matrícula" class="dropdown-item student-reactivate-registration" href="#">
                                                                        <i class="icon lg fas fa-check"></i>
                                                                        Reativar Matrícula
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="6"
                                                    class="text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $students->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="6">
                                                <div class="float-right">{!! $students->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('admin.students._skip-cheking')

    @include('admin.students._start-date')
@endsection

