@extends('layouts.default')

@section('content')

    {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.students.store', 'enctype' => 'multipart/form-data']) !!}
    <section class="section">
        <div class="section-header mb-0">
            <div class="section-header-back">
                <a href="{{ route('admin.students.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Novo Aluno</h1>
            {!! Breadcrumbs::render('students.create') !!}
        </div>

        <div class="section-options">
            <div class="text-right mb-2">
                <label class="mt-2 mr-2">
                    {!! Form::hidden('experimental', 0) !!}
                    {!! Form::checkbox('experimental', true, $student->experimental ?? false, ['class' => 'custom-switch-input', 'id' => 'experimental']) !!}
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Aula Experimental?</span>
                </label>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.students._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

    @include('admin.students._form-guardian')

@endsection
