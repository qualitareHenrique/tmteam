<table>
    <thead>
    <tr>
        <th><b>Nome</b></th>
        <th><b>Telefone</b></th>
        <th><b>Status</b></th>
        <th><b>Responsável</b></th>
        <th><b>Responsável - Telefone</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
        <tr>
            <td>{{ $student->user->name  }}</td>
            <td>{{ $student->user->phone  }}</td>
            <td> @if($student->registration)
                    {{ $student->registration->status == 1 ? "Regular" : '' }}
                    {{ $student->registration->status == 2 ? "Pendencia" : '' }}
                    {{ $student->registration->status == 3 ? "Cancelado em ".date('d/m/Y', strtotime($student->registration->updated_at)) : '' }}
                @else
                     Não matriculado
                 @endif
            </td>
            <td>{{ $student->guardian ? $student->guardian->user->name : ''  }}</td>
            <td>{{ $student->guardian ? $student->guardian->user->phone : ''  }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
