<div class="row">
    <div class="col-lg-12 mt-0 mb-2">
        <div class="section-title mt-2">Endereço</div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="form-group mb-4">
            {!! Form::label('user[cep]', 'CEP', ['class' => '']) !!}
            {!! Form::text('user[cep]', old('user[cep]'), ['class' => 'form-control js-mask-cep cep']) !!}
            @if($errors->has('user.cep'))
                <span class="text-danger">{{ $errors->first('user.cep') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-9">
        <div class="form-group mb-4">
            {!! Form::label('user[street]', 'Rua', ['class' => '']) !!}
            {!! Form::text('user[street]', old('user[street]'), ['class' => 'form-control rua']) !!}
            @if($errors->has('user.street'))
                <span class="text-danger">{{ $errors->first('user.street') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-5">
        <div class="form-group mb-4">
            {!! Form::label('user[neighborhood]', 'Bairro', ['class' => '']) !!}
            {!! Form::text('user[neighborhood]', old('user[neighborhood]'), ['class' => 'form-control bairro']) !!}
            @if($errors->has('user.neighborhood'))
                <span class="text-danger">{{ $errors->first('user.neighborhood') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group mb-2">
            {!! Form::label('user[number]', 'Número', ['class' => '']) !!}
            {!! Form::text('user[number]', old('user[number]'), ['class' => 'form-control numero']) !!}
            @if($errors->has('user.number'))
                <span class="text-danger">{{ $errors->first('user.number') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-5">
        <div class="form-group mb-4">
            {!! Form::label('user[complement]', 'Complemento', ['class' => '']) !!}
            {!! Form::text('user[complement]', old('user[complement]'), ['class' => 'form-control complemento']) !!}
            @if($errors->has('user.complement'))
                <span class="text-danger">{{ $errors->first('user.complement') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="form-group mb-2">
            {!! Form::label('user[city]', 'Cidade', ['class' => '']) !!}
            {!! Form::text('user[city]', old('user[city]'), ['class' => 'form-control cidade']) !!}
            @if($errors->has('user.city'))
                <span class="text-danger">{{ $errors->first('user.city') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group mb-2">
            {!! Form::label('user[state]', 'Estado', ['class' => '']) !!}
            {!! Form::text('user[state]', old('user[state]'), ['class' => 'form-control uf']) !!}
            @if($errors->has('user.state'))
                <span class="text-danger">{{ $errors->first('user.state') }}</span>
            @endif
        </div>
    </div>
</div>
