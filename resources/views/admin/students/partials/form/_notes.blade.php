<div class="row">
    <div class="col-lg-12 mt-0 mb-2">
        <div class="section-title mt-2">Informações</div>
    </div>
     <div class="col-sm-12 col-md-6">
        <div class="form-group mb-4">
            {!! Form::label('user[reason]', 'Motivo') !!}
            {!! Form::select('user[reason]', ['' => 'Selecione',
                                              'Entrar em forma' => 'Entrar em forma',
                                              'Defesa pessoal' => 'Defesa pessoal',
                                              'Auto estima' => 'Auto estima',
                                              'Fazer Amigos' => 'Fazer Amigos',
                                              'Praticar atividade física' => 'Praticar atividade física',
                                              ],
                                              null,
                                              ['class' => 'form-control']) !!}
            @if($errors->has('user[reason]'))
                <span class="text-danger">{{ $errors->first('user[reason]') }}</span>
            @endif
        </div>
     </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group mb-4">
            {!! Form::label('user[remark]', 'Observações', ['class' => '']) !!}
            {!! Form::textarea('user[remark]', old('user[remark]'), ['class' => 'form-control motivo']) !!}
            @if($errors->has('user.remark'))
                <span class="text-danger">{{ $errors->first('user.reason') }}</span>
            @endif
        </div>
    </div>

</div>
