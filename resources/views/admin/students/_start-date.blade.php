<div class="modal fade" tabindex="-1" role="dialog" id="startDateModal">
    <div class="modal-dialog" role="document">
        {!! Form::open(['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form']) !!}

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data de atestado</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group mb-2">
                            {!! Form::label('start_date', 'Selecione', ['class' => 'label-required']) !!}
                            {!! Form::text('start_date', old('start_date'), ['class' => 'form-control js-datepicker', 'required' => true]) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Cancelar
                </button>
                <button type="submit" class="btn btn-success">Enviar</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

@push('scripts')
    <script>
        $('#startDateModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var link = button.data('link');
            var modal = $(this);
            modal.find('form').attr('action', link);

            modal.find('#start_date').trigger('focus');
        })
    </script>
@endpush


