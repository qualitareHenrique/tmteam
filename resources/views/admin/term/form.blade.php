@extends('layouts.public')

@section('content')

    {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.users.accept-term', 'method' => 'POST']) !!}
    <div class="box">
        <h5 class="box-title">
            O não aceite de algum dos termos abaixo estará autorizado a suspensão dos treinos até sua devida regularização:
        </h5>

        <div class="col-sm-12 col-md-12">
            <div class="form-group mb-12">
                <input class="input_box" name="termo_de_uso" type="checkbox"/>
                <h7>Li o contrato e aceito os termos de uso.</h7>
            </div>
        </div>

        <div class="card-footer text-center">
            <button type="submit"
                    class="btn btn-icon icon-left btn-success btn-lg">
                <i class="fas fa-check"></i> Confirmar
            </button>
        </div>
    </div>
    {!! Form::close() !!}

@endsection
