<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <i class="far fa-edit lga"></i>
                Detalhes
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('teacher_name', 'Professor') !!}
                        {!! Form::text('teacher_name', $teacher->user->name ?? old('teacher'), ['disabled' => 'disabled', 'class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-4">
                        {!! Form::label('classroom_name', 'Turma') !!}
                        {!! Form::text('classroom_name', $classroom->name ?? old('classroom_name'), ['disabled' => 'disabled', 'class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group mb-2">
                        {!! Form::label('split_value', 'Split', ['class' => 'label-required']) !!}
                        <div 
                            class="input-group"
                            data-toggle="tooltip"
                            data-placement="top"
                        >
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">%</span>
                        </div>
                        {!! Form::number('split_value', $classroomTeacher->split_value ?? old('split_value'), ['class' => 'form-control', 'step'=> '.01', 'placeholder' => '0,00']) !!}
                        @if($errors->has('price'))
                            <span class="text-danger">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="card-footer text-right">
                <button type="submit"
                        class="btn btn-icon icon-left btn-success btn-lg">
                    <i class="fas fa-check"></i> Salvar
                </button>
            </div>
        </div>
    </div>
</div>
</div>
