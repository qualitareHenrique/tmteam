@extends('layouts.default')

@section('content')

    {!! Form::model($classroomTeacher, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.classrooms-teachers.update', 'id' => $classroomTeacher->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.classrooms.edit', ['id' => $classroom->id]) }}"
                   class="btn btn-icon" title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Professor Turma</h1>
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.classrooms-teachers._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
