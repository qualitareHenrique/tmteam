<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4>
                    <i class="fas fa-key lga"></i>
                    Usuário
                </h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-2 bg-whitesmoke pt-4 pb-4">
                        <ul class="nav nav-pills flex-column" id="tabs" role="tablist">
                            <li class="nav-item mb-2">
                                <a class="nav-link active show" id="access-tab" data-toggle="tab" href="#access-fields" role="tab" aria-controls="access" aria-selected="true">Acesso</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile-fields" role="tab" aria-controls="profile" aria-selected="false">Perfil</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-10">
                        <div class="tab-content no-padding" id="tabs-content">
                            <div class="tab-pane fade active show" id="access-fields" role="tabpanel" aria-labelledby="access-tab">
                                @include('admin.users.partials._access-fields')
                            </div>
                            <div class="tab-pane fade" id="profile-fields" role="tabpanel" aria-labelledby="profile-tab">
                                @include('admin.users.partials._profile-fields')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12 text-right">
    <button type="submit"
            class="btn btn-icon icon-left btn-success btn-lg"><i
                class="fas fa-check"></i> Salvar
    </button>
</div>

