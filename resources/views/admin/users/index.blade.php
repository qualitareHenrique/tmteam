@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Usuários</h1>
            @can('add_users')
                <div class="section-header-button mr-2">
                    <a href="{{ route('admin.users.create') }}"
                       class="btn btn-success btn-icon btn-lg btn-success"
                       title="Adicionar"> <i class="fas fa-plus"></i> Adicionar</a>
                </div>
            @endcan

            {!! Breadcrumbs::render('users.index') !!}
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.users.index', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 mb-0 form-group">
                                {!! Form::text('email', Request::get('email'), ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::select('status', [0 => 'Desativado', 1 => 'Ativo'], Request::get('status'), ['class' => 'form-control', 'title' => 'Status ']) !!}
                            </div>
                            @can('view_subsidiaries')
                                <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                            @endcan
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <button type="button"
                                        class="btn btn-icon btn-block btn-danger js-clear">
                                    <i class="fas fa-ban lg-icon"></i>
                                </button>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-12">
                    <h6 class="mb-3">Listagem de usuários cadastrados</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                        <tr class="tr-item td-border">
                                            <th>Apelido</th>
                                            <th>E-mail</th>
                                            <th>Unidade</th>
                                            <th>Último acesso</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                        @forelse($results as $result)
                                            <tr class="tr-item td-border">
                                                <td class="td-item no-wrap"><img
                                                            class="border-100"
                                                            src="{{ $result->avatarUrl }}"
                                                            alt="{{ $result->nickname }}"
                                                            width="25"> {{$result->nickname }}
                                                </td>
                                                <td class="td-item">{{ $result->email }}</td>
                                                <td class="td-item">{{ $result->subsidiary->name }}</td>
                                                <td class="td-item">{{ $result->last_login }}</td>
                                                <td class="td-item">{!! isActiveBadge($result->active) !!}</td>
                                                <td class="no-wrap text-right">
                                                    @can('edit_users')
                                                        <a href="{{ route('admin.users.sendEmail', ['id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Reenviar e-mail">
                                                            <i class="icon fas fa-undo lg"></i></a>
                                                    @endcan

                                                    @if($result->active)
                                                        @can('edit_users')
                                                            <a href="{{ route('admin.users.deactivate', ['id' => $result->id]) }}"
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               title=""
                                                               data-original-title="Desativar">
                                                                <i class="icon fas fa-ban lg"></i></a>
                                                        @endcan
                                                    @else
                                                        @can('edit_users')
                                                            <a href="{{ route('admin.users.activate', ['id' => $result->id]) }}"
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               title=""
                                                               data-original-title="Ativar">
                                                                <i class="icon fas  fa-check lg"></i></a>
                                                        @endcan
                                                    @endif

                                                    @can('edit_users')
                                                        <a href="{{ route('admin.users.edit', ['id' => $result->id]) }}"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title=""
                                                           data-original-title="Editar">
                                                            <i class="icon far fa-edit lg"></i></a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="6"
                                                    class="td-item text-center">
                                                    Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $results->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="5">
                                                <div class="float-right">{!! $results->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
