<div class="row">
    <div class="col-12 mt-0 mb-2">
        <div class="section-title">Dados pessoais</div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('cpf', 'CPF', ['class' => 'label']) !!}
            {!! Form::text('cpf', $user->cpf ?? old('cpf'), ['class' => 'form-control js-mask-cpf']) !!}
            @if($errors->has('cpf'))
                <span class="text-danger">{{ $errors->first('cpf') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group mb-4">
            {!! Form::label('name', 'Nome', ['class' => 'label']) !!}
            {!! Form::text('name', $user->name ?? old('person[name]'), ['class' => 'form-control']) !!}
            @if($errors->has('person.name'))
                <span class="text-danger">{{ $errors->first('person.name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-2">
            {!! Form::label('sex', 'Sexo', ['class' => 'label']) !!}
            {!! Form::select('sex', [1 => 'Masculino', 2 => 'Feminino'], $user->sex ?? old('sex'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
            @if($errors->has('sex'))
                <span class="text-danger">{{ $errors->first('sex') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-2">
            {!! Form::label('phone', 'Telefone', ['class' => 'label']) !!}
            {!! Form::text('phone',  $user->phone ?? old('phone'), ['class' => 'form-control js-mask-phone']) !!}
            @if($errors->has('phone'))
                <span class="text-danger">{{ $errors->first('phone') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-2">
            {!! Form::label('birthdate', 'Data de Nascimento', ['class' => 'label']) !!}
            {!! Form::text('birthdate', !empty($teacher) ? $user->birthdate : old('birthdate'), ['class' => 'form-control js-datepicker']) !!}
            @if($errors->has('birthdate'))
                <span class="text-danger">{{ $errors->first('birthdate') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 mt-0 mb-2">
        <div class="section-title">Endereço</div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="form-group mb-4">
            {!! Form::label('cep', 'CEP', ['class' => '']) !!}
            {!! Form::text('cep', $user->cep ?? old('cep'), ['class' => 'form-control js-mask-cep cep']) !!}
            @if($errors->has('cep'))
                <span class="text-danger">{{ $errors->first('cep') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-9">
        <div class="form-group mb-4">
            {!! Form::label('street', 'Rua', ['class' => '']) !!}
            {!! Form::text('street', $user->street ?? old('street'), ['class' => 'form-control rua']) !!}
            @if($errors->has('street'))
                <span class="text-danger">{{ $errors->first('street') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-5">
        <div class="form-group mb-4">
            {!! Form::label('neighborhood', 'Bairro', ['class' => '']) !!}
            {!! Form::text('neighborhood', $user->neighborhood ?? old('neighborhood'), ['class' => 'form-control bairro']) !!}
            @if($errors->has('neighborhood'))
                <span class="text-danger">{{ $errors->first('neighborhood') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group mb-2">
            {!! Form::label('number', 'Número', ['class' => '']) !!}
            {!! Form::text('number', $user->number ?? old('number'), ['class' => 'form-control']) !!}
            @if($errors->has('number'))
                <span class="text-danger">{{ $errors->first('number') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-5">
        <div class="form-group mb-4">
            {!! Form::label('complement', 'Complemento', ['class' => '']) !!}
            {!! Form::text('complement', $user->complement ?? old('complement'), ['class' => 'form-control']) !!}
            @if($errors->has('person.complement'))
                <span class="text-danger">{{ $errors->first('person.complement') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="form-group mb-2">
            {!! Form::label('city', 'Cidade', ['class' => '']) !!}
            {!! Form::text('city', $user->city ?? old('city'), ['class' => 'form-control cidade']) !!}
            @if($errors->has('person.city'))
                <span class="text-danger">{{ $errors->first('person.city') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group mb-2">
            {!! Form::label('state', 'Estado', ['class' => '']) !!}
            {!! Form::text('state', $user->state ?? old('state'), ['class' => 'form-control uf']) !!}
            @if($errors->has('state'))
                <span class="text-danger">{{ $errors->first('state') }}</span>
            @endif
        </div>
    </div>
</div>


