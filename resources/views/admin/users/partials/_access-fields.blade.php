<div class="row">
    <div class="col-12 mt-0 mb-2">
        <div class="section-title">Dados de acesso</div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('nickname', 'Apelido', ['class' => 'label-required']) !!}
            {!! Form::text('nickname', $user->nickname ?? old('nickname'), ['class' => 'form-control']) !!}
            @if($errors->has('nickname'))
                <span class="text-danger">{{ $errors->first('nickname') }}</span>
            @endif
        </div>
    </div>

    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('email', 'E-mail', ['class' => 'label-required']) !!}
            {!! Form::text('email', $user->email ?? old('email'), ['class' => 'form-control']) !!}
            @if($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>
    </div>

    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('avatar', 'Foto do usuário', []) !!}
            <div class="file-upload-wrapper">
                {!! Form::file('avatar', ['class' => 'file-upload-field']) !!}
            </div>
            @if($errors->has('avatar'))
                <span class="text-danger">{{ $errors->first('avatar') }}</span>
            @endif
        </div>
    </div>

    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('password', 'Senha', ['class' => 'label-required']) !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
            @if($errors->has('password'))
                <span class="text-danger">{{ $errors->first('password') }}</span>
            @endif
        </div>
    </div>

    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('password_confirmation', 'Confirmar senha', ['class' => 'label-required']) !!}
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
            @if($errors->has('password_confirmation'))
                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>
    </div>

    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-0 d-flex h-100">
            <label class="align-self-center">
                {!! Form::hidden('receive_messages', 0) !!}
                {!! Form::checkbox('receive_messages', true, $user->receive_messages ?? false, ['class' => 'custom-switch-input', 'id' => 'receive_messages']) !!}
                <span class="custom-switch-indicator"></span>
                <span class="custom-switch-description">Receber mensagem?</span>
            </label>
        </div>
    </div>

    @can('view_subsidiaries')
        <div class="col-sm-12 col-md-4">
            <div class="form-group mb-4">
                {!! Form::label('subsidiary_id', 'Unidade', ['class' => 'label-required']) !!}
                {!! Form::select('subsidiary_id', $subsidiaries, $user->subsidiary_id ?? old('subsidiary_id'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
                @if($errors->has('subsidiary_id'))
                    <span class="text-danger">{{ $errors->first('subsidiary_id') }}</span>
                @endif
            </div>
        </div>
    @else
        {!! Form::hidden('subsidiary_id', $user->subsidiary_id ?? Auth::user()->subsidiary_id) !!}
    @endcan
</div>

@can('edit_roles')
    <div class="row">
        <div class="col-12 mt-0 mb-2">
            <div class="section-title">Papéis</div>
        </div>
        <div class="col-12">
            <div class="form-group mb-4">
                {!! Form::label('roles', 'Grupos', ['class' => 'label-required']) !!}
                {!! Form::select('roles[]', $roles, isset($user->roles) ? $user->roles->pluck('id') : old('roles'), ['class' => 'form-control js-box js-select-target', 'multiple']) !!}
                @if($errors->has('roles'))
                    <span class="text-danger">{{ $errors->first('roles') }}</span>
                @endif
            </div>
        </div>
    </div>
@endcan
