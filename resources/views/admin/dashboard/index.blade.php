@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
            {!! Breadcrumbs::render('dashboard') !!}
        </div>
        <div class="section-body mt-4">
            <div class="row">
                @include('admin.dashboard._shortcuts')
            </div>
            <div class="row">
                @include('admin.dashboard._birthdays')
            </div>
        </div>
    </section>

@endsection
