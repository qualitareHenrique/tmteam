@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Financeiro</h1>
            {!! Breadcrumbs::render('financial') !!}
        </div>
        <div class="section-body mt-4">
            <div class="row">
                @can('view_balance_records')
                    @include('admin.dashboard.financial._year-resume')

                @endcan
            </div>
            <div class="row">
                @include('admin.dashboard.financial._billing-summary')
            </div>
        </div>
    </section>

@endsection
