<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="card card-statistic-1" id="income-card">
        <div class="card-icon shadow-second bg-success">
            <i class="fas fa-dollar-sign"></i>
        </div>
        <div class="card-wrap">
            <div class="card-header">
                <h4>Receitas</h4>
            </div>
            <div class="card-body">
                {{ number_format($balance->total_received ?? 0, 2, ',', '.') }}
            </div>
        </div>
    </div>
    <div class="card card-statistic-1" id="outcome-card">
        <div class="card-icon shadow-second bg-danger">
            <i class="fas fa-dollar-sign"></i>
        </div>
        <div class="card-wrap">
            <div class="card-header">
                <h4>Despesas</h4>
            </div>
            <div class="card-body">
                {{ number_format($balance->total_paid ?? 0, 2, ',', '.') }}
            </div>
        </div>
    </div>
    <div class="card card-statistic-1" id="registration-card">
        <div class="card-icon shadow-second bg-warning">
            <i class="far fa-user"></i>
        </div>
        <div class="card-wrap">
            <div class="card-header">
                <h4>Matriculas</h4>
            </div>
            <div class="card-body">{{ $registrations }}</div>
        </div>
    </div>
    <div class="card card-statistic-1" id="active-students-card">
        <div class="card-icon shadow-second bg-info">
            <i class="fas fa-graduation-cap"></i>
        </div>
        <div class="card-wrap">
            <div class="card-header">
                <h4>Alunos ativos</h4>
            </div>
            <div class="card-body">{{ $activeStudents->students ?? '0'  }}</div>
        </div>
    </div>
</div>
<div class="col-12 col-md-8 col-lg-8">
    <div class="card graph-card dashboard-card">
        <div class="card-header -info">
            <h4>
                <icon class="fas fa-chart-line lga"></icon>
                Desempenho
            </h4>
        </div>
        <div class="card-body">
            <div style="max-width: 740px; max-height: 370px">
                <canvas id="chart-financial"></canvas>
            </div>
        </div>
    </div>
</div>
