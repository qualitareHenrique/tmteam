<div class="col-lg-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4>
                <icon class="fas fa-wallet lga"></icon>
                Acadêmico por período
            </h4>
        </div>
        <div class="card-body">
            <div class="row justify-content-md-center mb-5">
                <div class="col-md-4 col-sm-12 mb-0 input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                    </div>
                    {{ Form::text('date_range', '', ['class' => 'form-control js-daterangepicker']) }}
                </div>
                <div class="col-md-4 col-sm-12 mb-0 form-group">
                    {{ Form::select('classroom_id[]', $classrooms, null, ['class' => 'form-control', 'title' => 'Turma', 'multiple' => 'multiple']) }}
                </div>
                @can('view_subsidiaries')
                    <div class="col-md-3 col-sm-12 mb-0 form-group">
                        {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                    </div>
                @endcan
                <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                    <button type="button"
                            id="filter-academic"
                            class="btn btn-primary form-control">
                        <icon class="fa fa-filter"></icon>
                    </button>
                </div>
            </div>

            <hr>

            <div class="row justify-content-lg-center mb-5">
                <div class="col-lg-12 text-center">
                    <div class="card-title">
                        <h4>Alunos por faixa</h4>
                    </div>
                    <div class="canvas-wrapper" style="position: relative; height: 100vh;">
                        <canvas id="chart-student-bands"></canvas>
                    </div>
                </div>
            </div>

            <div class="row justify-content-lg-center mb-5">
                <div class="col-lg-12 text-center">
                    <div class="card-title">
                        <h4>Alunos por categoria de idade</h4>
                    </div>
                    <div class="canvas-wrapper" style="position: relative; height: 100vh;">
                        <canvas id="chart-age-category"></canvas>
                    </div>

                </div>

                <div class="col-lg-12 text-center">
                    <div class="card-title">
                        <h4>Alunos por sexo</h4>
                    </div>
                    <div class="canvas-wrapper" style="position: relative; height: 100vh;">
                        <canvas id="chart-student-sex"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

