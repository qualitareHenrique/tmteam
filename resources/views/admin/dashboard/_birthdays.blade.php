<div class="col-12 col-sm-12 col-lg-6">
    <div class="card">
        <div class="card-header">
            <h4>
                <icon class="fas fa-birthday-cake lga"></icon>
                Aniversariantes do mês
            </h4>
        </div>
        <div class="card-body">
            <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder"
                style="overflow-y: scroll; max-height: 400px">
                @foreach($birthDays as $key => $result)

                    @php
                        $profile = $result->student ?? $result->guardian ?? $result->teacher;
                        $user = $profile->user;

                        $diff = $result->birth_day->isBirthday() ? 'Hoje' : $result->birth_day->diffForHumans();
                    @endphp

                    <li class="media">
                        <img alt="image"
                             class="mr-3 rounded-circle"
                             width="50"
                             src="{{ $user->avatarUrl }}">
                        <div class="media-body">
                            <div class="media-title">{{ $result->nickname }}</div>
                            <div class="text-job text-muted">{{ Str::contains($diff, 'horas') ? 'Amanhã' : $diff }}</div>
                        </div>
                        <div class="media-items">
                            <div class="media-item">
                                <div class="media-value">{{ $result->age }}</div>
                                <div class="media-label">anos</div>
                            </div>
                            <div class="media-item">
                                <a href="https://api.whatsapp.com/send?phone=55{{ $result->phone }}" target="_blank">
                                    <div class="media-value">
                                        <i class="fab icon-lg fa-whatsapp"></i>
                                    </div>
                                    <div class="media-label">mensagem</div>
                                </a>
                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
</div>
