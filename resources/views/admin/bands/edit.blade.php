@extends('layouts.default')

@section('content')

    {!! Form::model($band, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.bands.update', 'id' => $band->id]]) !!}
    <section class="section">
        <div class="section-header mb-4">
            <div class="section-header-back">
                <a href="{{ route('admin.bands.index') }}"
                   class="btn btn-icon"
                   title="Voltar">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Editar Faixa</h1>
            {!! Breadcrumbs::render('bands.edit', $band) !!}
        </div>

        <div class="clearfix"></div>

        <div class="section-body mt-4">
            <div class="row">
                @include('admin.bands._form')
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection
