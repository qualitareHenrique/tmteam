<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand mb-2">
            <a href="{{ route('admin.dashboard.index') }}">
                <img src="{{ asset('img/brand.png') }}"
                     width="50"></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('admin.dashboard.index') }}">
                <img src="{{ asset('img/brand.png') }}"
                     width="50"></a>
        </div>

        @can('registration')
            <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                <a href="{{ route('admin.new-registration.index') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
                    <i class="fas fa-graduation-cap"></i> Nova matrícula
                </a>
            </div>
        @endcan

        <ul class="sidebar-menu mt-4">

            @foreach(config('qualitare.modules') as $module)

                @can($module['permission'])
                    <li class="menu-header">{{ $module['name'] }}</li>

                    @foreach($module['options'] as $option)

                        @can($option['permission'])

                            @if(!$option['is_tree'])
                                @include('admin.partials.option', ['item' => $option])
                            @else
                                @include('admin.partials.tree', ['tree' => $option])
                            @endif

                        @endcan

                    @endforeach

                @endcan

            @endforeach
        </ul>
    </aside>
</div>
