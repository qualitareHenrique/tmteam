@can($item['permission'])
    <li class="{{ (Route::currentRouteName() == $item['route']) ? 'active' : '' }}">
        <a class="{{ (Route::currentRouteName() == $item['route']) ? 'actived' : '' }} nav-link"
           href="{{ route($item['route']) }}"
           title="{{ $item['name'] }}">
            <i class="{{ $item['icon'] }}"></i>
            <span>{{ $item['name'] }}</span>
        </a>
    </li>
@endcan