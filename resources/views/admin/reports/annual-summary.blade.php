@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-4">
            <h1>Resumo Anual</h1>
            {!! Breadcrumbs::render('reports.annual-summary') !!}
        </div>

        <div class="row">
            <div class="col-12">
                <h6 class="mb-3">Filtros</h6>
                <div class="card mb-0">
                    <div class="card-body mb-0">
                        {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.reports.annual-summary', 'method' => 'GET']) !!}
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                {!! Form::text('year', Request::get('year'), ['class' => 'form-control js-mask-year', 'placeholder' => 'Ano']) !!}
                            </div>
                            <div class="col-md-4 col-sm-12 mb-0 form-group">
                                {{ Form::select('months[]', $monthsOptions, old('months[]'), ['class' => 'form-control', 'title' => 'Meses', 'multiple' => 'multiple']) }}
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                <a href="#"
                                   class="btn btn-icon btn-block btn-danger"
                                   title="Limpar Campos">
                                    <i class="fas fa-ban lg-icon"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                <button type="submit"
                                        class="btn btn-icon btn-block btn-success">
                                    <i class="fas fa-search lg-icon"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-a">
                                        <th></th>
                                        <th>Novas Matrículas</th>
                                        <th>Alunos Ativos</th>
                                        <th>Receitas</th>
                                        <th>Despesas</th>
                                        <th>Diferença Mensal</th>
                                        <th>Resultado</th>
                                        <th>Acumulativo</th>
                                    </tr>

                                    @php
                                        $acumulativo = 0;
                                        $totalMatriculas = 0;
                                        $totalAtivos = 0;
                                        $totalReceitas = 0;
                                        $totalDespesas = 0;
                                        $totalResultado = 0;
                                    @endphp

                                    @forelse($summary as $month)
                                        @php
                                            $diferencaMensal = $month->revenues - $month->expeditures;
    
                                            $desvio = 0;

                                            if ($month->revenues > 0 && $month->expeditures > 0) {
                                                $desvio = (100 * ($month->revenues - $month->expeditures) / $month->expeditures);
                                            }
                                            
                                            if (!$month->revenues && $month->expeditures > 0) {
                                                $desvio = -100;
                                            }
                                            
                                            if ($month->revenues > 0 && !$month->expeditures) {
                                                $desvio = 100;
                                            }

                                            $alunosAtivos = \App\Core\Models\TrainingCenter\Registration::getActiveStudents($month->referring);
                                            $novasMatriculas = \App\Core\Models\TrainingCenter\Registration::getNewRegistrations($month->referring);

                                            $acumulativo     += $diferencaMensal;
                                            $totalMatriculas += $novasMatriculas->registries ?? 0;
                                            $totalAtivos     += $alunosAtivos->students ?? 0;
                                            $totalReceitas   += $month->revenues;
                                            $totalDespesas   += $month->expeditures;
                                            $totalResultado  += $desvio;

                                        @endphp

                                        <tr class="tr-item td-border">
                                            <td width="1" class="td-item">{{ months()[(int) str_after($month->referring, '-')] ?? '-' }}</td>
                                            <td width="1" class="td-item">{{ $novasMatriculas->registries ?? 0 }}</td>
                                            <td width="1" class="td-item">{{ $alunosAtivos->students ?? 0 }}</td>
                                            <td class="td-item">{{ number_format($month->revenues, 2, ',', '.') }}</td>
                                            <td class="td-item">{{ number_format($month->expeditures, 2, ',', '.') }}</td>
                                            <td width="2" class="td-item">{{ number_format($diferencaMensal, 2, ',', '.') }}</td>
                                            <td width="3">{{ number_format($desvio, 2, ',', '.') }} %</td>
                                            <td class="td-item">{{ number_format($acumulativo, 2, ',', '.') }}</td>
                                        </tr>
                                    @empty
                                        <tr class="tr-item td-border">
                                            <td colspan="3"
                                                class="text-center">Nenhum registro encontrado
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                    <tr class="tr-item td-border bg-white">
                                        <td class="td-item"><b>Total</b></td>
                                        <td class="td-item"><b>{{ $totalMatriculas }}</b></td>
                                        <td class="td-item"><b>{{ $totalAtivos }}</b></td>
                                        <td class="td-item">{{ number_format($totalReceitas, 2, ',', '.') }}</td>
                                        <td class="td-item">{{ number_format($totalDespesas, 2, ',', '.') }}</td>
                                        <td class="td-item">{{ number_format($acumulativo, 2, ',', '.') }}</td>
                                        <td class="td-item">{{ number_format($totalResultado, 2, ',', '.') }} %</td>
                                        <td class="td-item">{{ number_format($acumulativo, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr class="tr-item td-border bg-white">
                                        <td class="td-item"><b>Média Mês</b></td>
                                        <td class="td-item"><b>{{ number_format($totalMatriculas / 12, 2, ',', '.') }}</b></td>
                                        <td class="td-item"><b>{{ number_format($totalAtivos / 12, 2, ',', '.') }}</b></td>
                                        <td class="td-item">{{ number_format($totalReceitas / 12, 2, ',', '.') }}</td>
                                        <td class="td-item">{{ number_format($totalDespesas / 12, 2, ',', '.') }}</td>
                                        <td class="td-item">{{ number_format($acumulativo / 12, 2, ',', '.') }}</td>
                                        <td class="td-item">{{ number_format($totalResultado / 12, 2, ',', '.') }} %</td>
                                        <td class="td-item">{{ number_format($acumulativo / 12, 2, ',', '.') }}</td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection

