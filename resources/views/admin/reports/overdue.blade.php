@extends('layouts.default')

@section('content')

    <section class="section">
        <div class="section-header mb-4">
            <h1>Relatório de inadimplência</h1>
            {!! Breadcrumbs::render('reports.overdue') !!}
        </div>

        @can('view_subsidiaries')
            <div class="row">
                <div class="col-12">
                    <h6 class="mb-3">Filtros</h6>
                    <div class="card mb-0">
                        <div class="card-body mb-0">
                            {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.reports.overdue', 'method' => 'GET']) !!}
                            <div class="row">
                                <div class="col-md-6 col-sm-12 mb-0 form-group">
                                    {!! Form::text('student_name', Request::get('student_name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                                </div>
                                <div class="col-md-2 col-sm-12 mb-0 form-group">
                                    {!! Form::select('status', enumOptions('registration_status'), Request::get('status'), ['class' => 'form-control', 'title' => 'Situação ']) !!}
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                                    {!! Form::select('subsidiary_id', $subsidiaries, Request::get('subsidiary_id'), ['class' => 'form-control', 'title' => 'Unidade']) !!}
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group ml-auto">
                                    <a href="#"
                                       class="btn btn-icon btn-block btn-danger"
                                       title="Limpar Campos">
                                        <i class="fas fa-ban lg-icon"></i>
                                    </a>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                                    <button type="submit"
                                            class="btn btn-icon btn-block btn-success">
                                        <i class="fas fa-search lg-icon"></i>
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        <div class="section-body mt-4">
            <div class="row mt-4">
                <div class="col-lg-12 col-sm-12">
                    <h6 class="mb-3">Listagem de alunos inadimplentes</h6>
                    <div class="card">
                        <div class="card-body -table mb-0">
                            <div class="table-responsive">
                                {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.reports.resend-overdues', 'method' => 'POST', 'id' => 'form-orverdue-reports']) !!}
                                <table class="table table-striped overflow mb-0">
                                    <tbody>
                                    <tr class="tr-a">
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Parcelas em aberto</th>
                                    </tr>
                                    @forelse($records as $record)
                                        <tr class="tr-item td-border">
                                            <td class="td-item">
                                                {{ $record->student_name }}
                                                {!! registrationStatusBadge($record->status ?? 0) !!}
                                            </td>
                                            <td class="td-item">{{ $record->student_email }}</td>
                                            <td class="td-item td-overdue">{{ $record->overdues }}</td>
                                        </tr>
                                        @empty
                                            <tr class="tr-item td-border">
                                                <td colspan="3"
                                                    class="text-center">Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr class="tr-item">
                                            <td colspan="7">

                                            </td>
                                        </tr>
                                        <tr class="tr-item td-border bg-white">
                                            <td class="td-item">
                                                <b>Total: {!! $records->total() !!}</b>
                                            </td>
                                            <td class="td-item"
                                                colspan="2">
                                                <div class="float-right">{!! $records->render() !!}</div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection