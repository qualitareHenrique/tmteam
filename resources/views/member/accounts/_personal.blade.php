<div class="row">
    <div class="col-lg-12 mt-0 mb-2">
        <div class="section-title mt-2">Dados pessoais</div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('user[cpf]', 'CPF', ['class' => '']) !!}
            {!! Form::text('user[cpf]', old('user[cpf]'), ['class' => 'form-control js-mask-cpf', 'readonly' => true])!!}
            @if($errors->has('user.cpf'))
                <span class="text-danger">{{ $errors->first('user.cpf') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group mb-4">
            {!! Form::label('user[name]', 'Nome', ['class' => '']) !!}
            {!! Form::text('user[name]', old('user[name]'), ['class' => 'form-control']) !!}
            @if($errors->has('user.name'))
                <span class="text-danger">{{ $errors->first('user.name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('user[nickname]', 'Apelido', ['class' => '']) !!}
            {!! Form::text('user[nickname]', old('user[nickname]'), ['class' => 'form-control']) !!}
            @if($errors->has('user.nickname'))
                <span class="text-danger">{{ $errors->first('user.nickname') }}</span>
            @endif
        </div>
    </div>

    @if(isset($student->cbjj))
        <div class="col-sm-12 col-md-4">
            <div class="form-group mb-4">
                {!! Form::label('cbjj', 'CBJJ', ['class' => '']) !!}
                {!! Form::text('cbjj', old('cbjj'), ['class' => 'form-control']) !!}
                @if($errors->has('cbjj'))
                    <span class="text-danger">{{ $errors->first('cbjj') }}</span>
                @endif
            </div>
        </div>
    @endif

    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('user[email]', 'E-mail', ['class' => '']) !!}
            {!! Form::text('user[email]',  old('user[email]'), ['class' => 'form-control']) !!}
            @if($errors->has('user.email'))
                <span class="text-danger">{{ $errors->first('user.email') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-2">
            {!! Form::label('user[phone]', 'Telefone', ['class' => '']) !!}
            {!! Form::text('user[phone]',  old('user[phone]'), ['class' => 'form-control js-mask-phone']) !!}
            @if($errors->has('user.phone'))
                <span class="text-danger">{{ $errors->first('user.phone') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('user[birthdate]', 'Data de Nascimento', ['class' => '']) !!}
            {!! Form::text('user[birthdate]', old('user[birthdate]'), ['class' => 'form-control js-datepicker birthdate']) !!}
            @if($errors->has('user.birthdate'))
                <span class="text-danger">{{ $errors->first('user.birthdate') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('user[sex]', 'Sexo', ['class' => '']) !!}
            {!! Form::select('user[sex]', [1 => 'Masculino', 2 => 'Feminino'], old('user[sex]'), ['class' => 'form-control', 'title' => 'Selecione']) !!}
            @if($errors->has('user.sex'))
                <span class="text-danger">{{ $errors->first('user.sex') }}</span>
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
            {!! Form::label('user[age]', 'Idade', ['class' => '']) !!}
            {!! Form::number('user[age]', old('user[age]'), ['class' => 'form-control js-age']) !!}
            @if($errors->has('user.age'))
                <span class="text-danger">{{ $errors->first('user.age') }}</span>
            @endif
        </div>
    </div>
</div>
