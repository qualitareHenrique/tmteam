@extends('layouts.member')

@section('content')

    <section class="section">
        <div class="section-body mt-4">
            <div class="row">
                @include('member.dashboard._messages')
            </div>

            @if($student->registration)
                <div class="row">
                    @include('students.dashboard._fisic')
                    @include('students.dashboard._band-progress')
                </div>

                <div class="row">
                    @include('students.dashboard._frequencies')
                    @include('students.dashboard._installments')
                </div>

                <div class="row">
                    @if(count($history))
                        @include('students.dashboard._graduation-history')
                    @endif
                </div>
            @endif
        </div>
    </section>

@endsection


