<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4>Histórico de graduações</h4>
        </div>
        <div class="card-body">
            <div class="activities p-2 p-md-5">
                @foreach($history as $graduationRecord)
                    @php $band = $graduationRecord->degree->name == '0'; @endphp
                    <div class="activity">
                        <div class="activity-icon bg-primary text-white shadow-dark">
                            <i class="fas fa-{{ $band ? 'ribbon' : 'star'  }}"></i>
                        </div>
                        <div class="activity-detail">
                            <div class="mb-2">
                                <span class="text-job text-primary">{{ $graduationRecord->graduated_on->format('d/m/Y') }}</span>
                                <span class="bullet"></span>
                                <span class="text-job">{{ $graduationRecord->graduated_on->diffForHumans() }}</span>
                            </div>
                            @if($loop->last)
                                @if($band)
                                    <p>Agora você está na faixa {{ $graduationRecord->band->name }}.</p>
                                @else
                                    <p>Agora você está no grau {{ $graduationRecord->degree->name }} da faixa {{ $graduationRecord->band->name }}.</p>
                                @endif
                            @elseif($band)
                                <p>Parabéns, você conquistou a faixa {{ $graduationRecord->band->name }}, continue dando seu melhor.</p>
                            @else
                                <p>Parabéns, você conquistou o grau {{ $graduationRecord->degree->name }} da faixa {{ $graduationRecord->band->name }}, continue dando seu melhor.</p>
                            @endif
                        </div>
                    </div>
                @endforeach
                <div class="activity">
                    <div class="activity-icon bg-primary text-white shadow-dark">
                        <i class="fas fa-check"></i>
                    </div>
                    <div class="activity-detail">
                        <div class="mb-2">
                            <span class="text-job text-primary">{{ $student->registration->effective_date->format('d/m/Y') }}</span>
                            <span class="bullet"></span>
                            <span class="text-job">{{ $student->registration->effective_date->diffForHumans() }}</span>
                        </div>
                        <p>Você iniciou a sua jornada a ser um futuro faixa preta na CheckMat TMSisCon.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
