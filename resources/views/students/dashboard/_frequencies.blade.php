<div class="col-lg-6 col-sm-12">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h4>Treinos da semana</h4>
            <div>
                <a href="{{ route('students.frequencies.index') }}"
                   title="Histórico">
                    <i class="fas fa-history"></i>
                </a>
            </div>
        </div>
        <div class="card-body mb-0 p-2">
            <ul class="list-group">
                @forelse($frequencies as $frequency)
                    <li class="list-group-item">
                        {{ $frequency->classroom->name }}
                        <spam class="float-right">{{  $frequency->date->format('d/m H:i') }}</spam>
                    </li>
                @empty
                    <li class="list-group-item">
                        Nenhuma frequência registrada recentemente.
                    </li>
                @endforelse
            </ul>
        </div>
    </div>
</div>