@extends('layouts.member')

@section('content')
    <section class="section">
        <div class="section-header mb-10">
            <span class="fasfa-user"> </span>
            <h1>Registrar presença</h1>
        </div>

        <div class="section-body mt-4">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    {!! Form::open(['method' => 'POST',
                                            'novalidate',
                                            'role' => 'form',
                                            'class' => 'form',
                                            'route' => ['students.frequencies.store']
                                            ]) !!}
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-2">
                                <table class="table table-md">
                                    <tbody>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Turma</b></td>
                                        <td class="td-item">
                                            {{ $classroom->name }}
                                        </td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Modalidade</b></td>
                                        <td class="td-item">
                                            {{ $classroom->modality->name }}
                                        </td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Professores</b></td>
                                        <td class="td-item">
                                            @foreach($classroom->teachers as $teacher)
                                                {{ $teacher->user->name }}
                                                <br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr class="tr-item td-border">
                                        <td class="td-item"><b>Aluno</b></td>
                                        <td class="td-item">
                                            {!! Form::hidden('classroom_id', $classroom->id) !!}
                                            {!! Form::hidden('date', date('Y-m-d H:i:s')) !!}
                                            {!! Form::select('registration_id', $students, '', ['class' => 'form-control']) !!}
                                            @if($errors->has('registration_id'))
                                                <span class="text-danger">{{ $errors->first('registration_id') }}</span>
                                            @endif
                                            @if($errors->has('exists'))
                                                <span class="text-danger">{{ $errors->first('exists') }}</span>
                                            @endif
                                            @if($errors->has('not_belongs'))
                                                <span class="text-danger">{{ $errors->first('not_belongs') }}</span>
                                            @endif
                                            @if($errors->has('irregular'))
                                                <span class="text-danger">{{ $errors->first('irregular') }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="form-group mb-0">
                                <button type="submit"
                                        class="btn btn-lg btn-success btn-icon float-right"
                                        title="Atualizar dados"><i
                                            class="fas fa-check"></i>
                                    Salvar
                                </button>
                                <a href="{{ route('students.dashboard.index') }}"
                                   class="btn btn-danger btn-lg btn-icon float-right mr-2"
                                   title="Voltar"><i
                                            class="fas fa-angle-left"></i>
                                    Voltar</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
