<div class="row">
    <div class="col-lg-12 mt-0 mb-2">
        <div class="section-title mt-2">Arquivos</div>
    </div>
    <div class="form-group col-sm-12 col-md-6">
        {!! Form::label('user[avatar]', 'Foto do Aluno', []) !!}
        @if(!empty($avatar = $model->user->getFirstMedia('avatar')) && $avatar->validation_status == 1)
            <div class="border-dashed text-center">
                Aguardando aprovação ...
            </div>
        @else
            <div class="file-upload-wrapper">
                {{ Form::file('user[avatar]', ['class' => 'file-upload-field']) }}
            </div>
            <small id="avatarHelp" class="form-text text-muted">Inserir foto
                do busto, preferêncialmente de kimono e sem acessórios. Ex.:
                Boné,
                Óculos, etc.</small>
            @if($errors->has('user.avatar'))
                <span class="text-danger">{{ $errors->first('user.avatar') }}</span>
            @endif
        @endif
    </div>
    <div class="form-group col-sm-12 col-md-6">
        {!! Form::label('registration[fisic]', 'Aptidão Física', []) !!}
        @if(!empty($fisic = $model->registration->getFirstMedia('fisic')) && $fisic->validation_status == 1)
            <div class="border-dashed text-center">
                Aguardando aprovação ...
            </div>
        @else
            <div class="file-upload-wrapper">
                {{ Form::file('registration[fisic]', ['class' => 'file-upload-field']) }}
                @if($errors->has('registration.fisic'))
                    <span class="text-danger">{{ $errors->first('registration.fisic') }}</span>
                @endif
            </div>
        @endif
    </div>
    -
</div><?php
