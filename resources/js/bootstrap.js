/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');
    window.swal = require('sweetalert2');
    window.iziToast = require('izitoast');
    window.rangePlugin = require('flatpickr/dist/plugins/rangePlugin');
    window.monthSelectPlugin = require('flatpickr/dist/plugins/monthSelect');

    require('bootstrap');
    require('jquery.nicescroll');
    require('moment');
    require('flatpickr');
    require('flatpickr/dist/l10n/pt');
    require('summernote/dist/summernote-bs4');
    require('summernote/dist/lang/summernote-pt-BR');
    require('jquery-mask-plugin');
    require('bootstrap4-duallistbox');
    require('chart.js');
    require('chartjs-plugin-piechart-outlabels');
    require('bootstrap-select');
} catch (e) {}


