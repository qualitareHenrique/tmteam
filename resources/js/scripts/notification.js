$('.js-notification-delete').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();

    var target = $(this);

    $.ajax({
        type: "DELETE",
        url: target.data('link'),
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (data) {
            target.closest('.notification').fadeOut(300, function () {
                console.log('fadding');
                $(this.remove());
            });
        },
        error: function (data) {
            swal('Falha na solicitação', data.responseJSON.error, 'error');
        }
    });
});

$('.js-notification-redirect').on('click', function (e) {
    e.preventDefault();

    var target = $(this);

    $.ajax({
        type: "PUT",
        url: target.data('read-link'),
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {read: 'ok'},
        success: function (data) {
            window.location = target.data('link');
        },
        error: function (data) {
            swal('Falha na solicitação', data.responseJSON.error, 'error');
        }
    });
});