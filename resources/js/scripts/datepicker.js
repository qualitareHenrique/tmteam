window.flatpickr.l10ns.pt.rangeSeparator = " - ";

$(".js-datepicker").flatpickr({
    allowInput: true,
    enableTime: false,
    dateFormat: 'Y-m-d',
    altInput: true,
    altFormat: 'd/m/Y',
    disableMobile: "true",
    locale: 'pt',
    onClose(dates, currentdatestring, picker){
        picker.setDate(picker.altInput.value, true, picker.config.altFormat)
    }
});

$(".js-datetimepicker").flatpickr({
    allowInput: true,
    enableTime: true,
    time_24hr: false,
    dateFormat: 'Y-m-d H:i:S',
    altInput: true,
    altFormat: 'd/m/Y H:i',
    disableMobile: "true",
    locale: 'pt'
});

$(".js-daterangepicker").flatpickr({
    dateFormat: 'Y-m-d',
    altInput: true,
    altFormat: 'd/m/Y',
    disableMobile: "true",
    mode: 'range',
    locale: 'pt'
});

$(".js-monthpicker").flatpickr({
    disableMobile: "true",
    locale: 'pt',
    altInput: true,
    plugins: [
        new monthSelectPlugin({
            shorthand: true,
            dateFormat: "Y-m",
            altFormat: "m/Y",
            theme: "light"
        })
    ]
});