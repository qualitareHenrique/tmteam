const APP_URL = window.location.origin;

$(function () {
    $("select.form-control").not('.js-box').selectpicker({
        style: 'btn btn-light form-control',
        actionsBox: true,
        deselectAllText: 'Nenhum',
        selectAllText: 'Todos'
    });

    $("form").on("change", ".file-upload-field", function () {
        $(this).parent(".file-upload-wrapper")
            .attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
    });

    $('.js-confirm-delete').on('click', function (e) {
        e.preventDefault();

        swal({
            title: 'Você tem certeza?',
            text: $(this).data('title') + " será excluído(a).",
            type: 'error',
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            showCancelButton: true,
            confirmButtonColor: '#3f51b5',
            cancelButtonColor: '#ff4081',
        }).then((result) => {

            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: $(this).data('link'),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: function (data) {
                        swal('Excluído com sucesso', data.message, 'success');

                        if (data.route) {
                            setTimeout(function () {
                                window.location.href = data.route;
                            }, 500);

                        }
                    },
                    error: function (data) {
                        swal('Falha na solicitação', data.responseJSON.error, 'error');
                    }
                });
            } else if (result.dismiss === 'cancel') {
                swal('Ação cancelada', 'O registro não foi excluido', 'warning');
            }
        })
    });

    $(".js-clear").on("click", function () {
        $('.form')[0].reset();
    });

    $(".js-box").bootstrapDualListbox({
        filterPlaceHolder: "Filtrar",
        infoText: false
    });

    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

    if ($(".js-redactor").length) {
        $('.js-redactor').summernote({
            lang: 'pt-BR',
            height: 300,
            tabsize: 2,
            maximumImageFileSize: 2097152
        });
    }
});

function randomColor() {
    return ("#" + Math.floor(Math.random() * 16777215).toString(16)).padEnd(7, '');
}

function getHexContrast(hex) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }

    return (parseInt(hex, 16) > 0xffffff / 2) ? 'black' : 'white';
}


function updateTeacherInput(element) {
    $.ajax({
        type: "POST",
        url: '/admin/classrooms-teachers/add-teacher',
        data: { 
          teacher_id: $('select[name="teacher_id"]').val(),
          classroom_id: $('input[name="classroom_id"]').val()
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (data) {
          if (data.duplicated) {
            return false;
          }

          if (data.content) {
              $('table[id="classroom-teatcher-table"]').find('tbody').append(data.content);
              
              return false;
          }

          if (data.erro) {
              swal('Sucesso', data.erro, 'success');

              return false;
          }
        },
        error: function () {
          swal('Atenção', 'Ocorreu um erro durante a solicitação', 'error');
        }
    });
}

function deleteClassroomTeacher(element) {
  event.preventDefault()
  let id = $(element).parent().data('id');

  swal({
    title: 'Deseja realmente remover este professor da turma?',
    type: 'error',
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar',
    showCancelButton: true,
    confirmButtonColor: '#3f51b5',
    cancelButtonColor: '#ff4081',
  }).then((result) => {
      if (result.value) {
          $.ajax({
          type: "POST",
          url: '/admin/classrooms-teachers/remove-teacher',
          data: {id: id},
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          dataType: 'json',
          success: function (data) {
            if (data.success) {
              $(element).parent().parent().remove();
              swal('Sucesso', 'O professor foi removido da turma.', 'success');
  
              return false;
            }
          },
          error: function () {
            swal('Atenção', 'Ocorreu um erro durante a solicitação', 'error');
          }
        });  
      }
  });
}

$('.student-reactivate-registration').on('click', function () {
    swal({
        title: 'Deseja reativar essa matricula?',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "GET",
                url: $(this).data('link'),
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {
                    swal('Sucesso!', data.success, 'success');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500);
                },
                error: function (data) {
                    swal('Falha na solicitação', data.error, 'error');
                }
            });

            return false;
        }

        swal('Ação cancelada', 'A matrícula não foi reativada.', 'warning');
    });
});

$('.student-deactivate-registration').on('click', function () {
    let deactivateInstallments = 0;
    let deactivateRegistration = 0;

    swal.mixin({
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
    }).queue([
        {
            text: 'Deseja desativar esta matrícula?',
            preConfirm: function(value) {
                deactivateRegistration = value;
            }
        },
        {
            text: 'Deseja cancelar juntamente as parcelas?',
            preConfirm: function(value) {
                deactivateInstallments = value;
            }
        }
    ]).then((result) => {
        if (!deactivateRegistration) {
            swal('Ação cancelada', 'A matrícula não foi desativada.', 'warning');

            return false;
        }
        $.ajax({
            type: "POST",
            url: $(this).data('link'),
            data: {'deactive_installments': deactivateInstallments},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            success: function (data) {
                swal('Sucesso!', data.success, 'success');
                setTimeout(function () {
                    window.location.reload();
                }, 500);
            },
            error: function (data) {
                swal('Falha na solicitação', data.error, 'error');
            }
        });
    });
});

function resendOverdues(element, student, type, phone) {
    swal({
        title: 'Deseja reenviar as cobranças?',
        type: 'info',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: $(element).data('link'),
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    type: type,
                    student: student
                },
                success: function (data) {
                    if (data.erro) {
                        swal('Atenção', data.erro, 'error');
                        
                        return false;
                    }
                    if (type === 2) {
                        window.open('https://web.whatsapp.com/send?phone=55' + phone + '&text=' + data.data, '_blank');

                        return false;
                    }
                    swal('Sucesso', data.message, 'success');
                },
                error: function (data) {
                    swal('Falha na solicitação', data.responseJSON.error, 'error');
                }
            });
        }
    })
}