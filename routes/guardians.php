<?php

/**
 * Teacher Routes
 */
Route::prefix('responsavel')->middleware(['auth.admin', 'role.check', 'term.check'])->namespace('Guardian')->group(function () {
    Route::name('guardians.dashboard.index')->get('/', 'Dashboard\DashboardController@index');
    Route::name('guardians.accounts.edit')->get('/perfil', 'Accounts\AccountController@edit');
    Route::name('guardians.accounts.update')->put('/perfil/atualizar/{guardian}', 'Accounts\AccountController@update');
});
