<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api')->name('api.')->group(function () {

    Route::prefix('v1')->group(function(){
        Route::prefix('notifications')->group(function () {
            Route::name('invoice.notification')
                 ->post('/invoices/{record}', 'Notifications\InvoiceController@update');
        });
    });

    Route::prefix('v2')->group(function() {
        Route::prefix('juno')->namespace('Juno')->group(function(){
            Route::name('payment.store')
                 ->post("/payment", 'PaymentController@store');

            Route::name('account.status')
                 ->post("/account/status", 'AccountController@update');
        });
    });

});


