<?php

Breadcrumbs::for('dashboard', function ($breadcrumb) {
  $breadcrumb->push('Dashboard', route('admin.dashboard.index'));
});

Breadcrumbs::for('financial', function ($breadcrumb) {
    $breadcrumb->push('Financeiro', route('admin.financial.index'));
});

Breadcrumbs::for('academic', function ($breadcrumb) {
    $breadcrumb->push('Acadêmico', route('admin.academic.index'));
});

Breadcrumbs::for('audits.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Auditoria', route('admin.audits.index'));
});

Breadcrumbs::for('accounts.show', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Meu Perfil', route('admin.accounts.show'));
});

Breadcrumbs::for('accounts.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('accounts.show');
  $breadcrumb->push('Editar Perfil', route('admin.accounts.edit', ['id' => $result->id]));
});

Breadcrumbs::for('audits.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('audits.index');
  $breadcrumb->push('Visualizar Log', route('admin.audits.show', ['id' => $result->id]));
});

Breadcrumbs::for('users.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Usuários', route('admin.users.index'));
});

Breadcrumbs::for('users.create', function ($breadcrumb) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Novo Usuário', route('admin.users.create'));
});

Breadcrumbs::for('users.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Editar Usuário', route('admin.users.edit', ['id' => $result->id]));
});

Breadcrumbs::for('roles.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Grupos', route('admin.roles.index'));
});

Breadcrumbs::for('roles.create', function ($breadcrumb) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Novo Grupo', route('admin.roles.create'));
});

Breadcrumbs::for('roles.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Editar Grupo', route('admin.roles.edit', ['id' => $result->id]));
});

Breadcrumbs::for('permissions.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Permissões', route('admin.permissions.index'));
});

Breadcrumbs::for('permissions.create', function ($breadcrumb) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Nova Permissão', route('admin.permissions.create'));
});

Breadcrumbs::for('permissions.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Editar Permissão', route('admin.permissions.edit', ['id' => $result->id]));
});

Breadcrumbs::for('bands.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Faixas', route('admin.bands.index'));
});

Breadcrumbs::for('bands.create', function ($breadcrumb) {
  $breadcrumb->parent('bands.index');
  $breadcrumb->push('Nova Faixa', route('admin.bands.create'));
});

Breadcrumbs::for('bands.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('bands.index');
  $breadcrumb->push('Editar Faixa', route('admin.bands.edit', ['id' => $result->id]));
});

Breadcrumbs::for('ages_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('bands.index');
  $breadcrumb->push('Categorias', route('admin.age-categories.index'));
});

Breadcrumbs::for('ages_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('ages_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.age-categories.create'));
});

Breadcrumbs::for('ages_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('ages_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.age-categories.edit', ['id' => $result->id]));
});

Breadcrumbs::for('classrooms.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Turmas', route('admin.classrooms.index'));
});

Breadcrumbs::for('classrooms.create', function ($breadcrumb) {
  $breadcrumb->parent('classrooms.index');
  $breadcrumb->push('Nova Turma', route('admin.classrooms.create'));
});

Breadcrumbs::for('classrooms.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('classrooms.index');
  $breadcrumb->push('Editar Turma', route('admin.classrooms.edit', ['id' => $result->id]));
});

Breadcrumbs::for('classrooms_frequencies.index', function ($breadcrumb, $classroom) {
  $breadcrumb->parent('classrooms.index');
  $breadcrumb->push('Frequências', route('admin.classrooms.frequencies.index', ['classroom_id' => $classroom->id]));
});

Breadcrumbs::for('classrooms_frequencies.create', function ($breadcrumb, $classroom) {
  $breadcrumb->parent('classrooms_frequencies.index', $classroom);
  $breadcrumb->push('Nova Frequência', route('admin.classrooms.frequencies.create', ['classroom_id' => $classroom->id]));
});

Breadcrumbs::for('classrooms_frequencies.edit', function ($breadcrumb, $classroom, $result) {
  $breadcrumb->parent('classrooms_frequencies.index', $classroom);
  $breadcrumb->push('Editar Frequência', route('admin.classrooms.frequencies.edit', ['classroom_id' => $classroom->id, 'id' => $result->id]));
});

Breadcrumbs::for('qualifications.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Qualificações', route('admin.qualifications.index'));
});

Breadcrumbs::for('modalities.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Modalidades', route('admin.modalities.index'));
});

Breadcrumbs::for('modalities.create', function ($breadcrumb) {
  $breadcrumb->parent('modalities.index');
  $breadcrumb->push('Nova Modalidade', route('admin.modalities.create'));
});

Breadcrumbs::for('modalities.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('modalities.index');
  $breadcrumb->push('Editar Modalidade', route('admin.modalities.edit', ['id' => $result->id]));
});

Breadcrumbs::for('students.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Alunos', route('admin.students.index'));
});

Breadcrumbs::for('students.create', function ($breadcrumb) {
  $breadcrumb->parent('students.index');
  $breadcrumb->push('Novo Aluno', route('admin.students.create'));
});

Breadcrumbs::for('students.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('students.index');
  $breadcrumb->push('Visualizar Aluno', route('admin.students.show', ['id' => $result->id]));
});

Breadcrumbs::for('students.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('students.index');
  $breadcrumb->push('Editar Aluno', route('admin.students.edit', ['id' => $result->id]));
});

Breadcrumbs::for('guardians.index', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Responsáveis', route('admin.guardians.index'));
});

Breadcrumbs::for('guardians.create', function ($breadcrumb) {
    $breadcrumb->parent('guardians.index');
    $breadcrumb->push('Novo responsável', route('admin.guardians.create'));
});

Breadcrumbs::for('guardians.edit', function ($breadcrumb, $result) {
    $breadcrumb->parent('guardians.index');
    $breadcrumb->push('Editar responsável', route('admin.guardians.edit', ['id' => $result->id]));
});

Breadcrumbs::for('students_origins.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Origens', route('admin.origins.index'));
});

Breadcrumbs::for('students_origins.create', function ($breadcrumb) {
  $breadcrumb->parent('students_origins.index');
  $breadcrumb->push('Nova Origem', route('admin.origins.create'));
});

Breadcrumbs::for('students_origins.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('students_origins.index');
  $breadcrumb->push('Editar Origem', route('admin.origins.edit', ['id' => $result->id]));
});

Breadcrumbs::for('teachers.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Professores', route('admin.teachers.index'));
});

Breadcrumbs::for('teachers.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('teachers.index');
  $breadcrumb->push('Visualizar Professor', route('admin.teachers.show', ['id' => $result->id]));
});

Breadcrumbs::for('teachers.create', function ($breadcrumb) {
  $breadcrumb->parent('teachers.index');
  $breadcrumb->push('Novo Professor', route('admin.teachers.create'));
});

Breadcrumbs::for('teachers.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('teachers.index');
  $breadcrumb->push('Editar Professor', route('admin.teachers.edit', ['id' => $result->id]));
});

Breadcrumbs::for('messages.index', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Mensagens', route('admin.messages.index'));
});

Breadcrumbs::for('messages.create', function ($breadcrumb) {
    $breadcrumb->parent('messages.index');
    $breadcrumb->push('Criar mensagem', route('admin.messages.create'));
});

Breadcrumbs::for('messages.edit', function ($breadcrumb, $result) {
    $breadcrumb->parent('messages.index');
    $breadcrumb->push('Editar mensagem', route('admin.messages.edit', ['id' => $result->id]));
});

Breadcrumbs::for('balance_records.index', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Lançamentos', route('admin.messages.index'));
});

Breadcrumbs::for('balance_records.create', function ($breadcrumb) {
    $breadcrumb->parent('balance_records.index');
    $breadcrumb->push('Criar lançamento', route('admin.messages.create'));
});

Breadcrumbs::for('balance_records.edit', function ($breadcrumb, $result) {
    $breadcrumb->parent('balance_records.index');
    $breadcrumb->push('Editar lançamento', route('admin.messages.edit', ['id' => $result->id]));
});

Breadcrumbs::for('balance_records.show', function ($breadcrumb, $result) {
    $breadcrumb->parent('balance_records.index');
    $breadcrumb->push('Visualizar lançamento', route('admin.messages.show', ['id' => $result->id]));
});

Breadcrumbs::for('students_installments.index', function ($breadcrumb, $registration) {
    $breadcrumb->parent('students.index');
    $breadcrumb->push('Lançamentos', route('admin.students.installments.index', ['registration_id' => $registration->id]));
});

Breadcrumbs::for('students_installments.show', function ($breadcrumb, $registration, $result) {
    $breadcrumb->parent('students_installments.index', $registration);
    $breadcrumb->push('Visualizar Lançamento', route('admin.students.installments.show', ['registration_id' => $registration->id, 'id' => $result->id]));
});

Breadcrumbs::for('students_installments.create', function ($breadcrumb, $registration) {
    $breadcrumb->parent('students_installments.index', $registration);
    $breadcrumb->push('Novo Lançamento', route('admin.students.installments.create', ['registration_id' => $registration->id]));
});

Breadcrumbs::for('students_installments.edit', function ($breadcrumb, $registration, $result) {
    $breadcrumb->parent('students_installments.index', $registration);
    $breadcrumb->push('Editar Lançamento', route('admin.students.installments.edit', ['registration_id' => $registration->id, 'id' => $result->id]));
});

Breadcrumbs::for('students_graduations.index', function ($breadcrumb, $registration) {
    $breadcrumb->parent('students.index');
    $breadcrumb->push('Graduações', route('admin.students.graduations.index', ['registration_id' => $registration->id]));
});

Breadcrumbs::for('students_graduations.create', function ($breadcrumb, $registration) {
    $breadcrumb->parent('students_graduations.index', $registration);
    $breadcrumb->push('Nova graduação', route('admin.students.graduations.create', ['registration_id' => $registration->id]));
});

Breadcrumbs::for('students_graduations.edit', function ($breadcrumb, $registration, $result) {
    $breadcrumb->parent('students_graduations.index', $registration);
    $breadcrumb->push('Editar graduação', route('admin.students.graduations.edit', ['registration_id' => $registration->id, 'id' => $result->id]));
});

Breadcrumbs::for('students_issues.index', function ($breadcrumb, $registration) {
    $breadcrumb->parent('students.index');
    $breadcrumb->push('Pendências', route('admin.students.issues.index', ['registration_id' => $registration->id]));
});

Breadcrumbs::for('reports.balance', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Balanço', route('admin.reports.balance'));
});

Breadcrumbs::for('reports.overdue', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Inadimplência', route('admin.reports.overdue'));
});

Breadcrumbs::for('students_attachments.show', function ($breadcrumb, $result) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Validar anexo', route('admin.students.attachments.show', ['id' => $result->id]));
});

Breadcrumbs::for('subsidiaries.index', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Unidades', route('admin.subsidiaries.index'));
});

Breadcrumbs::for('subsidiaries.create', function ($breadcrumb) {
    $breadcrumb->parent('subsidiaries.index');
    $breadcrumb->push('Criar unidade', route('admin.subsidiaries.create'));
});

Breadcrumbs::for('subsidiaries.edit', function ($breadcrumb, $result) {
    $breadcrumb->parent('subsidiaries.index');
    $breadcrumb->push('Editar unidade', route('admin.subsidiaries.edit', ['id' => $result->id]));
});

Breadcrumbs::for('settings.edit', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Editar configurações', route('admin.settings.edit'));
});

Breadcrumbs::for('reports.annual-summary', function ($breadcrumb) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Resumo Financeiro Anual', route('admin.reports.annual-summary'));
});

Breadcrumbs::for('new-registration.index', function ($breadcrumb, ?int $student = null) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Aluno', route('admin.new-registration.index', ['studentId' =>  $student]));
});

Breadcrumbs::for('new-registration.guardian', function ($breadcrumb, ?int $student) {
    $breadcrumb->parent('dashboard');
    $breadcrumb->push('Aluno', route('admin.new-registration.index', ['studentId' =>  $student]));
    $breadcrumb->push('Responsavel', route('admin.new-registration.guardian', ['student' =>  $student]));
});

Breadcrumbs::for('new-registration.registration', function ($breadcrumb, ?int $student, bool $guardian = true) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Aluno', route('admin.new-registration.index', ['studentId' =>  $student]));

  if ($guardian) {
    $breadcrumb->push('Responsavel', route('admin.new-registration.guardian', ['student' =>  $student]));
  }

  $breadcrumb->push('Matrícula', route('admin.new-registration.registration', ['student' =>  $student]));
});

Breadcrumbs::for('new-registration.graduation', function ($breadcrumb, ?int $student, bool $guardian = true) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Aluno', route('admin.new-registration.index', ['studentId' =>  $student]));

  if ($guardian) {
    $breadcrumb->push('Responsavel', route('admin.new-registration.guardian', ['student' =>  $student]));
  }

  $breadcrumb->push('Matrícula', route('admin.new-registration.registration', ['student' =>  $student]));
  $breadcrumb->push('Graduação', route('admin.new-registration.graduation', ['student' =>  $student]));
});