<?php

return [
    'apiUrl'         => env('GATEWAY_URL'),
    'token'          => env('GATEWAY_TOKEN'),
    'maxOverdueDays' => env('GATEWAY_MAXOVERDUEDAYS', 29),
    'fine'           => env('GATEWAY_FINE', 0.00),
    'interest'       => env('GATEWAY_INTEREST', 0.00)
];
