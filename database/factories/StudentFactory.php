<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Core\Models\TrainingCenter\Student;
use App\Core\Models\Person;
use App\Core\Models\TrainingCenter\Guardian;
use App\Core\Models\Auth\User;
use App\Core\Models\TrainingCenter\Origin;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'experimental'   => true,
        'origin_id'      => factory(Origin::class)->create()->id,
        'guardian_id' => factory(Guardian::class)->create()->id,
    ];
});
