<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Core\Models\TrainingCenter\Modality;
use App\Core\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(Modality::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'price' => $faker->numberBetween(1000, 9000),
    ];
});
