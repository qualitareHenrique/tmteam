<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Core\Models\Person;
use App\Core\Models\TrainingCenter\Guardian;
use Faker\Generator as Faker;
use App\Core\Models\Auth\User;

$factory->define(Guardian::class, function (Faker $faker) {
    return [
        'user_id' => User::first()->id
    ];
});
