<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Core\Models\Person;
use App\Core\Models\TrainingCenter\Teacher;
use Faker\Generator as Faker;
use App\Core\Models\Auth\User;

$factory->define(Teacher::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
    ];
});
