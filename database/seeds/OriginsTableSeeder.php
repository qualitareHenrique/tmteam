<?php

use Illuminate\Database\Seeder;
use App\Core\Models\TrainingCenter\Origin as Origin;
use App\Core\Models\Auth\User;

class OriginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Origin::create(['name' => 'Site']);
        Origin::create(['name' => 'Instagram']);
        Origin::create(['name' => 'Facebook']);
        Origin::create(['name' => 'Whatsapp']);
        Origin::create(['name' => 'Telefone']);
        Origin::create(['name' => 'Presencial']);
        Origin::create(['name' => 'Outros']);
    }
}
