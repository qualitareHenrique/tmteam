<?php

use Illuminate\Database\Seeder;
use App\Core\Models\Auth\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // Permissions
        $permissions = [

            // Audit
            ['name' => 'view_audits', 'details' => 'Auditorias [view]'],

            // Dashboard
            ['name' => 'view_dashboard', 'details' => 'Dashboard [view]'],

            // Analytics
            ['name' => 'view_analytics', 'details' => 'Acessos [view]'],

            // Modules
            ['name' => 'view_modules', 'details' => 'Módulos [view]'],

            // Centro de treinamento
            ['name' => 'view_centers', 'details' => 'Centro de Treinamento [view]'],

            // Controle de despesas
            ['name' => 'view_controls', 'details' => 'Controle de Despesas [view]'],

            // Students Frequencies
            ['name' => 'view_students_frequencies', 'details' => 'Área do Aluno - Frequências [view]'],
            ['name' => 'add_students_frequencies', 'details' => 'Área do Aluno - Frequências [add]'],
            ['name' => 'edit_students_frequencies', 'details' => 'Área do Aluno - Frequências [edit]'],
            ['name' => 'delete_students_frequencies', 'details' => 'Área do Aluno - Frequências [delete]'],

            // Students Graduations
            ['name' => 'view_students_graduations', 'details' => 'Área do Aluno - Desempenhos [view]'],
            ['name' => 'add_students_graduations', 'details' => 'Área do Aluno - Desempenhos [add]'],
            ['name' => 'edit_students_graduations', 'details' => 'Área do Aluno - Desempenhos [edit]'],
            ['name' => 'delete_students_graduations', 'details' => 'Área do Aluno - Desempenhos [delete]'],

            // Students
            ['name' => 'view_students', 'details' => 'Alunos [view]'],
            ['name' => 'add_students', 'details' => 'Alunos [add]'],
            ['name' => 'edit_students', 'details' => 'Alunos [edit]'],
            ['name' => 'delete_students', 'details' => 'Alunos [delete]'],

            // Students Modalities
            ['name' => 'view_modalities', 'details' => 'Modalidades [view]'],
            ['name' => 'add_modalities', 'details' => 'Modalidades [add]'],
            ['name' => 'edit_modalities', 'details' => 'Modalidades [edit]'],
            ['name' => 'delete_modalities', 'details' => 'Modalidades [delete]'],

            // Students Origins
            ['name' => 'view_students_origins', 'details' => 'Alunos Origens [view]'],
            ['name' => 'add_students_origins', 'details' => 'Alunos Origens [add]'],
            ['name' => 'edit_students_origins', 'details' => 'Alunos Origens [edit]'],
            ['name' => 'delete_students_origins', 'details' => 'Alunos Origens [delete]'],

            // Students Submodules
            ['name' => 'view_students_submodules', 'details' => 'Alunos Submódulos [view]'],
            ['name' => 'add_students_submodules', 'details' => 'Alunos Submódulos [add]'],
            ['name' => 'edit_students_submodules', 'details' => 'Alunos Submódulos [edit]'],
            ['name' => 'delete_students_submodules', 'details' => 'Alunos Submódulos [delete]'],

            // Teachers
            ['name' => 'view_teachers', 'details' => 'Professores [view]'],
            ['name' => 'add_teachers', 'details' => 'Professores [add]'],
            ['name' => 'edit_teachers', 'details' => 'Professores [edit]'],
            ['name' => 'delete_teachers', 'details' => 'Professores [delete]'],

            // Teachers Submodules
            ['name' => 'view_teachers_submodules', 'details' => 'Professores Submódulos [view]'],
            ['name' => 'add_teachers_submodules', 'details' => 'Professores Submódulos [add]'],
            ['name' => 'edit_teachers_submodules', 'details' => 'Professores Submódulos [edit]'],
            ['name' => 'delete_teachers_submodules', 'details' => 'Professores Submódulos [delete]'],

            // Classrooms
            ['name' => 'view_classrooms', 'details' => 'Turmas [view]'],
            ['name' => 'add_classrooms', 'details' => 'Turmas [add]'],
            ['name' => 'edit_classrooms', 'details' => 'Turmas [edit]'],
            ['name' => 'delete_classrooms', 'details' => 'Turmas [delete]'],

            // Teachers Submodules
            ['name' => 'view_classrooms_submodules', 'details' => 'Turmas Submódulos [view]'],
            ['name' => 'add_classrooms_submodules', 'details' => 'Turmas Submódulos [add]'],
            ['name' => 'edit_classrooms_submodules', 'details' => 'Turmas Submódulos [edit]'],
            ['name' => 'delete_classrooms_submodules', 'details' => 'Turmas Submódulos [delete]'],

            // Bands
            ['name' => 'view_bands', 'details' => 'Faixas [view]'],
            ['name' => 'add_bands', 'details' => 'Faixas [add]'],
            ['name' => 'edit_bands', 'details' => 'Faixas [edit]'],
            ['name' => 'delete_bands', 'details' => 'Faixas [delete]'],

            // Bands Submodules
            ['name' => 'view_bands_submodules', 'details' => 'Faixas Submódulos [view]'],
            ['name' => 'add_bands_submodules', 'details' => 'Faixas Submódulos [add]'],
            ['name' => 'edit_bands_submodules', 'details' => 'Faixas Submódulos [edit]'],
            ['name' => 'delete_bands_submodules', 'details' => 'Faixas Submódulos [delete]'],

            // Balance records
            ['name' => 'view_balance_records', 'details' => 'Lançamentos [view]'],
            ['name' => 'add_balance_records', 'details' => 'Lançamentos [add]'],
            ['name' => 'edit_balance_records', 'details' => 'Lançamentos [edit]'],
            ['name' => 'delete_balance_records', 'details' => 'Lançamentos [delete]'],

            // Graduations
            ['name' => 'view_graduations', 'details' => 'Graduações [view]'],
            ['name' => 'add_graduations', 'details' => 'Graduações [add]'],
            ['name' => 'edit_graduations', 'details' => 'Graduações [edit]'],
            ['name' => 'delete_graduations', 'details' => 'Graduações [delete]'],

            // Graduations Submodules
            ['name' => 'view_graduations_submodules', 'details' => 'Graduações Submódulos [view]'],
            ['name' => 'add_graduations_submodules', 'details' => 'Graduações Submódulos [add]'],
            ['name' => 'edit_graduations_submodules', 'details' => 'Graduações Submódulos [edit]'],
            ['name' => 'delete_graduations_submodules', 'details' => 'Graduações Submódulos [delete]'],

            // Subscriptions
            ['name' => 'view_subscriptions', 'details' => 'Matrículas [view]'],
            ['name' => 'add_subscriptions', 'details' => 'Matrículas [add]'],
            ['name' => 'edit_subscriptions', 'details' => 'Matrículas [edit]'],
            ['name' => 'delete_subscriptions', 'details' => 'Matrículas [delete]'],

            // Messsages
            ['name' => 'view_messages', 'details' => 'Mensagens [view]'],
            ['name' => 'add_messages', 'details' => 'Mensagens [add]'],
            ['name' => 'edit_messages', 'details' => 'Mensagens [edit]'],
            ['name' => 'delete_messages', 'details' => 'Mensagens [delete]'],

            // Settings
            ['name' => 'view_settings', 'details' => 'Configurações [view]'],
            ['name' => 'add_settings', 'details' => 'Configurações [add]'],
            ['name' => 'edit_settings', 'details' => 'Configurações [edit]'],
            ['name' => 'delete_settings', 'details' => 'Configurações [delete]'],

            // Permissions
            ['name' => 'view_permissions', 'details' => 'Permissões [view]'],
            ['name' => 'add_permissions', 'details' => 'Permissões [add]'],
            ['name' => 'edit_permissions', 'details' => 'Permissões [edit]'],
            ['name' => 'delete_permissions', 'details' => 'Permissões [delete]'],

            // Roles
            ['name' => 'view_roles', 'details' => 'Grupos [view]'],
            ['name' => 'add_roles', 'details' => 'Grupos [add]'],
            ['name' => 'edit_roles', 'details' => 'Grupos [edit]'],
            ['name' => 'delete_roles', 'details' => 'Grupos [delete]'],

            // Users
            ['name' => 'view_users', 'details' => 'Usuários [view]'],
            ['name' => 'add_users', 'details' => 'Usuários [add]'],
            ['name' => 'edit_users', 'details' => 'Usuários [edit]'],
            ['name' => 'delete_users', 'details' => 'Usuários [delete]'],

            // Users
            ['name' => 'view_birthdays', 'details' => 'Aniversariantes [view]'],
        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission['name'],
                'details' => $permission['details'],
                'guard_name' => 'dashboard'
            ]);
        }
    }
}
