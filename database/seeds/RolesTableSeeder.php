<?php

use Illuminate\Database\Seeder;
use App\Core\Models\Auth\Role;
use App\Core\Models\Auth\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Root Role
        $root = Role::create([
            'name'       => 'root',
            'details'    => 'Root',
            'guard_name' => 'dashboard'
        ]);

        $root->syncPermissions(Permission::all());

        // Create Gestor Role
        $gestor = Role::create([
            'name'       => 'gestor',
            'details'    => 'Gestor',
            'guard_name' => 'dashboard'
        ]);

        // Create Funcionario Role
        $funcionario = Role::create([
            'name'       => 'funcionario',
            'details'    => 'Funcionário',
            'guard_name' => 'dashboard'
        ]);

        // Sync permissions
        $gestor->syncPermissions(Permission::whereNotIn('name', [
            'view_roles', 'add_roles', 'edit_roles', 'delete_roles',
            'view_users', 'add_users', 'edit_users', 'delete_users',
        ])->get());

        // Sync permissions
        $funcionario->syncPermissions(Permission::whereNotIn('name', [
            'view_audits', 'add_audits', 'edit_audits', 'delete_audits',
            'view_roles', 'add_roles', 'edit_roles', 'delete_roles',
            'view_users', 'add_users', 'edit_users', 'delete_users',
            'view_permissions', 'add_permissions', 'edit_permissions', 'delete_permissions',
        ])->get());

        // Create Aluno Role
        $aluno = Role::create([
            'name'       => 'aluno',
            'details'    => 'Aluno',
            'guard_name' => 'dashboard'
        ]);

        // Create Professor Role
        $professor = Role::create([
            'name'       => 'professor',
            'details'    => 'Professor',
            'guard_name' => 'dashboard'
        ]);

        // Create Responsável Role
        $responsavel = Role::create([
            'name'       => 'responsavel',
            'details'    => 'Responsável',
            'guard_name' => 'dashboard'
        ]);

        // Sync permissions
        $professor->syncPermissions(Permission::whereIn('name', [
            'view_dashboard',
        ])->get());

        // Sync permissions
        $aluno->syncPermissions(Permission::whereIn('name', [
            'view_dashboard',
        ])->get());

        // Sync permissions
        $responsavel->syncPermissions(Permission::whereIn('name', [
            'view_dashboard',
        ])->get());


    }
}
