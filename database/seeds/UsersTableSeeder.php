<?php

use Illuminate\Database\Seeder;
use App\Core\Models\Auth\User;
use App\Core\Models\Unit\Unit;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $betemuller = User::create([
            'nickname'         => 'BoletoFacil',
            'email'            => 'boletofacil@user.com.br',
            'password'         => bcrypt('senha'),
            'receive_messages' => false,
            'active'           => true
        ]);

        $qualitare = User::create([
            'nickname'         => 'Suporte',
            'email'            => 'suporte@qualitare.com.br',
            'password'         => bcrypt('senha'),
            'receive_messages' => false,
            'active'           => true,
        ]);

        $marcolino = User::create([
            'nickname'         => 'Thyago',
            'email'            => 'contato@tmteam.com.br',
            'password'         => bcrypt('senha'),
            'receive_messages' => false,
            'active'           => true,
        ]);

        $qualitare->assignRole('root');
        $marcolino->assignRole('root');
        $betemuller->assignRole('gestor');
    }
}
