<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToStudentsPerStatusReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW students_per_status_report AS
                SELECT date,
                       registration_id,
                       classroom_id,
                       IF(payment_type = 4, 0, status) AS status,
                       r.subsidiary_id AS subsidiary_id
                FROM registrations_frequencies rf
                         INNER JOIN registrations r ON rf.registration_id = r.id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_status_report AS
            SELECT date,
                   registration_id,
                   classroom_id,
                   status
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id;
        ');
    }
}
