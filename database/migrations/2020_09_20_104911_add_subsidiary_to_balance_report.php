<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToBalanceReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW balance_report AS
            SELECT SUM(total_charged)         AS total_charged,
                   SUM(total_received)        AS total_received,
                   SUM(total_charge_discount) AS total_charge_discount,
                   SUM(total_charge_fees)     AS total_charge_fees,
                   SUM(total_spent)           AS total_spent,
                   SUM(total_paid)            AS total_paid,
                   SUM(total_spent_discount)  AS total_spent_discount,
                   SUM(total_spent_fees)      AS total_spent_fees,
                   competence_month,
                   competence_year,
                   subsidiary_id
            FROM ((SELECT SUM(IF(type = 1, balance_records.amount, 0)) AS total_charged,
                          0                                            AS total_received,
                          0                                            AS total_charge_discount,
                          0                                            AS total_charge_fees,
                          SUM(IF(type = 2, balance_records.amount, 0)) AS total_spent,
                          0                                            AS total_paid,
                          0                                            AS total_spent_discount,
                          0                                            AS total_spent_fees,
                          MONTH(balance_records.due_date)              AS competence_month,
                          YEAR(balance_records.due_date)               AS competence_year,
                          balance_records.subsidiary_id
                   FROM balance_records
                   GROUP BY balance_records.due_date, balance_records.subsidiary_id)
                  UNION ALL
                  (SELECT 0                                                 AS total_charged,
                          SUM(IF(type = 1, balance_records.amount_paid, 0)) AS total_received,
                          SUM(IF(type = 1,
                                 IF(balance_records.amount_paid IS NULL, 0, balance_records.discount_amount),
                                 0))                                        AS total_charge_discount,
                          SUM(IF(type = 1, balance_records.charge_fee, 0))  AS total_charge_fees,
                          0                                                 AS total_spent,
                          SUM(IF(type = 2, balance_records.amount_paid, 0)) AS total_paid,
                          SUM(IF(type = 2,
                                 IF(balance_records.amount_paid IS NULL, 0, balance_records.discount_amount),
                                 0))                                        AS total_spent_discount,
                          SUM(IF(type = 2, balance_records.charge_fee, 0))  AS total_spent_fees,
                          MONTH(balance_records.payment_date)               AS competence_month,
                          YEAR(balance_records.payment_date)                AS competence_year,
                          balance_records.subsidiary_id
                   FROM balance_records
                   WHERE payment_date IS NOT NULL
                   GROUP BY balance_records.payment_date, balance_records.subsidiary_id)) AS balances
            GROUP BY competence_year, competence_month, subsidiary_id
            ORDER BY competence_year DESC, competence_month DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            CREATE VIEW balance_report AS
            SELECT
                SUM(IF(type = 1, balance_records.amount, 0)) AS total_charged,
                SUM(IF(type = 1, balance_records.amount_paid, 0)) AS total_received,
                SUM(IF(type = 1, IF(balance_records.amount_paid IS NULL, 0, balance_records.discount_amount), 0)) AS total_charge_discount,
                SUM(IF(type = 1, balance_records.charge_fee, 0)) AS total_charge_fees,
                SUM(IF(type = 2, balance_records.amount, 0)) AS total_spent,
                SUM(IF(type = 2, balance_records.amount_paid, 0)) AS total_paid,
                SUM(IF(type = 2, IF(balance_records.amount_paid IS NULL, 0, balance_records.discount_amount), 0)) AS total_spent_discount,
                SUM(IF(type = 2, balance_records.charge_fee, 0)) AS total_spent_fees,
                MONTH(balance_records.due_date) AS competence_month,
                YEAR(balance_records.due_date) AS competence_year
            FROM balance_records
            WHERE canceled < 1
            GROUP BY competence_year, competence_month
            ORDER BY competence_year DESC, competence_month DESC
        ");
    }
}
