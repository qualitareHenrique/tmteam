<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsPerStatusReportView2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_status_report AS
            SELECT CASE
                   WHEN status=3 THEN r.updated_at
                   ELSE rf.date END as date,
                   registration_id,
                   classroom_id,
                   status
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            CREATE OR REPLACE VIEW students_per_status_report AS
            SELECT date,
                   registration_id,
                   classroom_id,
                   status
            FROM registrations_frequencies rf
                     INNER JOIN registrations r ON rf.registration_id = r.id;
        ');
    }
}
