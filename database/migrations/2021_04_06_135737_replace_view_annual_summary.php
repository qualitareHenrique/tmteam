<?php

use Illuminate\Database\Migrations\Migration;

class ReplaceViewAnnualSummary extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW annual_financial_summary AS
            SELECT
                MAX(DATE_FORMAT(reg.effective_date, '%Y-%m')) referring,
                COUNT(distinct reg.id) new_registrations,
                SUM(bal_revenues.amount_paid) revenues,
                SUM(bal_expeditures.amount_paid) expeditures
            FROM registrations reg
            LEFT JOIN
                balance_records bal_revenues
                    ON DATE_FORMAT(bal_revenues.payment_date, '%Y-%m') = DATE_FORMAT(reg.effective_date, '%Y-%m')
                         AND bal_revenues.type = 1
            LEFT JOIN
                balance_records bal_expeditures
                    ON DATE_FORMAT(bal_expeditures.payment_date, '%Y-%m') = DATE_FORMAT(reg.effective_date, '%Y-%m')
                         AND bal_expeditures.type = 2
            WHERE effective_date IS NOT NULL
            GROUP BY YEAR(effective_date), MONTH(effective_date);
        ");
    }

    /**
     * @return void
     */
    public function down()
    {
    }
}
