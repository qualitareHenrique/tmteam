<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255)->unique();
            $table->date('effective_date');
            $table->decimal('acquisition', 10, 2)->default(0);
            $table->decimal('amount', 10, 2)->default(0);
            $table->decimal('advance_discount', 10, 2)->default(0);
            $table->unsignedInteger('payment_type')->default(1);
            $table->unsignedInteger('payment_day')->default(1);
            $table->unsignedInteger('status')->default(0);
            $table->unsignedInteger('student_id')->index();
            $table->foreign('student_id')->references('id')->on('students');
            $table->timestamps();
        });

        Schema::create('registrations_classrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('classroom_id')->index();
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
            $table->unsignedInteger('registration_id')->index();
            $table->foreign('registration_id')->references('id')->on('registrations')->onDelete('cascade');
        });

        Schema::create('registrations_bands', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('registration_id')->index();
            $table->foreign('registration_id')->references('id')->on('registrations');
            $table->unsignedInteger('band_id')->index();
            $table->foreign('band_id')->references('id')->on('bands');
            $table->integer('degree')->default(0);
            $table->timestamps();
        });

        Schema::create('registrations_frequencies', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->boolean('reviewed')->default(false);
            $table->unsignedInteger('classroom_id')->index();
            $table->foreign('classroom_id')->references('id')->on('classrooms');
            $table->unsignedInteger('registration_id')->index();
            $table->foreign('registration_id')->references('id')->on('registrations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
        Schema::dropIfExists('registrations_classrooms');
        Schema::dropIfExists('registrations_bands');
        Schema::dropIfExists('registrations_frequencies');
    }
}
