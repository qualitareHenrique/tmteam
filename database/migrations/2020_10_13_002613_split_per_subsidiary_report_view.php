<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SplitPerSubsidiaryReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW split_per_subsidiary_report AS
            SELECT SUM(expected) AS expected,
                   SUM(received) AS received,
                   competence_month,
                   competence_year,
                   subsidiary_id
            FROM ((SELECT SUM(balance_records.split_amount) AS expected,
                          0                                 AS received,
                          MONTH(balance_records.due_date)   AS competence_month,
                          YEAR(balance_records.due_date)    AS competence_year,
                          balance_records.subsidiary_id     AS subsidiary_id
                   FROM balance_records
                   GROUP BY subsidiary_id, competence_month, competence_year)
                  UNION ALL
                  (SELECT 0                                   AS expected,
                          SUM(balance_records.split_amount)   AS received,
                          MONTH(balance_records.payment_date) AS competence_month,
                          YEAR(balance_records.payment_date)  AS competence_year,
                          balance_records.subsidiary_id       AS subsidiary_id
                   FROM balance_records
                   WHERE payment_date IS NOT NULL
                   GROUP BY competence_month, competence_year, subsidiary_id)) AS sumary
            GROUP BY subsidiary_id, competence_month, competence_year
            ORDER BY competence_year DESC, competence_month DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW split_per_subsidiary_report');
    }
}
