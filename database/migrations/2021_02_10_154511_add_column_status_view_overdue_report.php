<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusViewOverdueReport extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW overdue_report AS
                SELECT u.name                                                           AS student_name,
                       u.email                                                          AS student_email,
                       u.phone                                                          AS phone,
                       group_concat(date_format(br.due_date, '%d/%m/%Y') SEPARATOR ',') AS overdues,
                       group_concat(br.id SEPARATOR ',')                                AS records,
                       r.subsidiary_id                                                  AS subsidiary_id,
                       r.status                                                         AS status,
                       s.id                                                             AS student_id
                FROM (((students s JOIN users u ON ((s.user_id = u.id))) JOIN registrations r ON ((s.id = r.student_id)))
                    JOIN balance_records br
                    ON (((br.chargeable_id = r.id) AND (br.chargeable_type like '%Registration'))))
                WHERE ((br.due_date < curdate()) AND (isnull(br.payment_date) OR (br.payment_date = '0000-00-00')))
                GROUP BY student_name, student_email
            ;
        ");
    }

    /**
     * @return void
     */
    public function down()
    {
        DB::statement("
            CREATE OR REPLACE VIEW overdue_report AS
                SELECT u.name                                                           AS student_name,
                       u.email                                                          AS student_email,
                       group_concat(date_format(br.due_date, '%d/%m/%Y') SEPARATOR ',') AS overdues,
                       r.subsidiary_id                                                  AS subsidiary_id
                FROM (((students s JOIN users u ON ((s.user_id = u.id))) JOIN registrations r ON ((s.id = r.student_id)))
                    JOIN balance_records br
                    ON (((br.chargeable_id = r.id) AND (br.chargeable_type like '%Registration'))))
                WHERE ((br.due_date < curdate()) AND (isnull(br.payment_date) OR (br.payment_date = '0000-00-00')))
                GROUP BY student_name, student_email
            ;
        ");
    }
}
