<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 100);
            $table->integer('type')->default(0);
            $table->decimal('amount', 10, 2);
            $table->decimal('amount_paid', 10, 2)->nullable();
            $table->decimal('discount_amount', 10, 2)->nullable();
            $table->integer('discount_days')->default(0);
            $table->date('due_date');
            $table->date('payment_date')->nullable();
            $table->integer('payment_type');
            $table->boolean('canceled')->default(false);
            $table->string('charge_code')->nullable();
            $table->string('charge_link')->nullable();
            $table->decimal('charge_fee', 10, 2)->nullable();
            $table->string('chargeable_type')->nullable();
            $table->integer('chargeable_id')->unsigned()->nullable();
            $table->index(['chargeable_type', 'chargeable_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_records');
    }
}
