<?php

use Illuminate\Database\Migrations\Migration;

class UpdateAnnualSummary extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW annual_financial_summary AS
            SELECT MAX(DATE_FORMAT(reg.effective_date, '%Y-%m')) referring,
                   COUNT(distinct reg.id)                        new_registrations,
                   MAX(revenues)                                 as revenues,
                   MAX(expeditures)                              as expeditures
            FROM registrations reg
                     LEFT JOIN (
                        SELECT
                             MAX(DATE_FORMAT(bal_revenues.payment_date, '%Y-%m')) referring,
                             SUM(bal_revenues.amount_paid) revenues
                        FROM balance_records bal_revenues
                        WHERE bal_revenues.type = 1
                        AND bal_revenues.canceled = 0
                        GROUP BY YEAR(payment_date), MONTH(payment_date)
                     ) bal_revenues
                            ON bal_revenues.referring = DATE_FORMAT(reg.effective_date, '%Y-%m')
                     LEFT JOIN (
                        SELECT
                             MAX(DATE_FORMAT(bal_expeditures.payment_date, '%Y-%m')) referring,
                             SUM(bal_expeditures.amount_paid) expeditures
                        FROM balance_records bal_expeditures
                        WHERE bal_expeditures.type = 2
                        AND bal_expeditures.canceled = 0
                        GROUP BY YEAR(payment_date), MONTH(payment_date)
                     ) bal_expeditures
                            ON bal_expeditures.referring = DATE_FORMAT(reg.effective_date, '%Y-%m')
            WHERE effective_date IS NOT NULL
            GROUP BY YEAR(effective_date), MONTH(effective_date);
        ");
    }

    /**
     * @return void
     */
    public function down()
    {
    }
}
