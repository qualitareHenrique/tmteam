<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 255)->unique();
            $table->boolean('experimental')->default(false);
            $table->string('cbjj', 255)->nullable();
            $table->unsignedInteger('origin_id')->index();
            $table->foreign('origin_id')->references('id')->on('origins');
            $table->unsignedInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('person_id')->index();
            $table->foreign('person_id')->references('id')->on('people');
            $table->unsignedInteger('age_category_id')->index()->nullable();
            $table->foreign('age_category_id')->references('id')->on('ages_categories');
            $table->unsignedInteger('responsible_id')->index()->nullable();
            $table->foreign('responsible_id')->references('id')->on('responsibles');
            $table->timestamps();
        });

        Schema::create('students_aptitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('status')->nullable()->default(1);
            $table->boolean('release')->nullable()->default(0);
            $table->boolean('follow')->nullable()->default(0);
            $table->unsignedInteger('student_id')->index();
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->date('expiration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_aptitudes');
        Schema::dropIfExists('students');
    }
}
