<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableChargeTeacherSplit extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        Schema::create('charge_teacher_splits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('balance_record_id')->index();
            $table->unsignedInteger('classroom_id')->index();
            $table->unsignedInteger('teacher_id')->index();
            $table->foreign('balance_record_id')->references('id')->on('balance_records');
            $table->foreign('classroom_id')->references('id')->on('classrooms');
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->decimal('split_value', 10, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charge_teacher_splits');
    }
}
