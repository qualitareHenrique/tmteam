<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW balance_report AS
            SELECT
                SUM(IF(type = 1, balance_records.amount, 0)) AS total_charged,
                SUM(IF(type = 1, balance_records.amount_paid, 0)) AS total_received,
                SUM(IF(type = 1, IF(balance_records.amount_paid IS NULL, 0, balance_records.discount_amount), 0)) AS total_charge_discount,
                SUM(IF(type = 1, balance_records.charge_fee, 0)) AS total_charge_fees,
                SUM(IF(type = 2, balance_records.amount, 0)) AS total_spent,
                SUM(IF(type = 2, balance_records.amount_paid, 0)) AS total_paid,
                SUM(IF(type = 2, IF(balance_records.amount_paid IS NULL, 0, balance_records.discount_amount), 0)) AS total_spent_discount,
                SUM(IF(type = 2, balance_records.charge_fee, 0)) AS total_spent_fees,
                MONTH(balance_records.due_date) AS competence_month,
                YEAR(balance_records.due_date) AS competence_year
            FROM balance_records
            WHERE canceled < 1
            GROUP BY competence_year, competence_month
            ORDER BY competence_year DESC, competence_month DESC
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW balance_report');
    }
}
