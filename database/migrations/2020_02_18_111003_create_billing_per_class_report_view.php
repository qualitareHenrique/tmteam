<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateBillingPerClassReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE VIEW billing_per_class_report AS
            SELECT c.id                    as id,
                   c.name                  as classroom,
                   SUM(br.amount)          as expected,
                   SUM(br.amount_paid)     as received,
                   SUM(IF(br.payment_date IS NULL, 0, br.discount_amount)) as discount,
                   SUM(br.charge_fee)      as charge_fee,
                   MONTH(br.due_date)      AS competence_month,
                   YEAR(br.due_date)       AS competence_year
            FROM balance_records br
                     INNER JOIN registrations r on br.chargeable_id = r.id AND r.status <> 3
                     INNER JOIN registrations_classrooms rc on r.id = rc.registration_id
                     INNER JOIN classrooms c on rc.classroom_id = c.id
            WHERE chargeable_type like \'%Registration\'
            GROUP BY id, competence_month, competence_year
            ORDER BY id, competence_year DESC, competence_month DESC
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW billing_per_class_report');
    }
}
