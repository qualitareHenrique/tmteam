<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDadosBancariosTeacher extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->string('bank_number', 4)->nullable()->after('user_id');
            $table->string('agency_number', 20)->nullable()->after('bank_number');
            $table->string('account_number', 20)->nullable()->after('agency_number');
            $table->string('account_complement_number', 4)->nullable()->after('account_number');
            $table->string('account_type', 10)->nullable()->after('account_complement_number');
            $table->string('account_holder_name', 100)->nullable()->after('account_type');
            $table->string('account_holder_document', 14)->nullable()->after('account_holder_name');
            $table->string('gateway_id', 50)->nullable()->after('account_holder_document');
            $table->string('gateway_status', 20)->nullable()->after('gateway_id');
            $table->string('resource_token', 191)->nullable()->after('gateway_status');
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::table('balance_records', function (Blueprint $table) {
            $table->dropColumn('bank_number');
            $table->dropColumn('agency_number');
            $table->dropColumn('account_number');
            $table->dropColumn('account_complement_number');
            $table->dropColumn('account_type');
            $table->dropColumn('account_holder_name');
            $table->dropColumn('account_holder_document');
            $table->dropColumn('gateway_id');
            $table->dropColumn('gateway_status');
            $table->dropColumn('resource_token');
        });
    }
}
