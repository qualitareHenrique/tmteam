<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountDataToSubsidiaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subsidiaries', function (Blueprint $table) {
            $table->string('fantasy_name', 100)->after('name')->nullable();
            $table->string('cnpj', 14)->after('fantasy_name')->nullable();
            $table->string('company_type', 50)->after('cnpj')->nullable();
            $table->string('business_area', 50)->after('company_type')->nullable();
            $table->string('lines_of_business', 100)->after('business_area')->nullable();
            $table->string('bank_number', 4)->after('lines_of_business')->nullable();
            $table->string('agency_number', 20)->after('bank_number')->nullable();
            $table->string('account_number', 20)->after('agency_number')->nullable();
            $table->string('account_complement_number', 4)->after('account_number')->nullable();
            $table->string('account_type', 10)->after('account_complement_number')->nullable();
            $table->string('account_holder_name', 100)->after('account_type')->nullable();
            $table->string('account_holder_document', 14)->after('account_holder_name')->nullable();
            $table->string('gateway_id', 50)->after('manager_id')->nullable();
            $table->string('resource_token')->after('gateway_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subsidiaries', function (Blueprint $table) {
            $table->dropColumn('fantasy_name');
            $table->dropColumn('cnpj');
            $table->dropColumn('company_type');
            $table->dropColumn('business_area');
            $table->dropColumn('lines_of_business');
            $table->dropColumn('bank_number');
            $table->dropColumn('agency_number');
            $table->dropColumn('account_number');
            $table->dropColumn('account_complement_number');
            $table->dropColumn('account_type');
            $table->dropColumn('account_holder_name');
            $table->dropColumn('account_holder_document');
            $table->dropColumn('resource_token');
            $table->dropColumn('gateway_id');
        });
    }
}
