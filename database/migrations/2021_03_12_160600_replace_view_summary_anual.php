<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceViewSummaryAnual extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW annual_financial_summary AS
            SELECT
                MAX(DATE_FORMAT(reg.effective_date, '%Y-%m')) referring,
                COUNT(distinct reg.id) new_registrations,
                MAX(active_students.active_students) active_students,
                SUM(bal_revenues.amount_paid) revenues,
                SUM(bal_expeditures.amount_paid) expeditures
            FROM registrations reg
            LEFT JOIN
                balance_records bal_revenues
                    ON DATE_FORMAT(bal_revenues.payment_date, '%Y-%m') = DATE_FORMAT(reg.effective_date, '%Y-%m')
                         AND bal_revenues.type = 1
            LEFT JOIN
                balance_records bal_expeditures
            
                    ON DATE_FORMAT(bal_expeditures.payment_date, '%Y-%m') = DATE_FORMAT(reg.effective_date, '%Y-%m')
                         AND bal_expeditures.type = 2
            LEFT JOIN
                (SELECT
                    COUNT(r.id) active_students,
                    MAX(DATE_FORMAT(date, '%Y-%m')) referring
                FROM registrations r
                JOIN registrations_frequencies rf ON rf.registration_id = r.id
                GROUP BY r.id, YEAR(date), MONTH(date)) active_students
                    ON active_students.referring = DATE_FORMAT(reg.effective_date, '%Y-%m')
            WHERE effective_date IS NOT NULL
            GROUP BY YEAR(effective_date), MONTH(effective_date);
        ");
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annual_financial_summary');
    }
}
