<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubsidiaryToBillingPerClassReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW billing_per_class_report AS
            SELECT id,
                   classroom,
                   SUM(expected)   AS expected,
                   SUM(received)   AS received,
                   SUM(discount)   AS discount,
                   SUM(charge_fee) AS charge_fee,
                   competence_month,
                   competence_year,
                   subsidiary_id
            FROM ((SELECT classrooms.id                  AS id,
                         classrooms.name                 AS classroom,
                         SUM(balance_records.amount)     AS expected,
                         0                               AS received,
                         0                               AS discount,
                         0                               AS charge_fee,
                         MONTH(balance_records.due_date) AS competence_month,
                         YEAR(balance_records.due_date)  AS competence_year,
                         balance_records.subsidiary_id   AS subsidiary_id
                  FROM balance_records
                           INNER JOIN registrations ON balance_records.chargeable_id = registrations.id
                           INNER JOIN (SELECT registration_id, classroom_id
                                       FROM registrations_classrooms
                                       GROUP BY registration_id) AS registrations_classrooms
                                      ON registrations.id = registrations_classrooms.registration_id
                           INNER JOIN classrooms ON registrations_classrooms.classroom_id = classrooms.id
                  WHERE chargeable_type like '%Registration'
                  GROUP BY classrooms.id, competence_month, competence_year, subsidiary_id)
            UNION ALL
                (SELECT classrooms.id                            AS id,
                        classrooms.name                          AS classroom,
                        0                                        AS expected,
                        SUM(balance_records.amount_paid)         AS received,
                        SUM(IF(balance_records.payment_date IS NULL, 0,
                               balance_records.discount_amount)) AS discount,
                        SUM(balance_records.charge_fee)          AS charge_fee,
                        MONTH(balance_records.payment_date)      AS competence_month,
                        YEAR(balance_records.payment_date)       AS competence_year,
                        balance_records.subsidiary_id            AS subsidiary_id
                 FROM balance_records
                          INNER JOIN registrations ON balance_records.chargeable_id = registrations.id
                          INNER JOIN (SELECT registration_id, classroom_id
                                      FROM registrations_classrooms
                                      GROUP BY registration_id) AS registrations_classrooms
                                     ON registrations.id = registrations_classrooms.registration_id
                          INNER JOIN classrooms ON registrations_classrooms.classroom_id = classrooms.id
                 WHERE chargeable_type like '%Registration'
                   AND payment_date IS NOT NULL
                 GROUP BY classrooms.id, competence_month, competence_year, subsidiary_id)) AS sumary
            GROUP BY classroom, competence_month, competence_year
            ORDER BY classroom, competence_year DESC, competence_month DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            CREATE OR REPLACE VIEW billing_per_class_report AS
                SELECT c.id                    as id,
                       c.name                  as classroom,
                       SUM(br.amount)          as expected,
                       SUM(br.amount_paid)     as received,
                       SUM(IF(br.payment_date IS NULL, 0, br.discount_amount)) as discount,
                       SUM(br.charge_fee)      as charge_fee,
                       MONTH(br.due_date)      AS competence_month,
                       YEAR(br.due_date)       AS competence_year
                FROM balance_records br
                         INNER JOIN registrations r on br.chargeable_id = r.id AND r.status <> 3
                         INNER JOIN registrations_classrooms rc on r.id = rc.registration_id
                         INNER JOIN classrooms c on rc.classroom_id = c.id
                WHERE chargeable_type like '%Registration'
                GROUP BY id, competence_month, competence_year
                ORDER BY id, competence_year DESC, competence_month DESC
        ");
    }
}
