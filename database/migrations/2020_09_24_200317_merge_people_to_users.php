<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MergePeopleToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->after('id')->nullable();
            $table->char('cpf', 11)->after('nickname')->nullable();
            $table->char('phone', 20)->after('email')->nullable();
            $table->date('birthdate')->after('phone')->nullable();
            $table->unsignedInteger('sex')->after('birthdate')->nullable();
            $table->char('cep', 11)->after('sex')->nullable();
            $table->string('street', 100)->after('cep')->nullable();
            $table->char('number', 20)->after('street')->nullable();
            $table->string('neighborhood', 50)->after('number')->nullable();
            $table->string('complement', 100)->after('neighborhood')->nullable();
            $table->string('city', 50)->after('complement')->nullable();
            $table->char('state', 2)->after('city')->nullable();
        });

        DB::statement("
            UPDATE users u
                 LEFT JOIN students s ON u.id = s.user_id
                 LEFT JOIN teachers t ON u.id = t.user_id
                 LEFT JOIN guardians g ON u.id = g.user_id
                 INNER JOIN people p ON s.person_id = p.id OR t.person_id = p.id OR g.person_id = p.id
            SET u.name = p.name,
                u.cpf = p.cpf,
                u.phone = p.phone,
                u.sex = p.sex,
                u.birthdate = p.birthdate,
                u.cep = p.cep,
                u.street = p.street,
                u.city = p.city,
                u.neighborhood = p.neighborhood,
                u.state = p.state,
                u.complement = p.complement,
                u.number = p.number
            WHERE u.id > 0;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('cpf');
            $table->dropColumn('phone');
            $table->dropColumn('sex');
            $table->dropColumn('birthdate');
            $table->dropColumn('cep');
            $table->dropColumn('street');
            $table->dropColumn('city');
            $table->dropColumn('neighborhood');
            $table->dropColumn('state');
            $table->dropColumn('complement');
            $table->dropColumn('number');
        });
    }
}
