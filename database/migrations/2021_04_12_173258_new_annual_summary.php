<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewAnnualSummary extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW annual_financial_summary AS
            SELECT
                MAX(DATE_FORMAT(br.payment_date, '%Y-%m')) referring,
                SUM(IF(br.type = 1, br.amount_paid, 0)) as revenues,
                SUM(IF(br.type = 2, br.amount_paid, 0)) as expeditures
            FROM balance_records  br
            WHERE br.canceled = 0
            GROUP BY YEAR(br.payment_date), MONTH(br.payment_date);"
        );
    }
}
