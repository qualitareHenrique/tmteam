<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSplitValueIntoClassromsTeacthers extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        Schema::table('classrooms_teachers', function (Blueprint $table) {
            $table->decimal('split_value', 10, 2)->default(0);
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::table('classrooms_teachers', function (Blueprint $table) {
            $table->dropColumn('split_valoue');
        });
    }
}
