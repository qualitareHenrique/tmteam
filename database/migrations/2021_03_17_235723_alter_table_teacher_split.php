<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTeacherSplit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE charge_teacher_splits
            DROP FOREIGN KEY charge_teacher_splits_balance_record_id_foreign;
        ");

        DB::statement("
            ALTER TABLE charge_teacher_splits
            ADD CONSTRAINT charge_teacher_splits_balance_record_id_foreign
            FOREIGN KEY (balance_record_id)
            REFERENCES balance_records(id) ON DELETE CASCADE ON UPDATE RESTRICT;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
