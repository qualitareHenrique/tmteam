<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampColumnClassroomTeacher extends Migration
{
    public function up()
    {
        Schema::table('classrooms_teachers', function (Blueprint $table) {
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('classrooms_teachers', function (Blueprint $table) {
            $table->dropTimestamps();
        });
    }
}
