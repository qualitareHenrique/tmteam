<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingSummaryReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE OR REPLACE VIEW billing_summary_report AS
            SELECT date,
                   classroom,
                   classroom_id,
                   SUM(expected_inconming) AS expected_incoming,
                   SUM(real_incoming)      AS real_incoming,
                   SUM(paid_charges)       AS paid_charges,
                   SUM(total_charges)      AS total_charges
            FROM (SELECT balance_records.due_date               AS date,
                         SUM(IFNULL(balance_records.amount, 0)) AS expected_inconming,
                         0                                      AS real_incoming,
                         0                                      AS paid_charges,
                         COUNT(balance_records.id)              AS total_charges,
                         classrooms.name                        AS classroom,
                         classrooms.id                          AS classroom_id
                  FROM balance_records
                           INNER JOIN registrations
                                      ON balance_records.chargeable_type LIKE
                                         "%Registration" AND
                                         balance_records.chargeable_id = registrations.id
                           INNER JOIN registrations_classrooms ON registrations.id =
                                                                  registrations_classrooms.registration_id
                           INNER JOIN classrooms ON registrations_classrooms.classroom_id =
                                                    classrooms.id
                  GROUP BY balance_records.due_date, classrooms.id
                  UNION ALL
                  SELECT balance_records.payment_date                AS date,
                         0                                           AS expetec_incoming,
                         SUM(IFNULL(balance_records.amount_paid, 0)) AS real_incoming,
                         COUNT(balance_records.id)                   AS paid_charges,
                         0                                           AS total_charges,
                         classrooms.name                             AS classroom,
                         classrooms.id                               AS classroom_id
                  FROM balance_records
                           INNER JOIN registrations
                                      ON balance_records.chargeable_type LIKE
                                         "%Registration" AND
                                         balance_records.chargeable_id = registrations.id
                           INNER JOIN registrations_classrooms ON registrations.id =
                                                                  registrations_classrooms.registration_id
                           INNER JOIN classrooms ON registrations_classrooms.classroom_id =
                                                    classrooms.id
                  WHERE payment_date IS NOT NULL
                  GROUP BY payment_date, classrooms.id) AS summary
            GROUP BY date, classroom_id;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW billing_summary_report');
    }
}
