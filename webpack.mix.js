const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/styles/app.scss', 'public/css')
    .styles([
        'resources/template/css/style.css',
        'resources/template/css/components.css',
        'resources/styles/css/common.css'
    ], 'public/css/global.css')
    .styles('resources/styles/css/member.css', 'public/css/member.css')
    .styles('resources/styles/css/access.css', 'public/css/access.css')
    .js('resources/js/app.js', 'public/js')
    .scripts([
        'resources/template/js/stisla.js',
        'resources/template/js/scripts.js',
        'resources/js/scripts/admin.js',
        'resources/js/scripts/mask.js',
        'resources/js/scripts/cep.js',
        'resources/js/scripts/piechart.js',
        'resources/js/scripts/profile.js',
        'resources/js/scripts/notification.js',
        'resources/js/scripts/academic.js',
        'resources/js/scripts/financial.js',
        'resources/js/scripts/band.js',
        'resources/js/scripts/datepicker.js'
    ], 'public/js/global.js')
    .copyDirectory('resources/img', 'public/img');

mix.version();
