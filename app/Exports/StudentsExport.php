<?php

namespace App\Exports;

use App\Core\Filters\StudentFilter;
use App\Core\Services\TrainingCenter\StudentService;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StudentsExport implements FromView
{
    protected StudentService $service;
    use Exportable;

    public function __construct(StudentService $service)
    {
        $this->service = $service;
    }

    public function view(): View
    {
        $students = $this->service->list(new StudentFilter(new Request()));

        return view('admin.students.export', [
            'students' => $students
        ]);
    }

}
