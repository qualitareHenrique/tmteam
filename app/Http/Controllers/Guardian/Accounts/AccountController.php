<?php

namespace App\Http\Controllers\Guardian\Accounts;

use App\Core\DTOs\GuardianData;
use App\Core\Services\TrainingCenter\GuardianService;
use App\Http\Controllers\Guardian\BaseController;
use App\Http\Requests\SaveGuardian;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AccountController extends BaseController
{
    use SEOToolsTrait;

    protected GuardianService $service;

    /**
     * AccountController constructor.
     *
     * @param \App\Core\Services\TrainingCenter\GuardianService $service
     */
    public function __construct(GuardianService $service)
    {
        $this->service = $service;

        parent::__construct();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return View
     */
    public function edit()
    {
        $this->seo()->setTitle('Conta | Aluno');

        $model = Auth::user()->guardian;

        return view('guardians.accounts.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\SaveGuardian $request
     * @param int $id
     *
     * @return RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Exception
     */
    public function update(SaveGuardian $request, int $id)
    {
        $data = GuardianData::fromRequest($request);
        $data->id = $id;

        $this->service->createOrUpdate($data);

        toast()->success('Dados atualizados com sucesso', 'Sucesso');

        return redirect()->route('teachers.accounts.edit');
    }
}
