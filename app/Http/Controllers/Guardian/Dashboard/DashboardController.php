<?php

namespace App\Http\Controllers\Guardian\Dashboard;

use App\Http\Controllers\Guardian\BaseController;
use App\Core\Models\Message;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class DashboardController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $this->seo()->setTitle('Dashboard | Aluno');

        $guardian = Auth::user()->guardian;
        $messages    = Message::nonExpired()->get();
        $dependents  = $guardian->students;

        return view('guardians.dashboard.index',
            compact('guardian', 'messages', 'dependents')
        );
    }

}
