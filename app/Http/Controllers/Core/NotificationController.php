<?php

namespace App\Http\Controllers\Core;

use App\Core\Models\Notification;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Notification $notification
     *
     * @return JsonResponse|RedirectResponse
     */
    public function update(Request $request, Notification $notification)
    {
        $notification->markAsRead();

        $msg = 'Notificação alterada com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Notification $notification
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request, Notification $notification)
    {
        $notification->delete();

        $msg = 'Notificação removida com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg, 'route' => '/'], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
