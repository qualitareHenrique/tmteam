<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveRole;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Core\Models\Auth\Role;
use App\Core\Models\Auth\Permission;
use Illuminate\Support\Facades\Auth;

class RoleController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo()->setTitle('Grupos');

        $this->middleware('permission:view_roles', ['only' => ['index']]);
        $this->middleware('permission:add_roles', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_roles', ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $results = Role::byUser(Auth::user())
                       ->orderBy('details', 'ASC')
                       ->paginate(20);

        return view('admin.roles.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {

        $permissions = Permission::pluck('details', 'id');

        return view('admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveRole $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SaveRole $request)
    {
        $data = $request->validated();
        $result = Role::create($data);

        $result->syncPermissions($request->get('permissions', []));

        toast()->success('Grupo criado com sucesso.');

        return redirect()->route('admin.roles.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Core\Models\Auth\Role $role
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Role $role)
    {
        $permissions = Permission::pluck('details', 'id');

        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveRole $request
     *
     * @param \App\Core\Models\Auth\Role $role
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SaveRole $request, Role $role)
    {
        $data = $request->validated();

        $role->fill($data)->save();
        $role->syncPermissions($request->get('permissions', []));

        toast()->success('Grupo atualizado com sucesso.');

        return redirect()->route('admin.roles.index');
    }
}
