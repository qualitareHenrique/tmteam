<?php

namespace App\Http\Controllers\Admin\Finances;

use App\Core\Services\Financial\MonthlyCharge;
use App\Http\Controllers\Admin\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class MonthlyChargeController extends BaseController
{
    use SEOToolsTrait;

    protected $service;

    public function __construct(MonthlyCharge $service)
    {
        $this->middleware('permission:add_balance_records', ['only' => ['store']]);

        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'competence' => 'required|date_format:Y-m'
        ]);

        $this->service->generate($data['competence']);

        toast()->success("Mensalidades criadas com sucesso.", 'Sucesso');

        return redirect()->route('admin.invoices.index');
    }
}
