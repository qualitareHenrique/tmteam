<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Core\Filters\OverdueFilter;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\Report\Overdue;
use App\Core\Models\Subsidiary;
use App\Core\Services\Financial\BalanceRecordService;
use App\Enums\ResendTypesEnum;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class OverdueController extends Controller
{
    /** @var string  */
    public const OVERDUES_RESENDS = 'Cobranças reenviadas com sucesso.';

    use SEOToolsTrait;
    
    /** @var BalanceRecordService  */
    private BalanceRecordService $recordService;

    /**
     * @param BalanceRecordService $recordService
     */
    public function __construct(BalanceRecordService $recordService)
    {
        $this->recordService = $recordService;
    }

    public function index(Request $request, OverdueFilter $filter)
    {
        $this->seo()->setTitle('Relatório de inadimplência');

        $subsidiaries = Subsidiary::pluck('name', 'id');
        $records      = Overdue::filter($filter)
                               ->paginate(20)
                               ->appends($request->except('page'));

        $types = new ResendTypesEnum();

        return view('admin.reports.overdue', compact('records', 'subsidiaries', 'types'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resend(Request $request)
    {
        try {
            $data = $this->recordService->resendInvoices(
                $request->get('student'),
                $request->get('type')
            );

            return new JsonResponse([
                'message' => self::OVERDUES_RESENDS,
                'data'    => $data
            ]);
        } catch (\DomainException $domainException) {
            return new JsonResponse(['erro' => $domainException->getMessage()]);
        }
    }
}
