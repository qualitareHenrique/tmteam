<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Admin\BaseController;
use App\Core\Models\Report\QualifiedStudents;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\View\View;

class QualificationController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo()->setTitle('Qualificações');

        $this->middleware('permission:view_graduations', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     * @return View
     */
    public function index()
    {
        $results = QualifiedStudents::all();

        return view('admin.qualifications.index', compact('results'));
    }

    /**
     * Print a listing of the resource.
     * @return View
     */
    public function print()
    {
        $this->seo()->setTitle('Qualificações');

        $results = QualifiedStudents::all();

        return view('admin.qualifications.print', compact('results'));
    }
}
