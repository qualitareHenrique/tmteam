<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Core\Models\Report\AnnualSummary;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools;
use App\Core\Filters\AnnualSummaryFilter;
use App\Enums\MonthsEnum;
use Carbon\Carbon;

class AnnualSummaryController extends Controller
{
    use SEOTools;

    /**
     * @param AnnualSummaryFilter $filter
     */
    public function index(AnnualSummaryFilter $filter)
    {
        $this->seo()->setTitle('Resumo Anual');
        
        $year = $filter->filters()['year'] ?? null;

        if (!$year) {
            $year = Carbon::now()->format('y');

            $filter->addFilters(['year' => $year]);
        }

        if ((int) $year < 19) {
            toast()->error('Ano não pose ser inferior a 2019');
            $filter->addFilters(['year' => 19]);
        }

        $summary        = AnnualSummary::filter($filter)->get();
        $monthsOptions  = MonthsEnum::OPTIONS;

        return view('admin.reports.annual-summary', compact('summary', 'monthsOptions'));
    }
}
