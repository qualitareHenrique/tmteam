<?php

namespace App\Http\Controllers\Admin\Juno;

use App\Core\Filters\InvoiceFilter;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\Subsidiary;
use App\Core\Services\Financial\BalanceRecordService;
use App\Core\Services\Juno\Exceptions\AccountNotVerifiedException;
use App\Core\Services\Juno\Exceptions\InvalidSplitDistribution;
use App\Core\Services\Juno\Exceptions\ResponseErrorException;
use App\Http\Controllers\Admin\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class InvoiceController extends BaseController
{
    use SEOToolsTrait;

    /** @var string */
    private const INVOICE_CANCELED = 'A cobrança foi cancelada com sucesso.';
    private const MANUAL_PAYMENT   = 'A baixa manual da cobrança foi realizada.';

    protected BalanceRecordService $service;

    public function __construct(BalanceRecordService $service)
    {
        $this->seo()->setTitle('Faturas');

        $this->middleware('permission:view_balance_records', ['only' => ['index']]);
        $this->middleware('permission:add_balance_records', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_balance_records', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_balance_records', ['only' => ['destroy']]);

        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\InvoiceFilter $filter
     *
     * @return View
     */
    public function index(Request $request, InvoiceFilter $filter)
    {
        $fail         = $request->get('fail');
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $records      = BalanceRecord::filter($filter)
                                     ->invoice()
                                     ->inTime()
                                     ->orderBy('due_date', 'ASC')
                                     ->paginate(20)
                                     ->appends($request->except('page'));

        return view('admin.invoices.index', compact('records', 'subsidiaries', 'fail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'balance_records' => 'required|array|min:1'
        ]);

        try {

            foreach ($data['balance_records'] as $id) {
                $this->service->charge($id);
            }

            toast()->success('Registros faturados com sucesso.', 'Sucesso');

        } catch (ResponseErrorException $ex){
            toast()->error($ex->getMessage(), 'Erro');

            Log::channel('system')->error($ex->errorBag);
        }  catch (InvalidSplitDistribution $ex) {
            toast()->warning($ex->getMessage(), 'Atenção');
        }

        return redirect()->route('admin.invoices.index');
    }

    /**
     * @param BalanceRecord $invoice
     * @return JsonResponse
     * @throws ResponseErrorException
     */
    public function cancel(BalanceRecord $invoice): JsonResponse
    {
        $this->service->cancelGateway($invoice);

        return new JsonResponse(['success' => self::INVOICE_CANCELED]);
    }

    /**
     * @param BalanceRecord $invoice
     * @return JsonResponse
     */
    public function manulPayment(Request $request, BalanceRecord $invoice): JsonResponse
    {
        $this->service->manualPayment($invoice, (bool) $request->get('cancelGateway'));

        return new JsonResponse(['success' => self::MANUAL_PAYMENT]);
    }
}
