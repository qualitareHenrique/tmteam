<?php


namespace App\Http\Controllers\Admin\Juno;


use App\Core\Services\Juno\AccountBalanceService;
use App\Http\Controllers\Admin\BaseController;
use Auth;
use Illuminate\Http\Request;

class TransferController extends BaseController
{

    private AccountBalanceService $service;

    /**
     * GatewayController constructor.
     *
     * @param \App\Core\Services\Juno\AccountBalanceService $service
     */
    public function __construct(AccountBalanceService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $subsidiary = Auth::user()->subsidiary;
        $result = $this->service->fetch($subsidiary);

        return view('admin.transfer.index', compact('result', 'subsidiary'));
    }

    public function store(Request $request)
    {
        $subsidiary = Auth::user()->subsidiary;
        $value = (float)$request->input('requested_amount');

        $this->service->transfer($subsidiary, $value);

        toast()->success('Valor transferido com sucesso', 'Sucesso');

        return redirect()->route('admin.transfer.index');
    }
}