<?php

namespace App\Http\Controllers\Admin;

use App\Core\Models\Setting;
use App\Enums\SplitTypeEnum;
use App\Http\Requests\SaveSettings;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $settings = Setting::pluck('value', 'key');
        $splitOptions = SplitTypeEnum::options();

        return view('admin.settings.edit', compact('settings', 'splitOptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\SaveSettings $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SaveSettings $request)
    {
        $data = $request->validated();

        foreach ($data as $key => $value){
            Setting::where('key', $key)->update(['value' => $value]);
        }

        toast()->success('Configurações atualizadas com sucesso', 'Sucesso');

        return redirect()->route('admin.settings.edit');
    }

}
