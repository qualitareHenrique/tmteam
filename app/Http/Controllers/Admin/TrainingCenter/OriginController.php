<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\Filters\OriginFilter;
use App\Core\Models\TrainingCenter\Origin;
use Exception;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveOrigin;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use Illuminate\Http\Request;


class OriginController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->middleware('permission:view_students_origins', ['only' => ['index']]);
        $this->middleware('permission:add_students_origins', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_students_origins', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_students_origins', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\OriginFilter $filter
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request, OriginFilter $filter)
    {
        $this->seo()->setTitle('Origens');

        $results = Origin::filter($filter)
                         ->latest()
                         ->paginate(20)
                         ->appends($request->except('page'));

        return view('admin.origins.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->seo()->setTitle('Origens - Nova Origem');

        return view('admin.origins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveOrigin $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SaveOrigin $request)
    {
        Origin::create($request->validated());

        toast()->success('Origem criada com sucesso.', 'Sucesso');

        return redirect()->route('admin.origins.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Origin $origin
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Origin $origin)
    {
        $this->seo()->setTitle('Origens - Editar Origem');

        return view('admin.origins.edit', compact('origin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveOrigin $request
     * @param Origin $origin
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SaveOrigin $request, Origin $origin)
    {
        $origin->fill($request->validated())->save();

        toast()->success('Origem atualizada com sucesso.', 'Sucesso');

        return redirect()->route('admin.origins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Origin $origin
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request, Origin $origin)
    {
        $origin->delete();

        $msg = 'Origem removida com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
