<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use Exception;

use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveBand;
use App\Core\Models\TrainingCenter\Modality;
use App\Core\Models\TrainingCenter\Band;
use App\Core\Models\TrainingCenter\AgeCategory;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class BandController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->seo()->setTitle('Faixas');

        $this->middleware('permission:view_bands', ['only' => ['index']]);
        $this->middleware('permission:add_bands', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_bands', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_bands', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request)
    {
        $bands = Band::latest()
                     ->paginate(20)
                     ->appends($request->except('page'));


        return view('admin.bands.index', compact('bands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $ageCategories = AgeCategory::asc()
                                    ->get()
                                    ->pluck('name', 'id');

        $modalities = Modality::asc()
                              ->whereActiveGraduation(true)
                              ->pluck('name', 'id');

        $bands = Band::all();

        return view('admin.bands.create', compact(
            'modalities', 'ageCategories', 'bands'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveBand $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(SaveBand $request)
    {
        DB::beginTransaction();

        $data = $request->validated();
        $band = Band::create($data);

        if ($band) {

            $band->ageCategories()->attach($data['age_categories']);
            $band->degrees()->createMany($data['degrees']);

            DB::commit();

            toast()->success('Faixa criada com sucesso.', 'Sucesso');

            return redirect()->route('admin.bands.index');
        }

        DB::rollback();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Band $band
     *
     * @return View
     */
    public function edit(Band $band)
    {
        $ageCategories = AgeCategory::asc()
                                    ->get()
                                    ->pluck('name', 'id');

        $modalities = Modality::asc()
                              ->pluck('name', 'id');

        $bands = Band::all();

        return view('admin.bands.edit', compact(
            'band', 'modalities', 'ageCategories', 'bands'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveBand $request
     * @param Band $band
     *
     * @return RedirectResponse
     * @throws Exception
     */
    public function update(SaveBand $request, Band $band)
    {
        DB::beginTransaction();

        $data = $request->validated();

        if ($band->fill($data)->save()) {

            $band->ageCategories()->sync($data['age_categories']);

            foreach ($data['degrees'] as $degree) {

                if (!empty($degree['_destroy'])) {
                    $band->degrees()->find($degree['id'])->delete();
                } else {
                    $band->degrees()->updateOrCreate([
                        'id' => $degree['id'] ?? null
                    ], $degree);
                }
            }

            DB::commit();

            toast()->success('Faixa atualizada com sucesso.', 'Sucesso');

            return redirect()->route('admin.bands.index');
        }

        DB::rollback();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Band $band
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request, Band $band)
    {
        $band->delete();

        $msg = 'Faixa removida com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
