<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\DTOs\StudentData;
use App\Core\Filters\StudentFilter;
use App\Core\Models\Subsidiary;
use App\Core\Services\TrainingCenter\StudentService;
use App\Enums\RegistrationStatusEnum;
use App\Core\Models\TrainingCenter\Band;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Registration;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveStudent;

use App\Core\Models\TrainingCenter\Modality;
use App\Core\Models\TrainingCenter\AgeCategory;
use App\Core\Models\TrainingCenter\Student;
use App\Core\Models\TrainingCenter\Origin;

use App\Exports\StudentsExport;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

use Exception;
use Maatwebsite\Excel\Excel;

class StudentController extends BaseController
{
    use SEOToolsTrait;

    protected StudentService $service;
    private $modalities;
    private $origins;
    private $ageCategories;
    private $bands;
    private $classrooms;
    private $subsidiaries;

    /**
     * Constructor
     *
     * @param \App\Core\Services\TrainingCenter\StudentService $service
     */
    public function __construct(StudentService $service)
    {
        $this->seo()->setTitle('Alunos');

        $this->middleware('permission:view_students', ['only' => ['index']]);
        $this->middleware('permission:add_students', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_students', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_students', ['only' => ['destroy']]);

        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\StudentFilter $filter
     *
     * @return View
     */
    public function index(Request $request, StudentFilter $filter)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $students     = $this->service->list($filter, 20, $request->except('page'));
        $status       = new RegistrationStatusEnum;

        return view('admin.students.index', compact(
            'students', 'subsidiaries', 'status'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $this->setOptions();

        return view('admin.students.create')
            ->with('subsidiaries', $this->subsidiaries)
            ->with('modalities', $this->modalities)
            ->with('origins', $this->origins)
            ->with('ageCategories', $this->ageCategories)
            ->with('bands', $this->bands)
            ->with('classrooms', $this->classrooms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveStudent $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(SaveStudent $request)
    {
        DB::beginTransaction();

        try {
            $data = StudentData::fromRequest($request);

            $this->service->createOrUpdate($data);
        } catch (Exception $ex) {
            DB::rollBack();

            throw $ex;
        }

        DB::commit();

        toast()->success('Aluno adicionado com sucesso.', 'Sucesso');

        return redirect()->route('admin.students.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id)
    {
        $this->setOptions();

        $student = $this->service->find($id);

        return view('admin.students.edit')
            ->with('subsidiaries', $this->subsidiaries)
            ->with('student', $student)
            ->with('modalities', $this->modalities)
            ->with('origins', $this->origins)
            ->with('ageCategories', $this->ageCategories)
            ->with('bands', $this->bands)
            ->with('classrooms', $this->classrooms)
            ->with('registrationStatus', new RegistrationStatusEnum())
        ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveStudent $request
     * @param int $id
     *
     * @return RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(SaveStudent $request, int $id)
    {
        DB::beginTransaction();

        try {
            $data     = StudentData::fromRequest($request);
            $data->id = $id;

            $this->service->createOrUpdate($data);
        } catch (Exception $ex) {
            DB::rollBack();

            throw $ex;
        }

        DB::commit();

        toast()->success('Aluno atualizado com sucesso.', 'Sucesso');

        return redirect()->route('admin.students.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Student $student
     *
     * @return RedirectResponse|JsonResponse
     *
     * @throws Exception
     */
    public function destroy(Request $request, Student $student)
    {
        $student->delete();

        if ($request->ajax()) {
            return response()->json(['message' => 'Aluno removido com sucesso.'], 200);
        }

        toast()->success('Aluno removido com sucesso.', 'Success');

        return redirect()->back();
    }

    /**
     * Deactivate the specified user
     *
     * @param Request $request
     * @param Registration $registration
     *
     * @return RedirectResponse
     */
    public function skipCheck(Request $request, Registration $registration)
    {
        $request->merge([
            'skip_checking_from' => date('Y-m-d', time()),
            'status'             => RegistrationStatusEnum::REGULAR
        ]);

        $registration->fill($request->all());
        $registration->save();

        toast()->success('Liberação registrada com sucesso.', 'Sucesso');

        return redirect()->back();
    }

    /**
     * Set combo options for views
     */
    private function setOptions()
    {
        $this->modalities    = Modality::options();
        $this->origins       = Origin::options();
        $this->bands         = Band::options();
        $this->ageCategories = AgeCategory::select()->asc()->get();
        $this->classrooms    = Classroom::withoutGlobalScopes()->get();
        $this->subsidiaries  = Subsidiary::withoutGlobalScopes()->get();
    }

    /**
     * @param Request $request
     * @param Registration $registration
     *
     * @return JsonResponse
     */
    public function deactivateRegistration(Request $request, Registration $registration): JsonResponse
    {
        try {
            $this->service->updateStatus(
                $registration,
                RegistrationStatusEnum::CANCELLED,
                (bool) $request->get('deactive_installments')
            );
        }
        catch (Exception $e) {
            dd($e);
        }

        return new JsonResponse(['success' => $this->service::DISABLED_REGISTRATION]);
    }

    /**
     * @param Request $request
     * @param Registration $registration
     *
     * @return JsonResponse
     */
    public function reactivateRegistration(Request $request, Registration $registration): JsonResponse
    {
        $this->service->updateStatus($registration, RegistrationStatusEnum::REGULAR);

        return new JsonResponse(['success' => $this->service::REACTIVATED_REGISTRATION]);
    }

    public function export( Excel $excel, StudentsExport $export)
    {
        return $excel->download($export, 'students.xlsx');
    }


}
