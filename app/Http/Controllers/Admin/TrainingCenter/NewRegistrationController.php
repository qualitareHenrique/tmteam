<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\DTOs\GuardianData;
use App\Core\DTOs\RegistrationData;
use App\Core\DTOs\StudentData;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Degree;
use App\Core\Services\TrainingCenter\StudentService;
use App\Core\Models\TrainingCenter\Band;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Registration;
use App\Http\Requests\SaveNewRegistrationRegistration;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveStudent;

use App\Core\Models\TrainingCenter\Modality;
use App\Core\Models\TrainingCenter\AgeCategory;
use App\Core\Models\TrainingCenter\Graduation;
use App\Core\Models\TrainingCenter\Student;
use App\Core\Models\TrainingCenter\Origin;
use App\Core\Services\Financial\InstallmentService;
use App\Core\Services\TrainingCenter\GuardianService;
use App\Core\Services\TrainingCenter\RegistrationService;
use App\Http\Requests\NewRegistrationSaveStudent;
use App\Http\Requests\SaveGraduation;
use App\Http\Requests\SaveGuardian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewRegistrationController extends BaseController
{
    use SEOToolsTrait;

    private StudentService $service;
    private GuardianService $guardianService;
    private RegistrationService $registrationService;
    private InstallmentService $installmentService;

    private $modalities;
    private $origins;
    private $ageCategories;
    private $bands;
    private $classrooms;
    private $subsidiaries;
    private $degrees;
    private $modality;

    /**
     * @param StudentService $service
     */
    public function __construct(
        StudentService $service,
        GuardianService $guardianService,
        RegistrationService $registrationService,
        InstallmentService $installmentService
    ) {
        $this->seo()->setTitle('Alunos');

        $this->service = $service;
        $this->guardianService = $guardianService;
        $this->registrationService = $registrationService;
        $this->installmentService = $installmentService;
    }

    private function setOptions(): void
    {
        $this->modalities    = Modality::options();
        $this->origins       = Origin::options();
        $this->bands         = Band::options();
        $this->ageCategories = AgeCategory::select()->asc()->get();
        $this->classrooms    = Classroom::withoutGlobalScopes()->get();
        $this->subsidiaries  = Subsidiary::withoutGlobalScopes()->get();
    }

    private function setOptionsGraduation(Registration $registration, Classroom $classroom)
    {
        $teachers         = [];
        $this->modality   = [$classroom->modality->id => $classroom->modality->name];

        $classroom->teachers->map(function ($teacher) use (&$teachers) {
            $teachers[$teacher->id] = $teacher->user->name;
        });

        $this->teachers = $teachers;
        $this->bands    = Band::whereModalityId($classroom->modality->id)->get();
        $this->degrees  = Degree::whereIn('band_id',
            $this->bands->pluck('id')
        )->get();
    }

    public function index(Request $request)
    {
        $origins       = Origin::options();
        $ageCategories = AgeCategory::select()->asc()->get();
        $subsidiaries  = Subsidiary::withoutGlobalScopes()->get();
        $student       = $request->get('studentId') 
            ? Student::whereId($request->get('studentId'))->get()->first()
            : null;;

        return view('admin.new-registration.student')
            ->with('subsidiaries', $subsidiaries)
            ->with('student', $student)
            ->with('origins', $origins)
            ->with('ageCategories', $ageCategories)
        ;
    }

    /**
     * @param Student $student
     */
    public function guardian(Student $student)
    {
        $origins       = Origin::options();
        $ageCategories = AgeCategory::select()->asc()->get();

        return view('admin.new-registration.guardian')
            ->with('origins', $origins)
            ->with('ageCategories', $ageCategories)
            ->with('student', $student)
            ->with('guardian', $student->guardian)
        ;
    }

    /**
     * @param Student $student
     */
    public function registration(Student $student)
    {
        $classrooms = Classroom::withoutGlobalScopes()->get()->filter(function ($index) use ($student) {
            return $index->subsidiary_id === $student->user->subsidiary_id;
        });

        return view('admin.new-registration.registration')
            ->with('classrooms', $classrooms)
            ->with('hasGuardian', $student->user->birthdate->age < 18)
            ->with('student', $student)
        ;
    }

    /**
     * @param Student $student
     */
    public function graduation(Request $request, Student $student)
    {
        $classrooms = $student->registration->classrooms;
        $classrooms = $classrooms->filter(function ($class) use ($student) {
            return $class->modality->active_graduation
                && !Graduation::hasGraduation($student->registration->id, $class->modality->id);
        });

        if ($classrooms->isEmpty()) {
            toast()->success('Maricula realizada com sucesso.', 'Sucesso!');

            return redirect()->route("admin.dashboard.index");
        }

        $classroom = $classrooms->first();

        $this->setOptionsGraduation($student->registration, $classroom);

        return view('admin.new-registration.graduation')
            ->with('registration', $student->registration)
            ->with('bands', $this->bands)
            ->with('degrees', $this->degrees)
            ->with('classroom', $classroom)
            ->with('teachers', $this->teachers)
            ->with('modality', $this->modality)
            ->with('hasGuardian', $student->user->birthdate->age < 18)
            ->with('student', $student)
        ;
    }

    /**
     * @param SaveStudent $request
     */
    public function saveStudent(NewRegistrationSaveStudent $request)
    {
        $this->setOptions();

        $data    = StudentData::fromRequest($request);
        $student = $this->service->createOrUpdate($data);
        $step    =  $student->user->birthdate->age >= 18 ? 'registration' : 'guardian';

        return redirect()->route("admin.new-registration.$step", [
            'student' => $student
        ]);
    }

    /**
     * @param SaveGuardian $request
     */
    public function saveGuardian(SaveGuardian $request, Student $student)
    {
        $data = GuardianData::fromRequest($request);

        $guardian = $this->guardianService->createOrUpdate($data);
        $this->service->updateGuardian($guardian, $student);

        return redirect()->route("admin.new-registration.registration", [
            'student' => $student
        ]);
    }

    /**
     * @param SaveNewRegistrationRegistration $request
     * @param Student $student
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveRegistration(SaveNewRegistrationRegistration $request, Student $student)
    {
        try {
            DB::beginTransaction();

            if ($student->registration) {
                return redirect()->route("admin.new-registration.graduation", ['student' => $student]);
            }

            $data           = (new RegistrationData())->fromRequest($request, $student);
            $registration   = $this->registrationService->createOrUpdate($data);
            $balanceRecord  = new BalanceRecord(
                $this->installmentService->getDataFromRegistration($request->validated()));
            $registration->installments()->save($balanceRecord);

            DB::commit();

            return redirect()->route("admin.new-registration.graduation", ['student' => $student]);
        } catch (\Throwable $throwable) {
            DB::rollBack();

            toast()->error('Ocorreu um erro durante a solicitação. verifique todos os campos preenchidos.', 'Atenção');

            return redirect()->route("admin.new-registration.registration", [
                'student' => $student
            ]);
        }
    }

    /**
     * @param SaveGraduation $request
     * @param Student $student
     */
    public function saveGraduation(SaveGraduation $request, Student $student, Classroom $classroom)
    {
        $data = $request->validated();

        Graduation::create($data);

        return redirect()->route('admin.new-registration.graduation', [
            'student' => $student,
            'classroom' => $classroom
        ]);
    }
}
