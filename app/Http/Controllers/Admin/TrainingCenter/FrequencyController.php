<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\DTOs\FrequencyData;
use App\Core\Filters\FrequencyFilter;
use App\Core\Services\TrainingCenter\FrequencyService;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveFrequency;
use App\Core\Models\TrainingCenter\Teacher;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Student;
use App\Core\Models\TrainingCenter\Frequency;
use Illuminate\View\View;

class FrequencyController extends BaseController
{
    use SEOToolsTrait;

    protected FrequencyService $service;

    public function __construct(FrequencyService $frequencyService)
    {
        $this->seo()->setTitle("Frequências");

        $this->middleware('permission:view_classrooms_submodules', ['only' => ['index']]);
        $this->middleware('permission:add_classrooms_submodules', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_classrooms_submodules', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_classrooms_submodules', ['only' => ['destroy']]);

        $this->service = $frequencyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Classroom $classroom
     * @param \App\Core\Filters\FrequencyFilter $filter
     *
     * @return View
     */
    public function index(
        Request $request,
        Classroom $classroom,
        FrequencyFilter $filter
    ) {
        $students = $classroom->selectStudents()
                              ->orderBy('users.name')
                              ->pluck('users.name', 'registrations.id');

        $filter->addFilters(['classroom' => $classroom->id]);
        $results = $this->service->list($filter, 10, $request->except('page'));

        return view('admin.classrooms.frequencies.index',
            compact('results', 'classroom', 'students')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Classroom $classroom
     *
     * @return View
     */
    public function create(Classroom $classroom)
    {
        $teachers = Teacher::with('user')
                           ->get()
                           ->pluck('user.name', 'id');

        $students = $classroom->selectStudents()
                              ->orderBy('users.name')
                              ->pluck('users.name', 'registrations.id');

        return view('admin.classrooms.frequencies.create',
            compact('teachers', 'students', 'classroom')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveFrequency $request
     * @param Classroom $classroom
     *
     * @return RedirectResponse
     */
    public function store(SaveFrequency $request, Classroom $classroom)
    {
        $this->service->createMany($request);

        toast()->success('Frequência criada com sucesso.', 'Sucesso');

        return redirect()->route('admin.classrooms.frequencies.index', [
            'classroom_id' => $classroom->id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Classroom $classroom
     * @param int $id
     *
     * @return View
     */
    public function edit(Classroom $classroom, int $id)
    {
        $frequency = $this->service->find($id);
        $teachers  = Teacher::with('user')
                            ->get()
                            ->pluck('user.name', 'id');

        $students = Student::with('user')
                           ->whereHas('registration', function ($query) use ($frequency) {
                               $query->where('id', $frequency->registration_id);
                           })->get()
                           ->pluck('user.name', 'registration.id');

        return view('admin.classrooms.frequencies.edit',
            compact('frequency', 'teachers', 'students', 'classroom')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveFrequency $request
     * @param Classroom $classroom
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(
        SaveFrequency $request,
        Classroom $classroom,
        int $id
    ) {
        $data     = FrequencyData::fromRequest($request);
        $data->id = $id;

        $this->service->createOrUpdate($data);

        toast()->success('Frequência atualizada com sucesso.', 'Sucesso');

        return redirect()->route('admin.classrooms.frequencies.index', [
            'classroom_id' => $classroom->id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Classroom $classroom
     * @param Frequency $frequency
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(
        Request $request,
        Classroom $classroom,
        Frequency $frequency
    ) {
        $frequency->delete();

        $msg = 'Freqüência removida com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
