<?php


namespace App\Http\Controllers\Admin\TrainingCenter;


use App\Core\Models\TrainingCenter\Registration;
use App\Http\Controllers\Admin\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class IssueController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->seo()->setTitle('Pendências');
    }

    public function index(Registration $registration)
    {
        $records = $registration->issues;

        return view('admin.students.issues.index', compact(
            'records', 'registration'
        ));
    }
}