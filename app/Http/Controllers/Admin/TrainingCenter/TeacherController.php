<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\DTOs\TeacherData;
use App\Core\Filters\TeacherFilter;
use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Modality;
use App\Core\Services\Juno\AccountService;
use App\Core\Services\TrainingCenter\TeacherService;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveTeacher;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TeacherController extends BaseController
{
    use SEOToolsTrait;

    protected TeacherService $service;
    protected AccountService $accountService;

    public function __construct(TeacherService $service, AccountService $accountService)
    {
        $this->seo()->setTitle('Professores');

        $this->middleware('permission:view_teachers', ['only' => ['index']]);
        $this->middleware('permission:add_teachers', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_teachers', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_teachers', ['only' => ['destroy']]);

        $this->service = $service;
        $this->accountService = $accountService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\TeacherFilter $filter
     *
     * @return View
     */
    public function index(Request $request, TeacherFilter $filter)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $results      = $this->service->list($filter, 20, $request->except('page'));

        return view('admin.teachers.index', compact(
            'results', 'subsidiaries'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $modalities   = Modality::asc()->pluck('name', 'id');
        $banks        = collect($this->accountService->listBanks())->pluck('name', 'number');

        return view('admin.teachers.create', compact(
            'modalities', 'subsidiaries', 'banks'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveTeacher $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(SaveTeacher $request)
    {
        $data = TeacherData::fromRequest($request);

        $this->service->createOrUpdate($data);

        toast()->success('Professor criado com sucesso.', 'Sucesso');

        return redirect()->route('admin.teachers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $modalities   = Modality::asc()->pluck('name', 'id');
        $teacher      = $this->service->find($id);
        $banks        = collect($this->accountService->listBanks())->pluck('name', 'id');

        return view('admin.teachers.edit', compact(
            'teacher', 'modalities', 'subsidiaries', 'banks'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveTeacher $request
     * @param int $id
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(SaveTeacher $request, int $id)
    {
        $data     = TeacherData::fromRequest($request);
        $data->id = $id;

        $this->service->createOrUpdate($data);

        toast()->success('Professor atualizado com sucesso.', 'Sucesso');

        return redirect()->route('admin.teachers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws \Exception
     */
    public function destroy(Request $request, int $id)
    {
        $this->service->delete($id);

        $msg = 'Professor removido com sucesso.';

        if ($request->ajax()) {
            return response()->json(['message' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
