<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\Filters\ModalityFilter;
use App\Http\Requests\SaveModality;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use App\Http\Controllers\Admin\BaseController;
use App\Core\Models\TrainingCenter\Modality;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ModalityController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->seo()->setTitle('Modalidades');

        $this->middleware('permission:view_modalities', ['only' => ['index']]);
        $this->middleware('permission:add_modalities', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_modalities', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_modalities', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\ModalityFilter $filter
     *
     * @return view
     */
    public function index(Request $request, ModalityFilter $filter)
    {
        $modalities = Modality::filter($filter)
                              ->latest()
                              ->paginate(20)
                              ->appends($request->except('page'));

        return view('admin.modalities.index', compact('modalities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.modalities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveModality $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SaveModality $request)
    {
        $data = $request->validated();
        $data['user_id'] = Auth::user()->id;

        Modality::create($data);

        toast()->success('Modalidade criada com sucesso.', 'Sucesso');

        return redirect()->route('admin.modalities.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Modality $modality
     *
     * @return View
     */
    public function edit(Modality $modality)
    {
        return view('admin.modalities.edit', compact('modality'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveModality $request
     * @param Modality $modality
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SaveModality $request, Modality $modality)
    {
        $modality->fill($request->all())->save();

        toast()->success('Modalidade atualizada com sucesso.', 'Sucesso');

        return redirect()->route('admin.modalities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Modality $modality
     *
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function destroy(Request $request, Modality $modality)
    {
        $modality->delete();

        if ($request->ajax()) {
            return response()->json(['message' => 'Aluno removido com sucesso.'], 200);
        }

        toast()->success('Aluno removido com sucesso.', 'Success');

        return redirect()->back();
    }
}
