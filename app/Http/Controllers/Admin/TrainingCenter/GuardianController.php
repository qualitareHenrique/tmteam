<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\DTOs\GuardianData;
use App\Core\Filters\GuardianFilter;
use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Student;
use App\Core\Services\TrainingCenter\GuardianService;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveGuardian;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GuardianController extends BaseController
{
    use SEOToolsTrait;

    protected GuardianService $service;

    /**
     * GuardianController constructor.
     *
     * @param \App\Core\Services\TrainingCenter\GuardianService $service
     */
    public function __construct(GuardianService $service)
    {
        $this->seo()->setTitle('Responsáveis');

        $this->middleware('permission:view_guardians', ['only' => ['index']]);
        $this->middleware('permission:add_guardians', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_guardians', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_guardians', ['only' => ['destroy']]);

        $this->service = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\GuardianFilter $filter
     *
     * @return View
     */
    public function index(Request $request, GuardianFilter $filter)
    {
        $subsidiaries = Subsidiary::pluck('name', 'id');
        $results      = $this->service->list($filter, 20, $request->except('page'));

        return view('admin.guardians.index', compact('results', 'subsidiaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $subsidiaries = Subsidiary::withoutGlobalScopes()->pluck('name', 'id');
        $students = Student::with('user')->get()->pluck('user.name','id');

        return view('admin.guardians.create', compact('subsidiaries', 'students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveGuardian $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(SaveGuardian $request)
    {
        $data = GuardianData::fromRequest($request);

        $guardian = $this->service->createOrUpdate($data);

        if ($request->ajax()) {
            $guardian->load('user');
            return response()->json(compact('guardian'));
        }

        toast()->success('Responsável criado com sucesso.', 'Sucesso');

        return redirect()->route('admin.guardians.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id)
    {
        $subsidiaries = Subsidiary::withoutGlobalScopes()->pluck('name', 'id');
        $guardian     = $this->service->find($id);
        $students = Student::with('user')->get()->pluck('user.name','id');

        return view('admin.guardians.edit', compact('guardian', 'subsidiaries', 'students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveGuardian $request
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(SaveGuardian $request, int $id)
    {
        $data     = GuardianData::fromRequest($request);
        $data->id = $id;

        $guardian = $this->service->createOrUpdate($data);

        if ($request->ajax()) {
            $guardian->load('user');
            return response()->json(compact('guardian'));
        }

        toast()->success('Responsável atualizado com sucesso.', 'Sucesso');

        return redirect()->route('admin.guardians.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request, int $id)
    {
        $this->service->delete($id);

        $msg = 'Responsável removido com sucesso.';

        if ($request->ajax()) {
            return response()->json(['msg' => $msg], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }
}
