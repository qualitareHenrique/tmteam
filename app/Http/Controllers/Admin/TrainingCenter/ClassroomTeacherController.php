<?php

namespace App\Http\Controllers\Admin\TrainingCenter;

use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\ClassroomsTeachers;
use App\Core\Models\TrainingCenter\Teacher;
use App\Core\Services\TrainingCenter\ClassroomTeacherService;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\SaveClassroomTeacher;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ClassroomTeacherController extends BaseController
{
    use SEOToolsTrait;

    private ClassroomTeacherService $service;

    /** @var string  */
    public const SUCCESS_REMOVED = 'Professor removido com sucesso.';

    public function __construct(ClassroomTeacherService $service)
    {
        $this->service = $service;
        $this->seo()->setTitle('Turma');

        $this->middleware('permission:edit_classrooms', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function addTeacher(Request $request): JsonResponse
    {
        try {
            $classroomTeachers = new Collection([$this->service->create($request)]);
            $content = view('admin.classrooms.partials.teachers-row', compact('classroomTeachers'))->render();
        } catch (\DomainException $domainException) {
            return new JsonResponse(['duplicated' => true]);
        }

        return new JsonResponse(['content' => $content]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function removeTeacher(Request $request): JsonResponse
    {
        $this->service->remove($request->get('id'));

        return new JsonResponse(['success' => self::SUCCESS_REMOVED]);
    }

    /**
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id)
    {
        $classroomTeacher = ClassroomsTeachers::whereId($id)->first();
        $teacher = $classroomTeacher->teacher()->first();
        $classroom = $classroomTeacher->classroom()->first();

        return view('admin.classrooms-teachers.edit', compact('classroomTeacher', 'teacher', 'classroom'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, int $id)
    {
        $classroomUpdated = $this->service->update($request->get('split_value'), $id);
        toast()->success('Professor atualizado com sucesso.', 'Sucesso');

        return redirect()->route('admin.classrooms.edit', ['id' => $classroomUpdated->classroom->id]);
    }
}
