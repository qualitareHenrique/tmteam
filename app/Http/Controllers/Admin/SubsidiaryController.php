<?php

namespace App\Http\Controllers\Admin;

use App\Core\DTOs\SubsidiaryData;
use App\Core\Filters\SubsidiaryFilter;
use App\Core\Models\Auth\User;
use App\Core\Models\Subsidiary;
use App\Core\Services\Juno\AccountService;
use App\Core\Services\Juno\Exceptions\ResponseErrorException;
use App\Core\Services\SubsidiaryService;
use App\Http\Requests\SaveSubsidiary;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SubsidiaryController extends Controller
{

    use SEOToolsTrait;

    protected SubsidiaryService $service;
    protected AccountService    $accountService;

    public function __construct(SubsidiaryService $service, AccountService $accountService)
    {
        $this->seo()->setTitle('Unidades');

        $this->middleware('permission:view_subsidiaries', ['only' => ['index']]);
        $this->middleware('permission:add_subsidiaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_subsidiaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_subsidiaries', ['only' => ['destroy']]);

        $this->service        = $service;
        $this->accountService = $accountService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\SubsidiaryFilter $filter
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request, SubsidiaryFilter $filter): View
    {
        $results = $this->service->list($filter, 20, $request->except('page'));

        return view('admin.subsidiaries.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create(): View
    {
        $subsidiary    = new Subsidiary;
        $companyTypes  = $this->companyTypes();
        $banks         = $this->banks();
        $businessAreas = $this->businessAreas();

        $users         = User::whereNotNull('name')
                             ->whereNotNull('cpf')
                             ->whereNotNull('birthdate')
                             ->pluck('name', 'id');


        return view('admin.subsidiaries.create',
            compact('users', 'banks', 'companyTypes', 'businessAreas', 'subsidiary')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\SaveSubsidiary $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function store(SaveSubsidiary $request): RedirectResponse
    {
        $data = SubsidiaryData::fromRequest($request);

        try {
            $subsidiary = $this->service->createOrUpdate($data);

            toast()->success('Unidade criada com sucesso.', 'Sucesso');

        } catch (ResponseErrorException $ex) {
            toast()->warning($ex->getMessage());

            Session::flash('gateway_messages', $ex->errorBag);

            if (isset($subsidiary)) {
                return redirect()->route('admin.subsidiaries.edit',
                    ['subsidiary' => $subsidiary->id]);
            }

            return redirect()->back();
        }

        return redirect()->route('admin.subsidiaries.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Subsidiary $subsidiary): View
    {
        $companyTypes  = $this->companyTypes();
        $banks         = $this->banks();
        $businessAreas = $this->businessAreas();
        $users         = User::whereNotNull('name')
                             ->whereNotNull('cpf')
                             ->whereNotNull('birthdate')
                             ->pluck('name', 'id');

        return view('admin.subsidiaries.edit',
            compact('users', 'banks', 'companyTypes', 'businessAreas', 'subsidiary')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\SaveSubsidiary $request
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(SaveSubsidiary $request, int $id): RedirectResponse
    {
        $data     = SubsidiaryData::fromRequest($request);
        $data->id = $id;

        try {

            $this->service->createOrUpdate($data);

            toast()->success('Unidade atualizada com sucesso.', 'Sucesso');

        } catch (ResponseErrorException $ex) {
            toast()->warning($ex->getMessage());

            Session::flash('gateway_messages', $ex->errorBag);

            return redirect()->back();
        }

        return redirect()->route('admin.subsidiaries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request, int $id)
    {
        $this->service->delete($id);

        $msg = 'Unidade removida com sucesso.';

        if ($request->ajax()) {
            return response()->json([
                'message' => $msg,
                'route'   => route('admin.subsidiaries.index')
            ], 200);
        }

        toast()->success($msg, 'Success');

        return redirect()->back();
    }

    private function companyTypes()
    {
        $types = $this->accountService->listCompanyTypes()->companyTypes;

        return array_combine($types, $types);
    }

    private function banks()
    {
        return collect(
            $this->accountService->listBanks()->banks
        )->pluck('name', 'number');
    }

    private function businessAreas()
    {
        return collect(
            $this->accountService->listBusinessAreas()->businessAreas
        )->pluck('category', 'code');
    }
}
