<?php

namespace App\Http\Controllers\Admin;

use App\Core\Filters\AuditFilter;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Core\Models\Audit;
use App\Core\Models\Auth\User;
use Illuminate\View\View;

class AuditController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->seo()->setTitle('Auditoria');

        $this->middleware('permission:view_audits', [
            'only' => ['index', 'show']
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @param \App\Core\Filters\AuditFilter $filter
     *
     * @return View
     */
    public function index(Request $request, AuditFilter $filter)
    {
        $actions = config('qualitare.audit.actions');
        $types   = config('qualitare.audit.types');

        $users   = User::pluck('nickname', 'id');
        $audits  = Audit::filter($filter)->latest()
                        ->paginate(50)
                        ->appends($request->except('page'));

        return view('admin.audits.index', compact(
            'audits', 'users', 'types', 'actions'
        ));
    }

    /**
     * Show the specified resource.
     *
     * @param Audit $audit
     *
     * @return View
     */
    public function show(Audit $audit)
    {
        return view('admin.audits.show', compact('audit'));
    }
}
