<?php

namespace App\Http\Controllers\Admin;

use App\Core\Models\UserTerm;
use App\Core\Services\UserTermService;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;
use Tymon\JWTAuth\Contracts\Providers\Auth as ProvidersAuth;

class UserTermController extends Controller
{
    use SEOToolsTrait;

    /**
     * @param UserTermService $service
     */
    public function __construct(UserTermService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        if (Auth::user()->hasRole(['aluno', 'responsavel'])) {
            return view('admin.term.student');    
        }

        if (Auth::user()->hasRole(['professor'])) {
            return view('admin.term.teacher');
        }

        return view('admin.term.form');
    }

    /**
     * @param Request $request
     */
    public function accept(Request $request)
    {
        if ($this->service->studentNot($request)) {
            return redirect()->route('auth.session.logout');
        }

        if ($this->service->teacherNot($request)) {
            return redirect()->route('auth.session.logout');
        }

        if ($this->service->userNot($request)) {
            return redirect()->route('auth.session.logout');
        }

        $this->service->accept();

        return redirect('/');
    }
}
