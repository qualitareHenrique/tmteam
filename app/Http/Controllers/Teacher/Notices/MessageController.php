<?php

namespace App\Http\Controllers\Teacher\Notices;

use App\Core\Models\TrainingCenter\Classroom;
use App\Http\Controllers\Teacher\BaseController;
use App\Http\Requests\SaveMessage;
use App\Core\Models\Message;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;


class MessageController extends BaseController
{
    use SEOToolsTrait;

    public function __construct()
    {
        $this->middleware('permission:add_messages', ['only' => ['create', 'store']]);

        parent::__construct();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $this->seo()->setTitle('Avisos');
        $classrooms = Classroom::whereActive(true)->pluck('name', 'id');

        return view('teachers.messages.create', compact('classrooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveMessage $request
     *
     * @return RedirectResponse
     */
    public function store(SaveMessage $request)
    {
        $data = $request->validated();

        Message::create($data);

        toast()->success('Mensagem criada com sucesso.', 'Sucesso');

        return redirect()->route('teachers.dashboard.index');
    }
}
