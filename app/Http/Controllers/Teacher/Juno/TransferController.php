<?php

namespace App\Http\Controllers\Teacher\Juno;

use App\Http\Controllers\MemberController;
use Auth;
use Illuminate\Http\Request;
use App\Core\Services\Juno\AccountBalanceService;

class TransferController extends MemberController
{
    private AccountBalanceService $service;

    /**
     * GatewayController constructor.
     *
     * @param \App\Core\Services\Juno\AccountBalanceService $service
     */
    public function __construct(AccountBalanceService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function index()
    {
        try {
            $teacher = Auth::user()->teacher()->first();
            $result = $this->service->fetch(null, $teacher);

            return view('teachers.transfer.index', compact('result', 'teacher'));
        } catch (\DomainException $domainException) {
            toast()->error($domainException->getMessage(), 'Atenção');

            return redirect()->route('teachers.dashboard.index');
        }
    }

    public function store(Request $request)
    {
        $teacher = Auth::user()->teacher()->first();
        $value = (float)$request->input('requested_amount');

        $this->service->transfer(null, $value, $teacher);

        toast()->success('Valor transferido com sucesso', 'Sucesso');

        return redirect()->route('teachers.financial.balance');
    }
}