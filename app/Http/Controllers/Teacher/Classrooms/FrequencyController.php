<?php

namespace App\Http\Controllers\Teacher\Classrooms;

use App\Core\Services\TrainingCenter\FrequencyService;
use App\Http\Controllers\Student\BaseController;
use App\Http\Requests\ReviewFrequency;
use App\Http\Requests\SaveFrequency;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Frequency;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class FrequencyController extends BaseController
{
    use SEOToolsTrait;

    protected $service;

    /**
     * FrequencyController constructor.
     *
     * @param $service
     */
    public function __construct(FrequencyService $service)
    {
        $this->service = $service;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Classroom $classroom
     *
     * @return View
     */
    public function index(Request $request, Classroom $classroom)
    {
        $teacher = Auth::user()->teacher;
        $date    = date('Y-m-d', time());

        if($request->filled('date')){
            $date = $request->get('date');
        }

        $frequencies = Frequency::select()
                                ->where('date', 'like',  "$date%")
                                ->where('classroom_id', $classroom->id)
                                ->get();

        $this->seo()->setTitle('Frequências | Aluno');

        return view('teachers.frequencies.index',
            compact('teacher', 'frequencies', 'classroom')
        );
    }

    public function create(Classroom $classroom)
    {
        $this->seo()->setTitle('Frequências');

        $students = $classroom->selectStudents()
                              ->orderBy('users.name')
                              ->pluck('users.name', 'registrations.id');

        return view('teachers.frequencies.create',
            compact('classroom', 'students')
        );
    }

    public function store(SaveFrequency $request)
    {
        $request->merge(['reviewed' => true]);
        $this->service->createMany($request);

        toast()->success('Frequência criada com sucesso.', 'Sucesso');

        return redirect()->route('teachers.dashboard.index');
    }

    public function update(ReviewFrequency $request)
    {
        $data = $request->validated();

        if (!empty($data['reviewed'])) {

            foreach ($data['reviewed'] as $id => $validated){
                $frequency           = Frequency::find($id);
                $frequency->reviewed = $validated;
                $frequency->save();
            }

            toast()->success('Frequências validadas com sucesso', 'Sucesso');
        } else {
            toast()->warning('Nenhum registro foi alterado', 'Alerta');
        }

        return redirect()->route('teachers.dashboard.index');
    }

}
