<?php

namespace App\Http\Controllers\Teacher\Financial;

use App\Core\Models\Financial\YieldTeacher;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\Student\BaseController;
use App\Core\Models\Message;
use App\Core\Models\TrainingCenter\Classroom;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use PhpParser\Node\Expr\Yield_;
use Symfony\Component\HttpFoundation\Request;

class FinancialController extends MemberController
{
    use SEOToolsTrait;

    private YieldTeacher $model;

    /**
     * @param YieldTeacher $model
     */
    public function __construct(YieldTeacher $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    /**
     * @return View
     */
    public function index(Request $request)
    {
        $yields     = $this->model->getYield(Auth::user()->teacher, $request);
        $classrooms = Auth::user()->teacher->classrooms()->get()->pluck('name', 'id');

        return view('teachers.financial.yield', compact('yields', 'classrooms'));
    }

}
