<?php

namespace App\Http\Controllers\Auth\Sessions;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\UserActivation;
use Illuminate\Http\RedirectResponse;
use App\Core\Models\Auth\Activation;
use App\Core\Models\Auth\User;

class ActivationController extends BaseController
{
    public function showActivationForm(Activation $activation)
    {
        if ($activation->expires_in->isPast()) {
            toast()->error(
                'Token expirado, entre em contato com o Administrador' .
                ' para envio de novo e-mail', 'Erro');

            return redirect()->route('auth.session.login');
        }

        return view('auth.session.activation', compact('activation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserActivation $request
     *
     * @return RedirectResponse
     */
    public function store(UserActivation $request)
    {
        $user = User::whereEmail($request->get('email'))->firstOrFail();

        $data = $request->validated();

        $user->password = $data['password'];
        $user->active = true;
        $user->save();

        toast()->success('Senha atualizada com sucesso!', 'Sucesso');

        return redirect()->route('auth.session.login');
    }
}
