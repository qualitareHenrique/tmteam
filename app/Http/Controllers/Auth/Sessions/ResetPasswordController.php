<?php

namespace App\Http\Controllers\Auth\Sessions;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends BaseController
{
    use ResetsPasswords;

    public function guard()
    {
        return Auth::guard('dashboard');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.session.resetPassword')->with([
            'token' => $token,
            'email' => $request->email
        ]);
    }

    public function reset(Request $request)
    {
        $validate = validator($request->all(), [
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required_with:password'
        ]);

        if ($validate->fails()) {
            toast()->error('Falha ao adicionar nova senha', 'Error');

            return redirect()->back()
                             ->withInput()
                             ->withErrors($validate->getMessageBag());
        }

        $response = $this->broker()->reset($this->credentials($request),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        $response == Password::PASSWORD_RESET ?
            $this->sendResetResponse($request, $response) :
            $this->sendResetFailedResponse($request, $response);

        toast()->success('Senha modificada com sucesso!', 'Successo');

        $role = Auth::user()->roles()->first()->name;

        return redirect()->route($this->dashByRole($role));
    }

    private function dashByRole($role)
    {
        switch ($role) {
            case 'aluno':
                return 'students.dashboard.index';
            case 'responsavel':
                return 'guardians.dashboard.index';
            case 'professor':
                return 'teachers.dashboard.index';
            default:
                return 'admin.dashboard.index';
        }
    }
}
