<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function __construct()
    {
        view()->composer('*', function ($view) {
            $avatar = Auth::user()->subsidiary->avatarUrl;
            $view->with('avatar', $avatar);
        });
    }
}
