<?php


namespace App\Http\Controllers\Api\Juno;

use App\Core\Services\Financial\BalanceRecordService;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessPaymentNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{

    private BalanceRecordService $balanceRecordService;

    /**
     * ChargeController constructor.
     *
     * @param \App\Core\Services\Financial\BalanceRecordService $balanceRecordService
     */
    public function __construct(BalanceRecordService $balanceRecordService)
    {
        $this->balanceRecordService = $balanceRecordService;
    }

    public function store(Request $request)
    {
        Log::channel('system')->info('Notificação de pagamento', $request->all());

        $validate = validator($request->all(), [
            'paymentToken'    => 'required',
            'chargeReference' => 'required',
            'chargeCode'      => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'message' => 'Invalid data',
                'errors'  => $validate->getMessageBag()
            ], 403);
        }

        $recordId = (int) $request->get('chargeReference');
        $record = $this->balanceRecordService->find($recordId);

        ProcessPaymentNotification::dispatch($record, $request->get('paymentToken'));

        return response()->json(['success' => 'ok'], 200);
    }
}