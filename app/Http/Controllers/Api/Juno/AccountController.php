<?php

namespace App\Http\Controllers\Api\Juno;

use App\Core\Services\SubsidiaryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{

    protected SubsidiaryService $service;

    /**
     * AccountController constructor.
     *
     * @param \App\Core\Services\SubsidiaryService $service
     */
    public function __construct(SubsidiaryService $service)
    {
        $this->service = $service;
    }

    public function update(Request $request)
    {
        Log::channel('system')->info('Notificação de mudança de status da conta', $request->all());

        $validate = validator($request->all(), [
            'eventId'                          => 'required',
            'eventType'                        => 'required',
            'timestamp'                        => 'required',
            'data.*.entityId'                  => 'required',
            'data.*.entityType'                => 'required',
            'data.*.attributes.previousStatus' => 'required',
            'data.*.attributes.status'         => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'message' => 'Invalid data',
                'errors'  => $validate->getMessageBag()
            ], 403);
        }

        $data     = current($request->get('data'));
        $entityId = $data['entityId'];
        $status   = $data['attributes']['status'];

        $subsidiary                 = $this->service->findBy('gateway_id', $entityId);
        $subsidiary->gateway_status = $status;
        $subsidiary->save();

        return response()->json(['success' => 'ok'], 200);
    }
}
