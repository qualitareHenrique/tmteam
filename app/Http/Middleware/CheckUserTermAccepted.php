<?php

namespace App\Http\Middleware;

use App\Core\Services\UserTermService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class CheckUserTermAccepted
{
    private UserTermService $service;

    /**
     * @param UserTermService $service
     */
    public function __construct(UserTermService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (
            !$this->service->hasTermAccepted(Auth::user()->id) 
            && !$request->get('initTerm')
            && "admin.users.accept-term" !== $request->route()->getName()
        ) {
            return redirect()->route('admin.users.term', ['initTerm' => true]);
        }

        return $next($request);
    }

    private function setCurrentAccess($routeName)
    {
        View::share('currentAccess', explode('.', $routeName)[0]);
    }
}
