<?php

namespace App\Http\Requests;

use App\Enums\RegistrationStatusEnum;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Registration;
use App\Core\Models\TrainingCenter\Frequency;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class SaveStudentFrequency extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'            => 'required|date_format:Y-m-d H:i:s',
            'classroom_id'    => 'required|numeric',
            'registration_id' => 'required|numeric'
        ];
    }

    public function withValidator(Validator $validator): void
    {
        if ($this->registration_id) {
            $validator->after(function (Validator $validator) {

                $registration = Registration::find($this->registration_id);

                if ($this->pedent($registration)) {
                    $message = 'Aluno com pendência, por favor dirija-se ao' .
                        ' setor administrativo para regularização.';

                    $validator->errors()
                              ->add('irregular', $message);
                }

                if ($this->belongsToClassroom($registration)) {
                    $message = 'Aluno não pertence a turma/modalidade.';
                    $validator->errors()
                              ->add('not_belongs', $message);
                }

                if ($this->registered()) {
                    $message = 'Presença já foi registrada.';
                    $validator->errors()
                              ->add('exists', $message);
                }
            });
        }

    }

    private function registered()
    {
        $today       = date('Y-m-d', time());
        $frequencies = Frequency::select()->where([
            'classroom_id'    => $this->classroom_id,
            'registration_id' => $this->registration_id
        ])->where('date', 'like', "$today%")->get();

        return $frequencies->count() > 0;
    }

    private function belongsToClassroom(Registration $registration)
    {
        $rClasses = $registration->classrooms;
        $belongs  = $rClasses->where('id', $this->classroom_id);

        if ($belongs) {
            return false;
        }

        $classroom    = Classroom::find($this->registration_id);
        $sameModality = $rClasses->first(function ($rClass) use ($classroom) {
            return $rClass->modality->id == $classroom->modality->id;
        });

        if ($sameModality) {
            return false;
        }
    }

    private function pedent($registration)
    {
        return $registration->status == RegistrationStatusEnum::IRREGULAR;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'registration_id.required' => 'Aluno experimental, por favor conclua a matrícula para registrar frequência',
        ];
    }
}
