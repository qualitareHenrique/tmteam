<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveSubsidiary extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->subsidiary ?? '';

        return [
            "name"                      => "required",
            "fantasy_name"              => "nullable",
            "cnpj"                      => "nullable|cnpj",
            "company_type"              => "nullable",
            "business_area"             => "required",
            "lines_of_business"         => "required",
            "manager_id"                => "required|int|unique:subsidiaries,manager_id,{$id}",
            "phone"                     => "required",
            "email"                     => "required|email",
            "cep"                       => "required",
            "street"                    => "required",
            "neighborhood"              => "required",
            "number"                    => "required",
            "city"                      => "required",
            "state"                     => "required",
            "complement"                => "nullable",
            "bank_number"               => "required",
            "agency_number"             => "required",
            "account_number"            => "required",
            "account_complement_number" => "nullable",
            "account_type"              => "required",
            "account_holder_name"       => "required",
            "account_holder_document"   => "required|cpf_cnpj",
            "resource_token"            => "nullable",
            "avatar"                    => "nullable|image|max:2048"
        ];
    }
}
