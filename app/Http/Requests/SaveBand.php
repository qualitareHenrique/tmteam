<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveBand extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                     => 'required|max:50',
            'color'                    => 'required',
            'modality_id'              => 'required',
            'next_id'                  => 'nullable',
            'age_categories'           => 'required|array',
            'degrees.*'                => 'required|array|min:1',
            'degrees.*.id'             => 'nullable',
            'degrees.*.name'           => 'required',
            'degrees.*.lessons'        => 'required|numeric',
            'degrees.*.minimum_period' => 'required|numeric',
            'degrees.*._destroy'       => 'nullable'
        ];
    }
}
