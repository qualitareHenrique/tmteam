<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewRegistrationSaveStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $studentId = $this->student ?? "";
        $userId    = $this->user_id ?? '';

        return [
            "id"              => "nullable",
            "cbjj"            => "nullable|max:50|unique:students,cbjj,{$studentId}",
            "origin_id"       => "required",
            "age_category_id" => "required",
            "guardian_id"     => "nullable",
            "experimental"    => "nullable",
            "user_id"         => "nullable",
            "registration_id" => "nullable",
            "user.name"       => "required|max:100",
            "user.nickname"   => "required|max:50",
            "user.email"      => "required|max:50|email|unique:users,email,{$userId}",
            "user.cpf"        => "nullable|cpf|unique:users,cpf,{$userId}",
            "user.phone"      => "required|max:20",
            "user.age"        => "required",
            "user.sex"        => "required",
            "user.birthdate"  => "required|date_format:Y-m-d",
            "user.avatar"     => "nullable|image|max:2048",
            "user.street"          => "required|max:255",
            "user.cep"             => "required|max:255",
            "user.neighborhood"    => "required|max:255",
            "user.number"          => "required|max:10",
            "user.city"            => "required|max:255",
            "user.state"           => "required|max:255",
            "user.subsidiary_id"   => "required",
        ];

        return $rules;
    }
}
