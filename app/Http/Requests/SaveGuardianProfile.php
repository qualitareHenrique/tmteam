<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveGuardianProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $personId = $this->guardian->person_id;

        return [
            "avatar"                     => "nullable|image|max:2048",
            "person_id"                  => "nullable",
            "person.name"                => "required|max:100",
            "person.nickname"            => "required|max:50",
            "person.email"               => "required|max:50|email|unique:people,email,{$personId}",
            "person.cpf"                 => "nullable|max:16|unique:people,cpf,{$personId}",
            "person.phone"               => "required|max:20",
            "person.age"                 => "required",
            "person.sex"                 => "required",
            "person.birthdate"           => "required|date_format:Y-m-d",
            "person.street"              => "required|max:100",
            "person.cep"                 => "nullable|max:9",
            "person.neighborhood"        => "nullable|max:50",
            "person.number"              => "nullable|max:4",
            "person.city"                => "nullable|max:50",
            "person.state"               => "nullable|max:2",
            'user.password'              => "nullable|min:8|confirmed",
            'user.password_confirmation' => "nullable|min:6",
        ];
    }
}
