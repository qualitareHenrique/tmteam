<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SavePermission extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $permissionId = $this->permission->id ?? '';

        return [
            'name'    => "required|max:60|unique:permissions,name,$permissionId",
            'details' => 'required|max:60',
            'roles'   => 'required|array|min:0'
        ];
    }
}
