<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveStudentProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $studentId = $this->student ?? '';
        $userId    = $this->user_id ?? '';

        return [
            "cbjj"                       => "nullable|max:50|unique:students,cbjj,{$studentId}",
            "avatar"                     => "nullable|image|max:2048",
            "user_id"                    => "required",
            "registration_id"            => "required",
            "user.active"                => "required",
            "user.name"                  => "required|max:100",
            "user.nickname"              => "required|max:50",
            "user.email"                 => "required|max:50|email|unique:users,email,{$userId}",
            "user.cpf"                   => "nullable|max:16|unique:users,cpf,{$userId}",
            "user.phone"                 => "required|max:20",
            "user.age"                   => "required",
            "user.sex"                   => "required",
            "user.birthdate"             => "required|date_format:Y-m-d",
            "user.street"                => "required|max:100",
            "user.cep"                   => "required|max:9",
            "user.neighborhood"          => "required|max:50",
            "user.number"                => "required|max:4",
            "user.city"                  => "required|max:50",
            "user.state"                 => "required|max:2",
            "user.complement"            => "nullable",
            "user.password"              => "nullable|min:8|confirmed",
            "user.password_confirmation" => "nullable|min:6",
            "user.avatar"                => "nullable|image|max:2048",
            "registration.fisic"         => "nullable|image|max:2048",
        ];
    }
}
