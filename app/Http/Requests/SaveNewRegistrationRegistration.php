<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SaveBalanceRecord
 * @package App\Http\Requests
 *
 * @property-read string $description
 * @property-read string $type
 * @property-read string $amount
 * @property-read string $amount_paid
 * @property-read string $discount_amount
 * @property-read string $discount_days
 * @property-read string $due_date
 * @property-read string $payment_date
 * @property-read string $payment_type
 * @property-read string $charge_fee
 * @property-read string $repeat
 * @property-read string $repeat_times
 */
class SaveNewRegistrationRegistration extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('registration')['id']) {
            return [];
        }

        return [
            'registration.advance_discount' => 'nullable|min:0',
            'registration.amount'           => 'required|min:1',
            'registration.effective_date'   => 'required',
            'registration.payment_type'     => 'required',
            'registration.payment_day'      => 'required',
            'registration.subsidiary_id'    => 'required',
            'registration.classrooms'       => 'required',
            'due_date'        => 'required|date_format:Y-m-d',
            'discount_amount' => 'nullable',
            'charge_fee'      => 'nullable',
            'type'            => 'required'
        ];
    }
}
