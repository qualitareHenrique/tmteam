<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveAgeCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->ageCategory->id ?? '';

        return [
            'name'       => "required|max:255|unique:ages_categories,name,$id",
            'age_begin'  => 'required',
            'age_finish' => 'required',
        ];
    }
}
