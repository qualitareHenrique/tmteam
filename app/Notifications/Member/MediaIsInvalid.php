<?php

namespace App\Notifications\Member;

use App\Core\Models\Media;
use Illuminate\Notifications\Notification;

class MediaIsInvalid extends Notification
{

    private $media;

    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $type = $this->media->human_readable_type;

        return [
            'subject' => "$type inválido",
            'message' => "Clique para reanexar uma nova foto",
            'link'    => route('students.accounts.edit'),
        ];
    }
}
