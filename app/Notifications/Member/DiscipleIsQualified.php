<?php

namespace App\Notifications\Member;

use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DiscipleIsQualified extends Notification
{
    use Queueable;

    private $student;
    private $classroom;

    /**
     * Create a new notification instance.
     *
     * @param Student $student
     * @param Classroom $classroom
     */
    public function __construct(Student $student, Classroom $classroom)
    {
        $this->student = $student;
        $this->classroom = $classroom;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $user = $this->student->user;

        return [
            'subject' => "Aluno qualificado",
            'message' => "O aluno $user->name, está pronto para ser graduado",
            'link'    => route('teachers.students.index', ['classroom' => $this->classroom->id])
        ];
    }
}
