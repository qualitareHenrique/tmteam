<?php

namespace App\Notifications\Admin;

use Carbon\Carbon;
use App\Core\Models\Auth\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Invoice extends Notification
{
    private Collection $recordes;

    /**
     * @return void
     */
    public function __construct(Collection $recordes)
    {
        $this->recordes = $recordes;
    }

    /**
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(config('app.name') . ' | Segunda Via Boletos')
            ->markdown(
                'admin.notifications.overdue.resend',
                [
                    'recordes' => $this->recordes,
                    'user'     => $notifiable,
                    'carbon'   => new Carbon()
                ]
            );
    }
}
