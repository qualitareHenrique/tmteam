<?php

namespace App\Notifications\Admin;

use App\Core\Models\TrainingCenter\Graduation;
use App\Core\Models\TrainingCenter\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class StudentCanBeGraduated extends Notification
{
    use Queueable;

    private $student;
    private $graduation;

    /**
     * Create a new notification instance.
     *
     * @param Student $student
     * @param Graduation $graduation
     */
    public function __construct(Student $student, Graduation $graduation)
    {
        $this->student = $student;
        $this->graduation = $graduation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $user = $this->student->user;
        $registration = $this->student->registration;

        return [
            'subject' => "Aluno qualificado",
            'message' => "O aluno $user->name, está pronto para ser graduado",
            'link'    => route('admin.students.graduations.create', [
                'registration' => $registration->id,
                'last' => $this->graduation->id
            ])
        ];
    }
}
