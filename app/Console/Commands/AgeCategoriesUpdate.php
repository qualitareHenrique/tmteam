<?php

namespace App\Console\Commands;

use App\Core\Models\Auth\User;
use App\Core\Models\TrainingCenter\AgeCategory;
use App\Core\Models\TrainingCenter\Student;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AgeCategoriesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'age-categories:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all student AgeCategories';

    protected $ageCategories;

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $startMsg  = 'Iniciando verificação diária' . PHP_EOL;
        $finishMsg = 'Verificação finalizada com sucesso' . PHP_EOL;

        $this->info($startMsg);
        Log::channel('system')->info($startMsg);

        $students            = Student::all();
        $this->ageCategories = AgeCategory::all();

        $bar = $this->output->createProgressBar(count($students));

        foreach ($students as $student) {
            $user = $student->user;

            if (empty($student)) {
                continue;
            }

            $ageCategory = $this->fetchCategory($this->calcAge($user));

            if ($ageCategory) {
                $student->age_category_id = $ageCategory->id;
                $student->save();
            }

            $bar->advance();
        }

        $bar->finish();

        $this->info(PHP_EOL . $finishMsg);
        Log::channel('system')->info($finishMsg);
    }

    private function calcAge(User $user): int
    {
        $birthday = $user->birthday;

        return $birthday > Carbon::now() ? $user->age + 1 : $user->age;
    }

    private function fetchCategory(int $age)
    {
        return $this->ageCategories->filter(
            function ($category) use ($age) {
                return $category->age_begin <= $age &&
                    $category->age_finish >= $age;
            })->first();
    }
}
