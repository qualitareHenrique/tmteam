<?php

namespace App\Console\Commands;

use App\Core\Models\Financial\BalanceRecord;
use App\Core\Services\Juno\ChargeService;
use App\Core\Services\Juno\Exceptions\ResponseErrorException;
use Illuminate\Console\Command;

class CancelCharges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'charges:cancel {competence}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel wrong generated monthly charges by competence';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param \App\Core\Services\Juno\ChargeService $chargeService
     *
     * @return mixed
     */
    public function handle(ChargeService $chargeService)
    {
        [$year, $month] = explode('-', $this->argument('competence'));
        $competence = $this->argument('competence');

        $startMsg = 'Iniciando cancelamento de cobranças' . PHP_EOL;
        $finishMsg = 'Cobranças canceladas com sucesso' . PHP_EOL;

        $this->info($startMsg);

        $description = "Mensalidade: $month/$year";
        $records = BalanceRecord::where('due_date', 'like', "$competence%")
                                ->where('description', $description)
                                ->whereNotNull('gateway_id')
                                ->whereNull('payment_date')
                                ->get();

        $bar = $this->output->createProgressBar(count($records));

        foreach ($records as $record){

            try {
                $chargeService->cancel($record);
            } catch (ResponseErrorException $ex) {
                \Log::info('Error on charge cancel', $ex->errorBag);
            } finally {

                $record->gateway_id = null;
                $record->split_amount = 0;
                $record->charge_link = null;
                $record->charge_code = null;
                $record->save();

                $bar->advance();
            }
        }

        $bar->finish();
        $this->info(PHP_EOL . $finishMsg);
    }
}
