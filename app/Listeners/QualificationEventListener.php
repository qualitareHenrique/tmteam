<?php

namespace App\Listeners;

use App\Events\StudentIsNowQualified;
use App\Core\Models\Auth\User;
use App\Core\Models\TrainingCenter\Graduation;
use App\Core\Models\TrainingCenter\Registration;
use App\Core\Models\TrainingCenter\Student;
use App\Notifications\Admin\StudentCanBeGraduated;
use App\Notifications\Member\DiscipleIsQualified;
use App\Notifications\Member\StudentQualified;
use Illuminate\Support\Facades\Auth;

class QualificationEventListener
{
    /**
     * Handle the event.
     *
     * @param StudentIsNowQualified $event
     *
     * @return void
     */
    public function handle(StudentIsNowQualified $event)
    {
        $graduation   = $event->graduation;
        $registration = $event->registration;
        $student      = $registration->student;

        $this->notifyStudent($student->user);
        $this->notifyTeachersAboutQualification($registration, $graduation);
        $this->notifyAdminsAboutQualification($student, $graduation);
    }

    private function notifyStudent(User $user)
    {
        $user->notify(new StudentQualified());
    }

    private function notifyAdminsAboutQualification(
        Student $student,
        Graduation $graduation
    ) {
        $createdBy = $student->user;
        $admins    = User::byRole(['root', 'gestor'])
                         ->where('subsidiary_id', $createdBy->subsidiary_id)
                         ->where('receive_messages', 1)
                         ->get();

        foreach ($admins as $admin) {
            $admin->notify(new StudentCanBeGraduated($student, $graduation));
        }
    }

    private function notifyTeachersAboutQualification(
        Registration $registration,
        Graduation $graduation
    ) {
        $classroom = $registration->classrooms()
                                  ->where('modality_id', $graduation->modality_id)
                                  ->first();

        foreach ($classroom->teachers as $teacher) {
            $teacher->user->notify(new DiscipleIsQualified($registration->student, $classroom));
        }
    }
}

