<?php


namespace App\Enums;


class PeriodTypesEnum
{
    const DAILY   = 'P1D';
    const WEEKLY  = 'P1W';
    const MONTHLY = 'P1M';
    const YEARLY  = 'P1Y';

    public static function options() : array
    {
        return [
            self::DAILY   => 'Diáriamente',
            self::WEEKLY  => 'Semanalmente',
            self::MONTHLY => 'Mensalmente',
            self::YEARLY  => 'Anualmente'
        ];
    }
}