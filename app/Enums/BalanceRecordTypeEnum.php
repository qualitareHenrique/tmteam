<?php


namespace App\Enums;

class BalanceRecordTypeEnum
{
    const RECIPE  = 1;
    const EXPENSE = 2;

    public static function options()
    {
        return [
            self::RECIPE  => 'Receita',
            self::EXPENSE => 'Despesa',
        ];
    }
}