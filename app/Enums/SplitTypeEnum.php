<?php


namespace App\Enums;


class SplitTypeEnum
{

    const AMOUNT     = 1;
    const PERCENTAGE = 2;

    public static function options()
    {
        return [
            self::AMOUNT     => 'R$',
            self::PERCENTAGE => '%',
        ];
    }

}