<?php

namespace App\Enums;

class ResendTypesEnum
{
    /** @var int  */
    public const
        EMAIL         = 1,
        WHATSAPP      = 2
    ;
}