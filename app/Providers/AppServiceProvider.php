<?php

namespace App\Providers;

use App\Core\Models\Setting;
use App\Core\Models\TrainingCenter\Frequency;
use App\Core\Observers\FrequencyObserver;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'pt_BR.utf-8');

        Schema::defaultStringLength(191);

        $this->bootApplicationSettings();
        $this->bootObservers();
        $this->bootHttpScheme();
    }

    private function bootApplicationSettings()
    {
        if (Schema::hasTable('settings')) {
            foreach (Setting::all() as $setting) {
                Config::set("settings.$setting->key", $setting->value);
            }
        }
    }

    private function bootHttpScheme()
    {
        if ($this->app->environment('production')) {
            URL::forceScheme('https');
        }
    }

    private function bootObservers()
    {
        Frequency::observe(FrequencyObserver::class);
    }
}
