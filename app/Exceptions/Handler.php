<?php

namespace App\Exceptions;

use ErrorException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Spatie\Permission\Exceptions\UnauthorizedException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        if (
            app()->environment() == 'production' &&
            app()->bound('sentry') &&
            $this->shouldReport($exception)
        ){
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if (app()->environment() == 'production') {
            if (
                $exception instanceof ModelNotFoundException ||
                $exception instanceof NotFoundHttpException
            ) {
                if ($request->ajax() || $request->wantsJson()) {
                    return response()->json(['error' => 'Página não encontrada.'], 404);
                }

                return response()->view('admin.errors.404', [], 404);
            }

            if ($exception instanceof UnauthorizedException) {
                if ($request->ajax() || $request->wantsJson()) {
                    return response()->json(['error' => 'Acesso negado.'], 404);
                }

                return response()->view('admin.errors.403', [], 403);
            }

            if ($exception instanceof ErrorException) {
                return response()->view('admin.errors.500', [], 500);
            }
        }

        if ($exception instanceof QueryException && $exception->getCode() == 23000) {
            $message = 'Não é possível excluir um resgistro relacionado a outros';

            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(['error' => $message], 403);
            }
        }

        return parent::render($request, $exception);
    }
}
