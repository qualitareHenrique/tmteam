<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Person;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use App\Core\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\TrainingCenter\Student
 *
 * @property int $id
 * @property bool $experimental
 * @property string|null $cbjj
 * @property int $origin_id
 * @property int $user_id
 * @property int|null $age_category_id
 * @property int|null $guardian_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Core\Models\TrainingCenter\AgeCategory|null $ageCategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Classroom[] $classrooms
 * @property-read int|null $classrooms_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Core\Models\TrainingCenter\Origin $origin
 * @property-read \App\Core\Models\TrainingCenter\Registration|null $registration
 * @property-read \App\Core\Models\TrainingCenter\Guardian|null $guardian
 * @property-read \App\Core\Models\Auth\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereAgeCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereCbjj($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereExperimental($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereOriginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student wherePersonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereResponsibleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Student whereGuardianId($value)
 * @mixin \Eloquent
 */
class Student extends Model implements AuditableInterface
{
    use Notifiable, AuditableTrait, Filterable;

    protected $fillable = [
        'code',
        'cbjj',
        'experimental',
        'age_category_id',
        'guardian_id',
        'origin_id',
        'user_id',
    ];

    protected $casts = [
        'experimental' => 'boolean'
    ];

    protected $auditInclude = [
        'code',
        'cbjj',
        'experimental',
        'age_category_id',
        'guardian_id',
        'origin_id',
        'user_id'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->configs();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
                    ->withoutGlobalScopes();
    }

    public function origin()
    {
        return $this->belongsTo(Origin::class, 'origin_id');
    }

    public function classrooms()
    {
        return $this->belongsToMany(
            Classroom::class,
            'classrooms_students',
            'student_id',
            'classroom_id'
        )->withoutGlobalScopes();
    }

    public function guardian()
    {
        return $this->belongsTo(Guardian::class, 'guardian_id');
    }

    public function registration()
    {
        return $this->hasOne(Registration::class, 'student_id', 'id')
                    ->withoutGlobalScopes();
    }

    public function ageCategory()
    {
        return $this->belongsTo(AgeCategory::class, 'age_category_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Instance methods
    |--------------------------------------------------------------------------
    */
    public function configs()
    {
        $cpf = $this->user['cpf'];
        $name = $this->user['name'];

        $this->attributes['code'] = sha1(date('dmyhis') . $cpf . $name);
    }
}
