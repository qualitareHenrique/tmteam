<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Auth\User;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Person;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Teacher
 *
 * @package App\Core\Models\TrainingCenter
 * @property int $id
 * @property int $user_id
 * @property User $user
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Classroom[] $classrooms
 * @property-read int|null $classrooms_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Modality[] $modalities
 * @property-read int|null $modalities_count
 * 
 * @property string|null $bank_number
 * @property string|null $agency_number
 * @property string|null $account_number
 * @property string|null $account_complement_number
 * @property string|null $account_type
 * @property string|null $account_holder_name
 * @property string|null $account_holder_document
 * 
 * @property string|null $gateway_id
 * @property string|null $gateway_status
 * @property string|null $resource_token
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher wherePersonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Teacher whereUserId($value)
 * @mixin \Eloquent
 */
class Teacher extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    protected $fillable = [
        'user_id',
        'bank_number',
        'agency_number',
        'account_number',
        'account_complement_number',
        'account_type',
        'account_holder_name',
        'account_holder_document',
        'resource_token'
    ];

    protected $auditInclude = [
        'user_id',
        'classrooms'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function classrooms()
    {
        return $this->belongsToMany(Classroom::class,
            'classrooms_teachers',
            'teacher_id',
            'classroom_id'
        );
    }

    public function modalities()
    {
        return $this->belongsToMany(Modality::class,
            'teachers_modalities',
            'teacher_id',
            'modality_id'
        );
    }
}
