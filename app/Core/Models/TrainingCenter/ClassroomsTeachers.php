<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Auth\Activation;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property-read  int $id
 * @property-read  string $type
 * @property-read  string $description
 * @property  double|null $split_value
 * @property int $teacher_id
 * 
 * @property \App\Core\Models\TrainingCenter\Classroom $classroom
 * @property \App\Core\Models\TrainingCenter\Teacher $teacher
 * 
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * 
 * @method static Builder|Activation whereId($value)
 * 
 * @mixin Eloquent
 */
class ClassroomsTeachers extends Model
{
    /** @var string */
    protected $table = 'classrooms_teachers';

    /** @var string[] */
    protected $fillable = [
        'teacher_id',
        'classroom_id',
        'split_value'
    ];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    public function classroom()
    {
        return $this->belongsTo(Classroom::class, 'classroom_id');
    }

    /**
     * @param int|null $classroom
     * @param int|null $teacher
     * @return ClassroomsTeachers|null
     */
    public static function findByClassroomAndTeacher(?int $classroom, ?int $teacher): ?ClassroomsTeachers
    {
        return ClassroomsTeachers::query()
            ->where('classroom_id', '=', $classroom)
            ->where('teacher_id', '=', $teacher)
            ->first()
        ;
    }
}
