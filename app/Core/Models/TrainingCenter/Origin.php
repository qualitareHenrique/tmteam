<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Concerns\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * Class Origin
 *
 * @package App\Core\Models\TrainingCenter
 * @property string $name
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin asc()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Origin whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Origin extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    protected $table = 'origins';

    protected $fillable = [
        'name',
    ];

    protected $auditInclude = [
        'name',
    ];

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */
    public function scopeAsc($query)
    {
        return $query->orderBy('name', 'ASC');
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    public function setNameAttribute($value)
    {
        if ($value){
            $this->attributes['name'] = Str::title($value);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */
    public static function options()
    {
        return self::select()->asc()->pluck('name', 'id');
    }
}
