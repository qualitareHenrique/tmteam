<?php

namespace App\Core\Models\TrainingCenter;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use App\Core\Models\Subsidiary;
use App\Enums\RegistrationStatusEnum;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Sentry\State\Scope;

/**
 * Class Classroom
 *
 * @package App\Core\Models\TrainingCenter
 * @property string $slug
 * @property string $name
 * @property int $modality_id
 * @property Modality $modality
 * @property boolean $active
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Frequency[] $frequencies
 * @property-read int|null $frequencies_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Registration[] $registrations
 * @property-read int|null $registrations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\Teacher[] $teachers
 * @property-read int|null $teachers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\TrainingCenter\ClassroomsTeachers[] $classroomsTeachers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereModalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $subsidiary_id
 * @property-read \App\Core\Models\Subsidiary $subsidiary
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\Classroom whereSubsidiaryId($value)
 */
class Classroom extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    protected $fillable = [
        'name',
        'modality_id',
        'subsidiary_id',
        'active'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    protected $auditInclude = [
        'name',
        'modality_id',
        'subsidiary_id',
        'active'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function teachers()
    {
        return $this->belongsToMany(Teacher::class,
            'classrooms_teachers',
            'classroom_id',
            'teacher_id'
        );
    }

    /**
     * @return HasMany|null
     */
    public function classroomsTeachers(): ?HasMany
    {
        return $this->hasMany(ClassroomsTeachers::class, 'classroom_id');
    }

    public function modality()
    {
        return $this->belongsTo(Modality::class, 'modality_id');
    }

    public function registrations()
    {
        return $this->belongsToMany(Registration::class,
            'registrations_classrooms',
            'classroom_id',
            'registration_id'
        );
    }

    public function frequencies()
    {
        return $this->hasMany(Frequency::class, 'classroom_id');
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'subsidiary_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    public function setNameAttribute($value)
    {
        if (empty($value)) return;

        $this->attributes[ 'name' ] = Str::title($value);
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */

    public function selectStudents($withCancelled = false)
    {
        $query = $this->registrations()
                    ->withoutGlobalScopes()
                    ->join('students', 'registrations.student_id', '=', 'students.id')
                    ->join('users', 'students.user_id', '=', 'users.id');

        if(!$withCancelled){

            $query->where('registrations.status', '<>',
                RegistrationStatusEnum::CANCELLED
            );
        }

        return $query;
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
