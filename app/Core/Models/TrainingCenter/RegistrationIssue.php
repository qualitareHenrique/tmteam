<?php

namespace App\Core\Models\TrainingCenter;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\TrainingCenter\RegistrationIssue
 *
 * @property int $id
 * @property string $type
 * @property string $description
 * @property int $registration_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Core\Models\TrainingCenter\Registration $registration
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\TrainingCenter\RegistrationIssue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RegistrationIssue extends Model
{

    protected $fillable = [
        'type',
        'description'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function registration()
    {
        return $this->belongsTo(Registration::class, 'registration_id');
    }
}
