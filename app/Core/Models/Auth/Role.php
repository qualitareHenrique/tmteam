<?php

namespace App\Core\Models\Auth;

use Spatie\Permission\Models\Role as SpatieRole;

/**
 * Class Role
 *
 * @package App\Core\Models\Auth
 * @property string $name
 * @property string $details
 * @property string $guard_name
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Auth\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role byUser($currentUser)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role findByName($name)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends SpatieRole
{
    protected $fillable = [
        'name',
        'details',
        'guard_name'
    ];

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeByUser($query, $currentUser)
    {
        $query->orderBy('id', 'ASC');

        if (!$currentUser->hasRole('root')) {
            if ($currentUser->hasRole('gestor')) {
                $query->where('name', '!=', 'root');
            } else {
                $query->whereNotIn('name', ['root', 'unidade', 'aluno', 'professor']);
            }
        }


        return $query;
    }

    public function scopeFindByName($query, $name)
    {
        return $query->where('name', $name);
    }
}
