<?php

namespace App\Core\Models\Auth;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Student;
use App\Core\Models\TrainingCenter\Guardian;
use App\Core\Models\TrainingCenter\Teacher;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticator;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Users\WelcomeUser;
use App\Notifications\Users\ResetPasswordNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Str;

use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * App\Core\Models\Auth\User
 *
 * @property int $id
 * @property string|null $name
 * @property string $nickname
 * @property string|null $cpf
 * @property string $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $birthdate
 * @property int|null $sex
 * @property string|null $cep
 * @property string|null $street
 * @property string|null $number
 * @property string|null $neighborhood
 * @property string|null $complement
 * @property string|null $city
 * @property string|null $state
 * @property string $password
 * @property bool|null $active
 * @property bool|null $receive_messages
 * @property \Illuminate\Support\Carbon|null $latest_login
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $subsidiary_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $age
 * @property-read mixed $avatar
 * @property-read mixed $avatar_url
 * @property-read mixed $avatar_validation_status
 * @property-read mixed $birthdate_br
 * @property-read mixed $birthday
 * @property-read mixed $day_of_birth
 * @property-read mixed $last_login
 * @property-read mixed $sex_name
 * @property-read \App\Core\Models\TrainingCenter\Guardian|null $guardian
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \App\Core\Models\TrainingCenter\Student|null $student
 * @property-read \App\Core\Models\Subsidiary $subsidiary
 * @property-read \App\Core\Models\TrainingCenter\Teacher|null $teacher
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User byRole($roles)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User bySubsidiary()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereCep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereComplement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereCpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereLatestLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereNeighborhood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereReceiveMessages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereSubsidiaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticator implements HasMedia, AuditableInterface
{
    use Filterable, Notifiable, HasMediaTrait, HasRoles, AuditableTrait;

    const DEFAULT_PASSWORD = "bemvindotmteam";

    protected $fillable = [
        'name',
        'cpf',
        'nickname',
        'email',
        'phone',
        'sex',
        'birthdate',
        'cep',
        'street',
        'neighborhood',
        'state',
        'complement',
        'city',
        'number',
        'reason',
        'remark',
        'password',
        'receive_messages',
        'active',
        'latest_login',
        'subsidiary_id',
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $casts = [
        'receive_messages' => 'boolean',
        'active'           => 'boolean',
        'birthdate'        => 'date',
        'created_at'       => 'date',
        'updated_at'       => 'date',
        'latest_login'     => 'date'
    ];

    protected $auditInclude = [
        'name',
        'cpf',
        'nickname',
        'email',
        'phone',
        'sex',
        'birthdate',
        'cep',
        'street',
        'neighborhood',
        'state',
        'complement',
        'city',
        'number',
        'reason',
        'remark',
        'receive_messages',
        'active'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $model->sendWelcomeNotification();
        });

        static::creating(function ($model) {
            if (empty($model->password)) {
                $model->password = self::DEFAULT_PASSWORD;
            }
        });
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('avatar')
             ->fit(Manipulations::FIT_CROP, 320, 320)
             ->quality(90)
             ->nonQueued();
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('avatar')
             ->useFallbackUrl('/img/avatar-default.png')
             ->singleFile();
    }

    /*
    |--------------------------------------------------------------------------
    | Notifications
    |--------------------------------------------------------------------------
    */

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this));
    }

    public function sendWelcomeNotification()
    {
        $this->notify(new WelcomeUser(
            Activation::generateToken($this)->token, $this
        ));
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeByRole($query, $roles)
    {
        return $query->whereHas('roles',
            function ($q) use ($roles) {
                $q->whereIn('name', $roles);
            });
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeBySubsidiary($query)
    {
        if(Auth::user()->isAdmin()){
            return $query;
        }

        return $query->where('subsidiary_id', Auth::user()->subsidiary_id);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function teacher()
    {
        return $this->hasOne(Teacher::class, 'user_id');
    }

    public function student()
    {
        return $this->hasOne(Student::class, 'user_id');
    }

    public function guardian()
    {
        return $this->hasOne(Guardian::class, 'user_id');
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'subsidiary_id')
                    ->withoutGlobalScopes();
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    public function setEmailAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['email'] = Str::lower($value);
    }


    public function setPasswordAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        return $this->attributes['password'] = Hash::needsRehash($value) ?
            Hash::make($value) : $value;
    }

    public function setNameAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['name'] = \Illuminate\Support\Str::title($value);
    }

    public function setPhoneAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setCpfAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['cpf'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setSubsidiaryIdAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        $this->attributes['subsidiary_id'] = $value;
    }

    public function setActiveAttribute($value)
    {
        if ($value === null) {
            return;
        }

        $this->attributes['active'] = $value;
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getLastLoginAttribute()
    {
        return ($this->latest_login) ?
            $this->latest_login->diffForHumans() :
            'Não efetuou login';
    }

    public function getAvatarAttribute()
    {
        return $this->getFirstMedia('avatar');
    }

    public function getAvatarUrlAttribute()
    {
        return $this->getFirstMediaUrl('avatar', 'avatar');
    }

    public function getAvatarValidationStatusAttribute()
    {
        $media = $this->getFirstMedia('avatar');

        return $media ? $media->getCustomProperty('validation') : null;
    }

    public function getSexNameAttribute()
    {
        switch ($this->sex) {
            case 1:
                return 'Masculino';
            case 2:
                return 'Feminino';
            default:
                return '';
        }
    }

    public function getAgeAttribute()
    {
        return $this->birthdate->age ?? null;
    }

    public function getDayOfBirthAttribute()
    {
        if (empty($this->birthdate)) {
            return;
        }

        return $this->birthdate->format('d');
    }

    public function getBirthdayAttribute()
    {
        if (empty($this->birthdate)) {
            return;
        }

        return $this->birthdate->year(Carbon::now()->year);
    }

    public function getBirthdateBrAttribute()
    {
        if (empty($this->birthdate)) {
            return;
        }

        return Carbon::createFromFormat('Y-m-d H:i:s', $this->birthdate)
                     ->format('d/m/Y');
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */

    public function isAdmin()
    {
        return $this->hasRole('root');
    }

    public function isGuardian()
    {
        return $this->hasRole('responsavel');
    }

    public function isStudent()
    {
        return $this->hasRole('aluno');
    }

    public function isTeacher()
    {
        return $this->hasRole('professor');
    }
}
