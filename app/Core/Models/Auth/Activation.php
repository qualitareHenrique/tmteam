<?php

namespace App\Core\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class Activation
 *
 * @package App\Core\Models\Auth
 * @property string $token
 * @property string $email
 * @property \DateTime $expires_in
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation whereExpiresIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Auth\Activation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Activation extends Model
{

    protected $fillable = [
        'token',
        'email',
        'expires_in'
    ];

    protected $dates = [
        'expires_in'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'token';
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->configs();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Class methods
    |--------------------------------------------------------------------------
    */

    /**
     * Generates token for user activation
     *
     * @param User
     * @return Activation
     */
    public static function generateToken(User $user)
    {
        return Activation::create([
            'email'      => $user->email,
            'expires_in' => Carbon::now()->addDays(1)
        ]);
    }

    /**
     * Configs
     */
    protected function configs()
    {
        $this->attributes['token'] = sha1(date('dmyhis') . $this->email);
    }
}
