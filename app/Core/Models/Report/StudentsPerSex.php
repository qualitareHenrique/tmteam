<?php


namespace App\Core\Models\Report;


use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\StudentsPerSex
 *
 * @property string|null $date
 * @property int $registration_id
 * @property int $classroom_id
 * @property int|null $sex
 * @property int $subsidiary_id
 * @method static \App\Core\Models\Report\StudentsPerSexCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex filter(\App\Core\Filters\Filter $filter)
 * @method static \App\Core\Models\Report\StudentsPerSexCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerSex whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class StudentsPerSex extends Model
{

    use Filterable;

    protected $table = 'students_per_sex_report';

    public function newCollection(array $models = [])
    {
        return new StudentsPerSexCollection($models);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SubsidiaryScope);
    }

}