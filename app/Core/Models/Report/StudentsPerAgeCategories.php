<?php


namespace App\Core\Models\Report;


use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\StudentsPerAgeCategories
 *
 * @property string|null $date
 * @property int $registration_id
 * @property int $classroom_id
 * @property int|null $age_category_id
 * @property string $age_category
 * @property int $subsidiary_id
 * @method static \App\Core\Models\Report\StudentsPerAgeCategoriesCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories filter(\App\Core\Filters\Filter $filter)
 * @method static \App\Core\Models\Report\StudentsPerAgeCategoriesCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories whereAgeCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories whereAgeCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerAgeCategories whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class StudentsPerAgeCategories extends Model
{

    use Filterable;

    protected $table = 'students_per_age_categories_report';

    public function newCollection(array $models = [])
    {
        return new StudentsPerAgeCategoriesCollection($models);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('registration_id', function (Builder $builder) {
            $builder->groupBy(['registration_id']);
        });

        static::addGlobalScope(new SubsidiaryScope);
    }

}