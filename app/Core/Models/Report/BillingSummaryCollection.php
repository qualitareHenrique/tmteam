<?php


namespace App\Core\Models\Report;

use Illuminate\Database\Eloquent\Collection;

class BillingSummaryCollection extends Collection
{
    public function resume()
    {
        $data = [
            'expected_incoming' => 0,
            'real_incoming'     => 0,
            'paid_charges'      => 0,
            'total_charges'     => 0,
            'conversion'        => 0,
            'medium_ticket'     => 0
        ];

        $this->each(function($record) use (&$data){
            $data['expected_incoming'] += $record->expected_incoming;
            $data['real_incoming'] += $record->real_incoming;
            $data['paid_charges'] += $record->paid_charges;
            $data['total_charges'] += $record->total_charges;
        });

        $data['conversion'] = $this->conversion($data['real_incoming'], $data['expected_incoming']);
        $data['medium_ticket'] = $this->mediumTicket($data['real_incoming'], $data['paid_charges']);

        return $data;
    }

    private function conversion($realIncoming, $expectedIncoming)
    {
        if(empty($realIncoming) || empty($expectedIncoming)){
            return 0;
        }

        return $realIncoming / $expectedIncoming * 100;
    }

    private function mediumTicket($realIncoming, $paidCharges)
    {
        if(empty($realIncoming) || empty($paidCharges)){
            return 0;
        }

        return $realIncoming / $paidCharges;
    }
}