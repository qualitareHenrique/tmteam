<?php

namespace App\Core\Models\Report;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\BillingSummary
 *
 * @property \Illuminate\Support\Carbon|null $date
 * @property string $classroom
 * @property int $classroom_id
 * @property int $subsidiary_id
 * @property float|null $expected_incoming
 * @property float|null $real_incoming
 * @property float|null $paid_charges
 * @property float|null $total_charges
 * @method static \App\Core\Models\Report\BillingSummaryCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary filter(\App\Core\Filters\Filter $filter)
 * @method static \App\Core\Models\Report\BillingSummaryCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary whereClassroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary whereExpectedIncoming($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary wherePaidCharges($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary whereRealIncoming($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary whereSubsidiaryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\BillingSummary whereTotalCharges($value)
 * @mixin \Eloquent
 */
class BillingSummary extends Model
{
    use Filterable;

    protected $table = 'billing_summary_report';

    protected $casts = [
        'date' => 'date'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

    /**
     * @inheritdoc
     */
    public function newCollection(array $models = [])
    {
        return new BillingSummaryCollection($models);
    }
}
