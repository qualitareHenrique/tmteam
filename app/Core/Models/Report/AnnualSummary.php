<?php

namespace App\Core\Models\Report;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\Overdue
 *
 * @property string $referring
 * @property string $newRegistrations
 * @property float|null $revenues
 * @property float|null $expeditures
 * 
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\Overdue filter(\App\Core\Filters\Filter $filter)
 * 
 * @mixin \Eloquent
 */
class AnnualSummary extends Model
{
    use Filterable;

    protected $table = 'annual_financial_summary';
}
