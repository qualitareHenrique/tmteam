<?php


namespace App\Core\Models\Report;


use Illuminate\Database\Eloquent\Collection;

class StudentsPerAgeCategoriesCollection extends Collection
{

    public function resume() : \Illuminate\Support\Collection
    {
        return $this->groupBy('age_category')->map(function ($item, &$key) {
            return $key = $item->count();
        });
    }

}