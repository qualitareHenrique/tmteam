<?php


namespace App\Core\Models\Report;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\Concerns\SubsidiaryScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\StudentsPerStatus
 *
 * @property string|null $date
 * @property int $registration_id
 * @property int $classroom_id
 * @property int $status
 * @property int $subsidiary_id
 * @method static \App\Core\Models\Report\StudentsPerStatusCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus filter(\App\Core\Filters\Filter $filter)
 * @method static \App\Core\Models\Report\StudentsPerStatusCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\StudentsPerStatus whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class StudentsPerStatus extends Model
{

    use Filterable;

    protected $table = 'students_per_status_report';

    public function newCollection(array $models = [])
    {
        return new StudentsPerStatusCollection($models);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('registration_id', function (Builder $builder) {
            $builder->groupBy(['registration_id']);
        });

        static::addGlobalScope(new SubsidiaryScope);
    }

}