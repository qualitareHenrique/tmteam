<?php

namespace App\Core\Models\Report;

use App\Core\Models\Concerns\SubsidiaryScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\Models\Report\ActiveStudents
 *
 * @property int $students
 * @property int|null $competence_month
 * @property int|null $competence_year
 * @property int $subsidiary_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents currentMonth()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents whereCompetenceMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents whereCompetenceYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents whereStudents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Report\ActiveStudents whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class ActiveStudents extends Model
{
    protected $table = 'active_students_report';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeCurrentMonth($query)
    {
        $now = Carbon::now();

        return $query->where("competence_month", $now->month)
                     ->where("competence_year", $now->year);
    }
}