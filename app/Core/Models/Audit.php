<?php

namespace App\Core\Models;

use App\Core\Models\Concerns\Filterable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Audit as AuditContract;
use OwenIt\Auditing\Audit as AuditTrait;
use Carbon\Carbon;

/**
 * Class Audit
 *
 * @package App\Core\Models
 * @property string $old_values
 * @property string $new_values
 * @property int $id
 * @property string|null $user_type
 * @property int|null $user_id
 * @property string $event
 * @property string $auditable_type
 * @property int $auditable_id
 * @property string|null $url
 * @property string|null $ip_address
 * @property string|null $user_agent
 * @property string|null $tags
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $auditable
 * @property-read mixed $action_name
 * @property-read mixed $type_name
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit filter(\App\Core\Filters\Filter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit today()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereAuditableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereAuditableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereNewValues($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereOldValues($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Audit whereUserType($value)
 * @mixin \Eloquent
 */
class Audit extends Model implements AuditContract
{
    use AuditTrait, Filterable;

    protected $guarded = [];

    protected $casts = [
        'old_values' => 'json',
        'new_values' => 'json'
    ];

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */

    public function scopeToday($query)
    {
        $query->whereDate('created_at', Carbon::today());
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getTypeNameAttribute()
    {
        $auditableType = class_basename($this->auditable_type);

        $types = config('qualitare.audit.types');

        return isset($types[$auditableType]) ? $types[$auditableType] : 'Indefinido';
    }

    public function getActionNameAttribute()
    {
        $actions = config('qualitare.audit.actions');

        return $actions[$this->event];
    }
}
