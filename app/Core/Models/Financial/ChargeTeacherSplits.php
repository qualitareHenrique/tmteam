<?php

namespace App\Core\Models\Financial;

use App\Core\Models\Concerns\Filterable;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\Teacher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * @property int $id
 * @property int $balance_record_id
 * @property int $classroom_id
 * @property int $teacher_id
 * @property float $split_value
 * 
 * @property \App\Core\Models\TrainingCenter\Classroom $classroom
 * @property \App\Core\Models\TrainingCenter\Teacher $teacher
 * 
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $chargeable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * 
 * @mixin \Eloquent
 */
class ChargeTeacherSplits extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    /** @var string */
    protected $table = 'charge_teacher_splits';

    /** @var string[] */
    protected $fillable = [
        'balance_record_id',
        'classroom_id',
        'teacher_id',
        'split_value'
    ];

    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    public function classroom(): BelongsTo
    {
        return $this->belongsTo(Classroom::class, 'classroom_id');
    }
}
