<?php

namespace App\Core\Models\Financial;

use App\Core\Models\TrainingCenter\Teacher;
use Illuminate\Support\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class YieldTeacher extends Model
{
    /**
     * @param Teacher $teacher
     * 
     * @return Collection
     */
    public function getYield(Teacher $teacher, Request $request): Collection
    {
        if (empty($request->all())) {
            return new Collection();
        }

        $paymentDates = explode(' - ', $request->get('date_range_payment'));
        $dueDates     = explode(' - ', $request->get('date_range_due_date'));

        $query = DB::table('balance_records as b')
            ->select([
                'u.name as teacher',
                's_u.name as student',
                'b.due_date',
                'b.payment_date',
                'b.amount',
                'b.amount_paid',
                'cts.id as charge_split',
                'cts.split_value as charge_split_value',
                'ct.split_value as classroom_teacher_split_value'
            ])
            ->leftJoin('charge_teacher_splits as cts', function ($join) use ($teacher) {
                $join->on('b.id', '=', 'cts.balance_record_id')
                    ->where('cts.teacher_id', $teacher->id);
            })
            ->join('registrations as r', 'r.id', '=', 'b.chargeable_id')
            ->join('registrations_classrooms as rc', 'rc.registration_id', '=', 'r.id')
            ->join('classrooms as c', 'c.id', '=', 'rc.classroom_id')
            ->join('classrooms_teachers as ct', function ($join) use ($teacher) {
                $join->on('ct.classroom_id', '=', 'c.id')
                    ->where('ct.teacher_id', $teacher->id);
            })
            ->join('teachers as t', 't.id' , '=', 'ct.teacher_id')
            ->join('users as u', 'u.id' , '=', 't.user_id')
            ->join('students as s', 's.id', '=', 'r.student_id')
            ->join('users as s_u', 's_u.id', '=', 's.user_id')
            ->where('t.id', $teacher->id)
            ->orderByRaw('s_u.name, b.due_date')
            ->groupBy(['b.id']);

        if (!empty($paymentDates[0])) {
            $query->where('b.payment_date', '>=', $paymentDates[0]);
        }

        if (!empty($paymentDates[1])) {
            $query->where('b.payment_date', '<=', $paymentDates[1]);
        }

        if (!empty($dueDates[0])) {
            $query->where('b.due_date', '>=', $dueDates[0]);
        }

        if (!empty($dueDates[1])) {
            $query->where('b.due_date', '<=', $dueDates[1]);
        }

        if ($request->get('classroom_id')) {
            $query->whereIn('ct.classroom_id', $request->get('classroom_id'));
        }

        return $query->get();
    }
}
