<?php

namespace App\Core\Models;

use DateTime;
use App\Core\Models\Auth\User;
use App\Core\Models\Concerns\Filterable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * @property int $id
 * @property int $user_id
 * @property DateTime $accepted_date
 * @property User $user
 * 
 * @mixin \Eloquent
 */
class UserTerm extends Model implements AuditableInterface
{
    use AuditableTrait, Filterable;

    /** @var string */
    protected $table = 'user_accept_term';

    /** @var string[] */
    protected $fillable = [
        'user_id',
        'accepted_date'
    ];

    /**
     * @param int $user
     */
    public static function whereUserId(int $user): ?UserTerm
    {
        return UserTerm::query()
            ->where('user_id', '=', $user)
            ->first()
        ;
    }

    /**
     * @param int $user
     */
    public static function whereAcceptedByUser(int $user): ?UserTerm
    {
        return UserTerm::query()
            ->where('user_id', '=', $user)
            ->whereNotNull('accepted_date')
            ->first()
        ;
    }
}
