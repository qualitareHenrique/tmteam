<?php

namespace App\Core\Models;

use App\Core\Models\Concerns\SubsidiaryScope;
use App\Core\Models\TrainingCenter\Classroom;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * Class Message
 *
 * @package App\Core\Models
 * @property string $subject
 * @property string $body
 * @property \DateTime $expiration
 * @property-read \DateTime $created_at
 * @property int $id
 * @property int $subsidiary_id
 * @property int $classroom_id
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message nonExpired()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message whereExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Models\Message whereSubsidiaryId($value)
 * @mixin \Eloquent
 */
class Message extends Model implements AuditableInterface
{
    use Auditable;

    protected $fillable = [
        'subject',
        'body',
        'expiration',
        'classroom_id',
    ];

    protected $casts = [
        'expiration' => 'datetime',
        'created_at' => 'datetime'
    ];

    protected $auditInclude = [
        'subject',
        'body',
        'expiration',
        'subsidiary_id',
        'classroom_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SubsidiaryScope);

        static::creating(function($model){
            $model->subsidiary_id = Auth::user()->subsidiary_id;
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Scopes
    |--------------------------------------------------------------------------
    */
    public function scopeNonExpired($query)
    {
        return $query->with('classroom')->where('expiration', '>=', date('Y-m-d H:i:s', time()))
                    ->orderBy('created_at', 'desc');
    }

    public function classroom()
    {
        return $this->belongsTo(Classroom::class, 'classroom_id');
    }

}
