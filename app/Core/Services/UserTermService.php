<?php

namespace App\Core\Services;

use App\Core\Models\UserTerm;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class UserTermService
{
    private UserTerm $model;

    /**
     * @param UserTerm $model
     */
    public function __construct(UserTerm $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $user
     * 
     * @return bool
     */
    public function hasTermAccepted(int $user): bool
    {
//        return (bool) UserTerm::whereAcceptedByUser($user);

        return true;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function studentNot(Request $request): bool
    {
        return Auth::user()->hasRole(['aluno', 'responsavel'])
            && !$this->acceptAllTermsStudent($request);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function teacherNot(Request $request): bool
    {
        return Auth::user()->hasRole(['professor'])
            && !$this->acceptAllTermsTeacher($request);
    }

    public function userNot(Request $request): bool
    {
        return
            (!Auth::user()->hasRole(['professor']) && !Auth::user()->hasRole(['aluno', 'responsavel']))
            && !$this->acceptAllTerms($request)
        ;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function acceptAllTermsStudent(Request $request): bool
    {
        if (
            !$request->get('padrao_kimonos')
            || !$request->get('atestado_medico')
            || !$request->get('termo_de_uso')
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function acceptAllTermsTeacher(Request $request): bool
    {
        if (!$request->get('termo_de_uso')) {
            return false;
        }

        return true;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function acceptAllTerms(Request $request): bool
    {
        if (!$request->get('termo_de_uso')) {
            return false;
        }

        return true;
    }

    public function accept()
    {
        $user                    = Auth()->user();
        $userTerm                = $this->model->make();
        $userTerm->user_id       = $user->id;
        $userTerm->accepted_date = Carbon::now();
        $userTerm->save();
    }
}
