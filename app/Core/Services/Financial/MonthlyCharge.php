<?php


namespace App\Core\Services\Financial;


use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\TrainingCenter\Registration;
use App\Enums\BalanceRecordTypeEnum;

class MonthlyCharge
{

    /**
     * @param string $competence
     *
     * @return array
     */
    public function generate(string $competence)
    {
        $registrations = Registration::notCancelled()->get();
        $installments  = [];

        foreach ($registrations as $registration) {
            try {
                $installments[] = $this->makeCharge($registration, $competence);
            } catch (\Exception $ex) {
                continue;
            }
        }

        return $installments;
    }

    /**
     * @param \App\Core\Models\TrainingCenter\Registration $registration
     * @param string $competence
     *
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    private function makeCharge(Registration $registration, string $competence)
    {
        $records   = $registration->installments()->get();
        $monthYear = date('m/Y', strtotime("$competence-1"));

        $installment                  = new BalanceRecord;
        $installment->description     = "Mensalidade: $monthYear";
        $installment->amount          = $registration->amount;
        $installment->discount_amount = $registration->advance_discount;
        $installment->type            = BalanceRecordTypeEnum::RECIPE;
        $installment->due_date        = "$competence-$registration->payment_day";
        $installment->payment_type    = $registration->payment_type;
        $installment->subsidiary_id   = $registration->subsidiary_id;

        if (empty($installment->amount) ||
            count($records->where('due_date', $installment->due_date))) {
            throw new \Exception('Installment already exists');
        }

        return $registration->installments()->save($installment);
    }

}