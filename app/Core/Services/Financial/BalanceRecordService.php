<?php

namespace App\Core\Services\Financial;

use App\Core\DTOs\BalanceRecordData;
use App\Core\Filters\BalanceRecordFilter;
use App\Core\Models\Auth\User;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\Report\Overdue;
use App\Core\Models\TrainingCenter\Student;
use App\Core\Services\Juno\AccountService;
use App\Core\Services\Juno\ChargeService;
use App\Core\Services\MailService;
use App\Enums\BalanceRecordStatusEnum;
use App\Enums\PaymentTypesEnum;
use App\Enums\ResendTypesEnum;
use App\Enums\SplitTypeEnum;
use App\Notifications\Admin\Invoice;
use Carbon\Carbon;
use DomainException;
use Illuminate\Support\Facades\DB;

class BalanceRecordService
{
    /** @var string */
    public const STUDENTS_RESEND_EMPTY = 'Selecione ao menos um estudante!';

    protected BalanceRecord  $model;
    protected ChargeService  $chargeService;
    protected AccountService $accountService;

    /**
     * BalanceRecordService constructor.
     *
     * @param \App\Core\Models\Financial\BalanceRecord $model
     * @param \App\Core\Services\Juno\ChargeService $chargeService
     * @param \App\Core\Services\Juno\AccountService $accountService
     */
    public function __construct(
        BalanceRecord $model, ChargeService $chargeService, AccountService $accountService
    ) {
        $this->model          = $model;
        $this->chargeService  = $chargeService;
        $this->accountService = $accountService;
    }

    /**
     * @param \App\Core\DTOs\BalanceRecordData $data
     *
     * @return \App\Core\Models\Financial\BalanceRecord|\Illuminate\Database\Eloquent\Model|null
     */
    public function createOrUpdate(BalanceRecordData $data)
    {
        $balanceRecord = $this->model->make();

        if ($data->id) {
            $balanceRecord = $this->find($data->id);
        }

        $balanceRecord->fill(array_filter($data->all()));
        $balanceRecord->save();

        return $balanceRecord->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\Financial\BalanceRecord
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): BalanceRecord
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\BalanceRecordFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\Financial\BalanceRecord[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(BalanceRecordFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @param int $id
     *
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function charge(int $id): void
    {
        DB::beginTransaction();
        $record     = $this->find($id);

        $result = $this->chargeService->create($record);
        $charge = current($result->getCharges());
        $data   = BalanceRecordData::fromChargeResource($charge);

        $record->fill($data->onlyFilled());
        $record->save();
        DB::commit();
    }

    /**
     * @param BalanceRecord $record
     * @param bool|null $cancelRecord
     *
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function cancelGateway(BalanceRecord $record, ?bool $cancelRecord = true): void
    {
        $this->chargeService->cancel($record);

        if ($cancelRecord) {
            $record->gateway_id   = null;
            $record->split_amount = 0;
            $record->charge_link  = null;
            $record->charge_code  = null;
            $record->canceled     = 1;

            $record->save();
        }
    }

    /**
     * @param BalanceRecord $record
     * @param bool|null $cancelGateway
     *
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function manualPayment(BalanceRecord $record, ?bool $cancelGateway = false): void
    {
        $record->pay();
        $record->save();

        if ($record->status === BalanceRecordStatusEnum::BILLED && $cancelGateway) {
            $this->cancelGateway($record, false);
        }
    }

    /**
     * @param array|null $students
     * @param int $type
     */
    public function resendInvoices(int $student, int $type)
    {
        switch ($type) {
            case ResendTypesEnum::WHATSAPP:
                return $this->resendWhatsapp($student);
            case ResendTypesEnum::EMAIL:
                return $this->resendEmail($student);
        }
    }

    /**
     * @param int $student
     */
    public function resendEmail(int $student)
    {
        $studentEntity = Student::whereId($student)->first();
        $user          = $studentEntity->user;
        $overdue       = Overdue::whereStudentId($student);
        $records       = BalanceRecord::whereIdIn(explode(',', $overdue->records));

        $records = $records->filter(function ($record) {
            return !empty($record->gateway_id);
        });

        if ($records->isEmpty()) {
            throw new DomainException("Não existem parcelas faturadas para esse aluno.");
        }

        return $user->notify(new Invoice($records));
    }

    /**
     * @param int $student
     *
     * @return string
     */
    public function resendWhatsapp(int $student): string
    {
        $studentEntity = Student::whereId($student)->first();
        $user          = $studentEntity->user;
        $text          = $this->getWhatsResendText($user);
        $overdue       = Overdue::whereStudentId($student);
        $records       = BalanceRecord::whereIdIn(explode(',', $overdue->records));

        $records = $records->filter(function ($record) {
            return !empty($record->gateway_id);
        });

        if ($records->isEmpty()) {
            throw new DomainException("Não existem parcelas faturadas para esse aluno.");
        }

        $records->map(function ($record) use (&$text) {
            $text .=
                Carbon::createFromFormat('Y-m-d H:i:s', $record->due_date)->format('d/m/Y') .
                ': ' .
                $record->charge_link .
                '%0A %0A';
        });

        return $text;
    }

    /**
     * @param User $user ,,
     *
     *
     * @return string
     */
    public function getWhatsResendText(User $user): string
    {
        return "Olá, $user->nickname. %0A %0A Segue a segunda via dos seus boletos. %0A %0A";
    }

    /**
     * @param array $data
     * @param BalanceRecord $record
     *
     * @return bool
     */
    public function isManualPayment(array $data, BalanceRecord $record): bool
    {
        return $data['payment_date'] && !$record->payment_date;
    }

    /**
     * @param array $data
     * @param BalanceRecord $record
     *
     * @return void
     */
    public function checkCancelGateway(array $data, BalanceRecord $record): void
    {
        if (PaymentTypesEnum::INVOICE === (int)$record->payment_type && $record->gateway_id) {
            $this->cancelGateway($record, false);
        }
    }
}
