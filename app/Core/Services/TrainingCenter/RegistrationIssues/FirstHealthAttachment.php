<?php


namespace App\Core\Services\TrainingCenter\RegistrationIssues;

use App\Core\Models\TrainingCenter\Registration;
use App\Enums\RegistrationStatusEnum;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\Models\Media;

class FirstHealthAttachment implements Issuer
{

    /**
     * @var Registration
     */
    private $registration;

    /**
     * @var Media
     */
    private $healthCertificate;

    public function __construct(Registration $registration)
    {
        $this->registration      = $registration;
        $this->healthCertificate = $registration->fisic;
    }

    private function checkForExpiredFirstAttachment()
    {
        $date = $this->closestDate();

        if (empty($this->healthCertificate) && $date->addDays(30) < Carbon::now()) {
            return true;
        }

        return false;
    }

    private function closestDate()
    {
        $effectiveDate = $this->registration->effective_date;
        $createdAt     = $this->registration->created_at;

        return $effectiveDate > $createdAt ? $effectiveDate : $createdAt;
    }

    public function check(): int
    {
        $result          = RegistrationStatusEnum::REGULAR;
        $firstExpiration = $this->checkForExpiredFirstAttachment();

        if ($firstExpiration) {
            $result = RegistrationStatusEnum::IRREGULAR;
        }

        return $result;
    }

    public function description(): string
    {
        return "Prazo para anexar o atestado médico expirou";
    }
}