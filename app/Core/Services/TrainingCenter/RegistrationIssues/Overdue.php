<?php


namespace App\Core\Services\TrainingCenter\RegistrationIssues;

use App\Enums\RegistrationStatusEnum;
use App\Core\Models\TrainingCenter\Registration;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

class Overdue implements Issuer
{
    /**
     * @var Registration
     */
    private $registration;

    /**
     * @var Carbon
     */
    private $date;

    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
        $this->date = Carbon::now();
    }

    public function check(): int
    {
        $result = RegistrationStatusEnum::REGULAR;
        $today = $this->date;
        $late = $this->getLateInstallments($today);

        if ($late->count() > 0) {
            $result = RegistrationStatusEnum::IRREGULAR;
            $veryLate = $this->getVeryLateInstallments($late, $today);

            if ($veryLate->count() > 0) {
                $result = RegistrationStatusEnum::CANCELLED;
            }
        }

        return $result;
    }

    private function getLateInstallments(Carbon $today): Collection
    {
        $minOverdue = config('settings.min_overdue_days');

        return $this->registration
            ->installments()
            ->where('due_date', '<', $today->format('Y-m-d'))
            ->whereNull('payment_date')
            ->get()
            ->filter(function ($installment, $key) use ($today, $minOverdue) {
                return $today->diffInDays($installment->due_date) > $minOverdue;
            });
    }

    private function getVeryLateInstallments(Collection $late, Carbon $today)
    {
        $maxOverdue = config('settings.max_overdue_days');

        return $late->filter(
            function ($installment, $key) use ($today, $maxOverdue) {
                return $today->diffInDays($installment->due_date) > $maxOverdue;
            });
    }

    public function description(): string
    {
        return "Pagamento em atraso";
    }
}