<?php

namespace App\Core\Services\TrainingCenter;

use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\ClassroomsTeachers;
use App\Core\Models\TrainingCenter\Teacher;
use Illuminate\Http\Request;

class ClassroomTeacherService
{
    /** @var string  */
    public const DUPLICATED = 'this teacher is already inserted in this class';

    protected ClassroomsTeachers $model;
    protected Classroom $classroomModel;

    public function __construct(ClassroomsTeachers $model, Classroom $classroomModel)
    {
        $this->model          = $model;
        $this->classroomModel = $classroomModel;
    }

    /**
     * @param Request $request
     * @return ClassroomsTeachers
     */
    public function create(Request $request): ClassroomsTeachers
    {
        $post = $request->post();
        $post['split_value'] = 0.00;

        $this->validHasTeacher($post);

        $classroomTeacher = new ClassroomsTeachers();
        $classroomTeacher->fill($post);

        $classroomTeacher->save();
        
        return $classroomTeacher;
    }

    /**
     * @param array $post
     */
    public function validHasTeacher(array $post)
    {
        $teacher = $this->model->findByClassroomAndTeacher(
            $post['classroom_id'], 
            $post['teacher_id']
        );

        if ($teacher) {
            throw new \DomainException(self::DUPLICATED);
        }
    }

    /**
     * @param int $classroomTeacher
     * @throws \Exception
     */
    public function remove(int $classroomTeacher): void
    {
        $classroomTeacherEntity = ClassroomsTeachers::whereId($classroomTeacher);
        $classroomTeacherEntity->delete();
    }

    /**
     * @param string $splitValue
     * @param int $classroomTeacher
     * @return \App\Core\Models\Auth\Activation|\Illuminate\Database\Eloquent\Builder
     */
    public function update(string $splitValue, int $classroomTeacher)
    {
        $classroomTeacher = $this->model::whereId($classroomTeacher)->first();
        $classroomTeacher->split_value = $splitValue;
        $classroomTeacher->save();

        return $classroomTeacher;
    }
}