<?php


namespace App\Core\Services\TrainingCenter;

use App\Core\DTOs\TeacherData;
use App\Core\DTOs\UserData;
use App\Core\Filters\TeacherFilter;
use App\Core\Models\TrainingCenter\Teacher;
use App\Core\Services\Auth\UserService;
use Illuminate\Support\Facades\DB;

class TeacherService
{

    protected Teacher     $model;
    protected UserService $userService;

    /**
     * TeacherService constructor.
     *
     * @param \App\Core\Models\TrainingCenter\Teacher $model
     * @param \App\Core\Services\Auth\UserService $userService
     */
    public function __construct(Teacher $model, UserService $userService)
    {
        $this->model       = $model;
        $this->userService = $userService;
    }


    /**
     * @param \App\Core\DTOs\TeacherData $data
     *
     * @return \App\Core\Models\TrainingCenter\Teacher|\Illuminate\Database\Eloquent\Model|null
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function createOrUpdate(TeacherData $data)
    {
        $teacher = $this->model->make();

        try {
            DB::beginTransaction();

            if($data->user){
                $data->user_id = $this->createOrUpdateUserForTeacher($data->user);
            }

            if ($data->id) {
                $teacher = $this->find($data->id);
            }

            $teacher->fill($data->all());
            $teacher->save();
            $teacher->modalities()->sync($data->modalities);

        } catch (\Exception  $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return $teacher->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\TrainingCenter\Teacher
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): Teacher
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\TeacherFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\TrainingCenter\Teacher[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(TeacherFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @param \App\Core\DTOs\UserData $userData
     *
     * @return int|mixed
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    protected function createOrUpdateUserForTeacher(UserData $userData)
    {
        $user = $this->userService->createOrUpdate($userData);

        if(!$user->isTeacher()){
            $user->assignRole('professor');
        }

        return $user->id;
    }
}