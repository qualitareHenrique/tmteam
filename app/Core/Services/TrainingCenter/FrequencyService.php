<?php


namespace App\Core\Services\TrainingCenter;


use App\Core\DTOs\FrequencyData;
use App\Core\Filters\FrequencyFilter;
use App\Core\Models\TrainingCenter\Frequency;
use App\Http\Requests\SaveFrequency;

class FrequencyService
{

    protected Frequency $model;

    /**
     * FrequencyService constructor.
     *
     * @param \App\Core\Models\TrainingCenter\Frequency $model
     */
    public function __construct(Frequency $model)
    {
        $this->model = $model;
    }

    public function createOrUpdate(FrequencyData $data)
    {
        $model = $this->model->make();

        if($data->id){
            $model = $this->find($data->id);
        }

        $model->fill($data->onlyFilled());
        $model->save();

        return $model->fresh();
    }

    public function createMany(SaveFrequency $request)
    {
        foreach ($request->input('registration_id') as $registrationId){
            $model = $this->model->make();
            $model->fill($request->except('registration_id'));
            $model->registration_id = $registrationId;
            $model->save();
        }
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\TrainingCenter\Frequency
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): Frequency
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\FrequencyFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\TrainingCenter\Guardian[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(FrequencyFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

}
