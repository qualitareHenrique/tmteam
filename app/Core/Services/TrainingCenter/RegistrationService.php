<?php


namespace App\Core\Services\TrainingCenter;

use App\Core\DTOs\RegistrationData;
use App\Core\Models\Financial\BalanceRecord;
use App\Core\Services\TrainingCenter\RegistrationIssues\IssueChecker;
use App\Core\Models\TrainingCenter\Registration;
use App\Core\Services\Financial\BalanceRecordService;
use App\Enums\AttachmentStatusEnum;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;

class RegistrationService
{

    protected Registration $model;

    /** @var BalanceRecordService */
    private BalanceRecordService $recordService;

    /**
     * RegistrationService constructor.
     *
     * @param \App\Core\Models\TrainingCenter\Registration $model
     */
    public function __construct(
        Registration $model,
        BalanceRecordService $balanceRecordService
    ) {
        $this->model         = $model;
        $this->recordService = $balanceRecordService;
    }

    /**
     * @param \App\Core\DTOs\RegistrationData $data
     *
     * @return \App\Core\Models\TrainingCenter\Registration|\Illuminate\Database\Eloquent\Model|null
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function createOrUpdate(RegistrationData $data)
    {

        $registration = $this->model->make();

        if ($data->id) {
            $registration = $this->find($data->id);
        }

        $registration->fill($data->toArray());
        $registration->save();

        if($data->classrooms){
            $registration->classrooms()->sync(
                array_filter($data->classrooms)
            );
        }

        if ($data->fisic) {
            $status = Auth::user()->isAdmin() ?
                AttachmentStatusEnum::VALID :
                AttachmentStatusEnum::PENDING;

            $this->attachHealthCertificate($registration, $data->fisic, $status);
        }

        return $registration->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\TrainingCenter\Registration
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): Registration
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Models\TrainingCenter\Registration $registration
     *
     * @return \App\Core\Models\TrainingCenter\Registration
     */
    public function checkForIssues(Registration $registration)
    {
        $status = (new IssueChecker($registration))->check();

        if ($status != $registration->status) {
            $registration->status = $status;
            $registration->save();
        }

        return $registration->fresh();
    }

    /**
     * @param \App\Core\Models\TrainingCenter\Registration $registration
     * @param \Illuminate\Http\UploadedFile $file
     * @param int $valid
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function attachHealthCertificate(
        Registration $registration,
        UploadedFile $file,
        $valid = AttachmentStatusEnum::PENDING
    ) {
        $name       = $file->getClientOriginalName();
        $extension  = $file->getClientOriginalExtension();
        $expiration = Carbon::now()->addMonths(12)->format('Y-m-d');

        $filename = md5($name) . '.' . $extension;
        $registration->addMedia($file)
                     ->withCustomProperties([
                         'validation' => $valid,
                         'start_date' => Carbon::now(),
                         'expiration' => $expiration
                     ])->usingFileName($filename)
                     ->toMediaCollection('fisic');
    }

    /**
     * @param Registration $registration
     */
    public function deactivateInstallments(Registration $registration): void
    {
        $now = Carbon::now();

        $registration->installments->map(function ($installment) use ($now) {
            /** @var BalanceRecord $installment */
            if ($installment->gateway_id) {
                $this->recordService->cancelGateway($installment);
            }

            if ($installment->due_date > $now && !$installment->payment_date) {
                $installment->delete();

                return null;
            }

            $installment->cancel();
            $installment->save();
        });
    }
}
