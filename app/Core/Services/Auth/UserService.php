<?php


namespace App\Core\Services\Auth;

use App\Core\DTOs\UserData;
use App\Core\Filters\UserFilter;
use App\Core\Models\Auth\User;
use App\Enums\AttachmentStatusEnum;
use Auth;
use Illuminate\Http\UploadedFile;

/**
 * Class UserService
 * @package App\Core\Services\Auth
 */
class UserService
{

    private User $model;

    /**
     * UserService constructor.
     *
     * @param \App\Core\Models\Auth\User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param \App\Core\DTOs\UserData $data
     *
     * @return \App\Core\Models\Auth\User|\Illuminate\Database\Eloquent\Model
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function createOrUpdate(UserData $data)
    {
        $user = $this->model->make();

        if ($data->id) {
            $user = $this->find($data->id);
        }

        $user->fill($data->toArray());
        $user->save();

        if($data->roles){
            $user->syncRoles($data->roles);
        }

        if ($data->avatar) {
            $status = Auth::user()->isAdmin() ?
                AttachmentStatusEnum::VALID :
                AttachmentStatusEnum::PENDING;

            $this->attachAvatar($user, $data->avatar, $status);
        }

        return $user->fresh();
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\Auth\User
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): User
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\UserFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\Auth\User[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(UserFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)->bySubsidiary()->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @param \App\Core\Models\Auth\User $user
     * @param \Illuminate\Http\UploadedFile $file
     * @param int $valid
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    protected function attachAvatar(
        User $user,
        UploadedFile $file,
        $valid = AttachmentStatusEnum::VALID
    ) {
        $name      = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $filename = md5($name) . '.' . $extension;
        $user->addMedia($file)
             ->withCustomProperties(['validation' => $valid])
             ->usingFileName($filename)
             ->toMediaCollection('avatar');
    }

    /**
     * @param User $user
     * @param bool|null $active
     * 
     * @return void
     */
    public function updateUserStatus(User $user, ?bool $active): void
    {
        $user->setActiveAttribute((int) $active);
        $user->save();
    }
}