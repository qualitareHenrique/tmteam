<?php


namespace App\Core\Services\Juno\Adapters;

use App\Core\Models\Subsidiary;
use Jetimob\Juno\Lib\Model\LegalRepresentative as JunoLegalRepresentative;

class LegalRepresentative extends JunoLegalRepresentative
{

    public function __construct(Subsidiary $subsidiary)
    {
        $this->name      = $subsidiary->manager->name;
        $this->document  = $subsidiary->manager->cpf;
        $this->birthDate = $subsidiary->manager->birthdate->format('Y-m-d');
    }

}