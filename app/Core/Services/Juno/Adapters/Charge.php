<?php


namespace App\Core\Services\Juno\Adapters;


use App\Core\Models\Financial\BalanceRecord;
use Jetimob\Juno\Lib\Model\Charge as JunoCharge;

class Charge extends JunoCharge
{

    public function __construct(BalanceRecord $record)
    {
        $this->description    = $record->description;
        $this->amount         = $record->amount;
        $this->references     = [strval($record->id)];
        $this->dueDate        = $record->due_date->format('Y-m-d');
        $this->maxOverdueDays = config('settings.max_overdue_days');
        $this->fine           = config('settings.fine');
        $this->interest       = config('settings.interest');

        $this->applyDiscount($record);
    }

    protected function applyDiscount(BalanceRecord $record)
    {
        if(floatval($record->discount_amount) > 0){
            $this->discountAmount = $record->discount_amount;
            $this->discountDays   = $record->discount_days;
        }
    }

}