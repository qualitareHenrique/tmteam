<?php

namespace App\Core\Services\Juno\Adapters;

use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Teacher;
use Jetimob\Juno\Lib\Model\BankAccount as JunoBankAccount;

class BankAccount extends JunoBankAccount
{
    /**
     * @var Subsidiary|null $subsidiary
     * @var Teacher|null $teacher
     */
    public function __construct(
        ?Subsidiary $subsidiary = null,
        ?Teacher $teacher = null
    ) {
        if ($teacher) {
            $this->setBankTeacher($teacher);

            return null;
        }

        $this->setBankSubsidiary($subsidiary);
    }

    
    /**
     * @param Teacher $teacher
     * 
     * @return void
     */
    public function setBankTeacher(Teacher $teacher): void
    {
        $this->accountHolder           = new AccountHolder(null, $teacher);
        $this->bankNumber              = $teacher->bank_number;
        $this->accountNumber           = $teacher->account_number;
        $this->agencyNumber            = $teacher->agency_number;
        $this->accountComplementNumber = $teacher->account_complement_number ?? '';
        $this->accountType             = $teacher->account_type ?? 'CHECKING';
    }

    /**
     * @param Subsidiary $teacher
     * 
     * @return void
     */
    public function setBankSubsidiary(Subsidiary $subsidiary): void
    {
        $this->accountHolder           = new AccountHolder($subsidiary);
        $this->bankNumber              = $subsidiary->bank_number;
        $this->accountNumber           = $subsidiary->account_number;
        $this->agencyNumber            = $subsidiary->agency_number;
        $this->accountComplementNumber = $subsidiary->account_complement_number ?? '';
        $this->accountType             = $subsidiary->account_type ?? 'CHECKING';
    }
}
