<?php

namespace App\Core\Services\Juno\Adapters;

use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Teacher;
use Jetimob\Juno\Lib\Model\AccountHolder as JunoAccountHolder;

class AccountHolder extends JunoAccountHolder
{
    /**
     * @var Subsidiary|null $subsidiary
     * @var Teacher|null $teacher
     */
    public function __construct(
        ?Subsidiary $subsidiary = null,
        ?Teacher $teacher = null
    ) {
        $this->name     = $subsidiary ? $subsidiary->account_holder_name : $teacher->account_holder_name;
        $this->document = $subsidiary ? $subsidiary->account_holder_document : $teacher->account_holder_document;
    }
}