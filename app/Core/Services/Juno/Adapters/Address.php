<?php


namespace App\Core\Services\Juno\Adapters;


use App\Core\Models\Subsidiary;
use Jetimob\Juno\Lib\Model\Address as JunoAddress;

class Address extends JunoAddress
{

    /**
     * Address constructor.
     *
     * @param \App\Core\Models\Subsidiary $subsidiary
     */
    public function __construct(Subsidiary $subsidiary)
    {
        $this->street       = $subsidiary->street;
        $this->number       = $subsidiary->number;
        $this->complement   = $subsidiary->complement ?? '';
        $this->neighborhood = $subsidiary->neighborhood;
        $this->city         = $subsidiary->city;
        $this->state        = $subsidiary->state;
        $this->postCode     = preg_replace('/[^0-9]/', '', $subsidiary->cep);
    }
}