<?php


namespace App\Core\Services\Juno\Adapters;


use App\Core\Models\Auth\User;
use Jetimob\Juno\Lib\Model\Address as JunoAddress;
use Jetimob\Juno\Lib\Model\Billing as JunoBilling;

class Billing extends JunoBilling
{

    public function __construct(User $user)
    {
        $this->name     = $user->name;
        $this->document = $user->cpf;
        $this->email    = $user->email;
        $this->notify   = $this->notifyInProduction();

        $this->address = new JunoAddress();
        $this->address->street = $user->street;
        $this->address->number = $user->number;
        $this->address->complement = $user->complement ?? '';
        $this->address->neighborhood = $user->neighborhood;
        $this->address->city = $user->city;
        $this->address->state = $user->state;
        $this->address->postCode = preg_replace('/[^0-9]/', '', $user->cep);
    }

    protected function notifyInProduction()
    {
        return app()->environment('production');
    }

}
