<?php

namespace App\Core\Services\Juno;

use App\Core\Models\Financial\BalanceRecord;
use App\Core\Models\Financial\ChargeTeacherSplits;
use App\Core\Models\TrainingCenter\Classroom;
use App\Core\Models\TrainingCenter\ClassroomsTeachers;
use App\Core\Services\Juno\Exceptions\InvalidSplitDistribution;
use App\Enums\SplitTypeEnum;
use Illuminate\Support\Collection;
use Jetimob\Juno\Lib\Model\Split;

class SplitService
{
    private const PRECISION = 2;

    private AccountService $accountService;

    private BalanceRecord $record;

    private string $mainToken;

    private string $subToken;

    private bool $subIsMain;

    private string $amountRemainder;

    /**
     * SplitService constructor.
     *
     * @param \App\Core\Services\Juno\AccountService $accountService
     */
    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @param \App\Core\Models\Financial\BalanceRecord $record
     * @param string $mainToken
     * @param string $subToken
     *
     * @return array|null
     */
    public function create(BalanceRecord $record, string $mainToken, string $subToken): ?array
    {
        $this->record          = $record;
        $this->mainToken       = $mainToken;
        $this->subToken        = $subToken;
        $this->subIsMain       = $this->accountService->isMainAccount($record->subsidiary);
        $this->amountRemainder = $this->formatNumber($record->amount);

        return $this->makeSplit();
    }

    private function formatNumber(float $value): string
    {
        return number_format($value, self::PRECISION);
    }

    private function makeSplit(): ?array
    {
        $main     = $this->makeMainSplit();
        $teachers = $this->makeTeacherSplit();
        $sub      = $this->makeSubsidiarySplit();

        $this->setResourceRemainder($main, $sub);

        if (empty($teachers) && empty($sub)) {
            return null;
        }

        return array_values(array_filter(array_merge($teachers, [$main, $sub])));
    }

    private function makeMainSplit(): Split
    {
        $main                 = new Split();
        $main->recipientToken = $this->mainToken;
        $main->amount         = $this->calcSplitAmount();

        $this->subAmountRemainder($main->amount);

        return $main;
    }

    private function makeSubsidiarySplit(): ?Split
    {
        if ($this->subIsMain) {
            return null;
        }

        $sub                 = new Split();
        $sub->recipientToken = $this->subToken;
        $sub->amount         = 0;

        return $sub;
    }

    private function makeTeacherSplit(): array
    {
        $teacherSplits = $this->getTeacherSplits()->flatten();

        return $teacherSplits->map(function ($teacherSplit) {
            if (!empty($teacherSplit->teacher->resource_token) && $teacherSplit->split_value > 0) {
                $split   = new Split();
                $teacher = $teacherSplit->teacher;

                $split->recipientToken = $teacher->resource_token;
                $split->amount         = $teacherSplit->split_value;
                $split->chargeFee      = false;

                $this->subAmountRemainder($split->amount);

                return $split;
            }
        })->toArray();
    }

    private function calcSplitAmount(): float
    {
        if ($this->subIsMain) {
            return 3.00;
        }

        $splitAmount = config('settings.split_amount');

        if (config('settings.split_type') === SplitTypeEnum::AMOUNT) {
            return floatval($splitAmount);
        }

        $recordAmount = $this->formatNumber($this->record->amount);
        $percent      = bcdiv($splitAmount, '100', self::PRECISION);

        return floatval(bcmul($recordAmount, $percent, self::PRECISION));
    }

    private function getTeacherSplits(): ?Collection
    {
        $registration = $this->record->chargeable()->first();

        if (!$registration) {
            return null;
        }

        $classrooms = $registration->classrooms;

        if (!$classrooms) {
            return null;
        }

        return $classrooms->map(function ($classroom) {
            return $this->getTeachersClassrooms($classroom);
        })->filter();
    }

    private function getTeachersClassrooms(Classroom $classroom): Collection
    {
        $teachers = $classroom->teachers;

        return $teachers->map(function ($teacher) use ($classroom) {
            $new              = new ChargeTeacherSplits();
            $teacherClassroom =
                ClassroomsTeachers::findByClassroomAndTeacher($classroom->id, $teacher->id);

            if ($teacherClassroom->split_value <= 0) {
                return null;
            }

            $new->balance_record_id = $this->record->id;
            $new->teacher_id        = $teacherClassroom->teacher_id;
            $new->classroom_id      = $teacherClassroom->classroom_id;
            $new->split_value       = $this->calculateTeacherSplit($teacherClassroom);
            $new->save();

            return $new;
        })->filter();
    }

    private function calculateTeacherSplit(ClassroomsTeachers $teacherClassroom): float
    {
        $percent    = bcdiv($this->amountRemainder, '100', self::PRECISION);
        $splitValue = $this->formatNumber($teacherClassroom->split_value);

        return floatval(bcmul($percent, $splitValue, self::PRECISION));
    }

    private function subAmountRemainder(float $value)
    {
        $subtract = $this->formatNumber($value);
        $result   = floatval(
            bcsub($this->amountRemainder, $subtract, self::PRECISION)
        );

        $this->amountRemainder = $result;
    }

    private function setResourceRemainder(Split $main, ?Split $sub)
    {
        if ($this->amountRemainder < 0) {
            throw new InvalidSplitDistribution();
        }

        $resource                  = $this->subIsMain ? $main : $sub;
        $resource->chargeFee       = true;
        $resource->amountRemainder = true;
        $resource->amount          = bcadd(
            $this->amountRemainder,
            $this->formatNumber($resource->amount),
            self::PRECISION
        );
    }
}
