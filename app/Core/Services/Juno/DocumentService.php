<?php


namespace App\Core\Services\Juno;


use App\Core\Models\Subsidiary;
use App\Core\Services\Juno\Exceptions\ResponseErrorException;
use Illuminate\Http\UploadedFile;
use Jetimob\Juno\Facades\Juno;
use Jetimob\Juno\Lib\Http\Document\DocumentFileUploadRequest;
use Jetimob\Juno\Lib\Http\Document\DocumentFileUploadResponse;
use Jetimob\Juno\Lib\Http\Document\DocumentListRequest;
use Jetimob\Juno\Lib\Http\Document\DocumentListResponse;
use Jetimob\Juno\Lib\Http\ErrorResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class DocumentService
{

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return \Jetimob\Juno\Lib\Http\Document\DocumentListResponse|\Jetimob\Juno\Lib\Http\ErrorResponse|\Jetimob\Juno\Lib\Http\Response
     */
    public function list(Subsidiary $subsidiary): DocumentListResponse
    {
        return Juno::request(new DocumentListRequest(), $subsidiary->resource_token);
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $documentId
     *
     * @return \Jetimob\Juno\Lib\Http\Document\DocumentFileUploadResponse
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function send(Subsidiary $subsidiary, UploadedFile $file, string $documentId
    ): DocumentFileUploadResponse {

        $request          = new DocumentFileUploadRequest();
        $request->id      = $documentId;
        $request->files[] = fopen($this->renameFile($file), 'r');

        $response = Juno::request($request, $subsidiary->resource_token);

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     *
     * @return string
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    private function renameFile(UploadedFile $file): string
    {
        $filePath = $file->getRealPath();
        $newPath  =
            $file->getPath() . "/" .
            $file->getFilename() . "." .
            $file->getClientOriginalExtension();

        if (rename($filePath, $newPath)) {
            return $newPath;
        }

        throw new FileException('Impossible to rename the given file');
    }
}
