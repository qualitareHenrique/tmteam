<?php

namespace App\Core\Services\Juno;

use App\Core\Models\Subsidiary;
use App\Core\Models\TrainingCenter\Teacher;
use App\Core\Services\Juno\Adapters\BankAccount;
use Jetimob\Juno\Facades\Juno;
use Jetimob\Juno\Lib\Http\Balance\BalanceConsultRequest;
use Jetimob\Juno\Lib\Http\ErrorResponse;
use Jetimob\Juno\Lib\Http\Transference\TransferenceRequest;
use Jetimob\Juno\Lib\Model\ErrorDetail;

class AccountBalanceService
{
    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return mixed
     */
    public function fetch(
        ?Subsidiary $subsidiary = null,
        ?Teacher $teacher = null
    ) {
        $token = $subsidiary ? $subsidiary->resource_token : $teacher->resource_token;

        if (empty($token)) {
            throw new \DomainException("Solicite a configuração da sua conta.");
        }

        $result = Juno::request(new BalanceConsultRequest(), $token);

        if ($result instanceof ErrorResponse) {
            $mensagem = $result->getDetails()[0]->getMessage() ?? "Ocorreu um erro durante a solicitação.";

            throw new \DomainException($mensagem);
        }

        return $result;
    }

    /**
     * @var Subsidiary|null $subsidiary
     * @var float $amount
     * @var Teacher|null $teacher
     */
    public function transfer(
        ?Subsidiary $subsidiary = null,
        float $amount,
        ?Teacher $teacher = null
    ) {
        $request     = new TransferenceRequest();
        $bankAccount = new BankAccount(
            $subsidiary ? $subsidiary : null,
            $teacher ? $teacher : null
        );

        $bankAccount->accountHolder = null;
        $request->bankAccount       = $bankAccount;
        $request->name              = $subsidiary ? $subsidiary->name : $teacher->user->name;
        $request->document          = $subsidiary ? $subsidiary->cnpj : $teacher->user->cpf;
        $request->amount            = $amount;

        return Juno::request(
            $request,
            $subsidiary ? $subsidiary->resource_token : $teacher->resource_token
        );
    }

}
