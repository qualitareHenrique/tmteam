<?php

namespace App\Core\Services\Juno;

use App\Core\Models\Financial\BalanceRecord;
use App\Core\Services\Juno\Adapters\Billing;
use App\Core\Services\Juno\Adapters\Charge;
use App\Core\Services\Juno\Exceptions\ResponseErrorException;
use Jetimob\Juno\Facades\Juno;
use Jetimob\Juno\Lib\Http\Charge\ChargeCancelRequest;
use Jetimob\Juno\Lib\Http\Charge\ChargeCancelResponse;
use Jetimob\Juno\Lib\Http\Charge\ChargeConsultRequest;
use Jetimob\Juno\Lib\Http\Charge\ChargeConsultResponse;
use Jetimob\Juno\Lib\Http\Charge\ChargeCreationRequest;
use Jetimob\Juno\Lib\Http\Charge\ChargeCreationResponse;
use Jetimob\Juno\Lib\Http\ErrorResponse;
use Jetimob\Juno\Lib\Model\Payment;
use Jetimob\Juno\Lib\Model\Split;

class ChargeService
{

    /** @var string */
    public const PROFESSOR_SEM_TOKEN = 'Por favor insira o token no professor %s';
    private SplitService $splitService;

    /**
     * ChargeService constructor.
     *
     * @param \App\Core\Services\Juno\SplitService $splitService
     */
    public function __construct(SplitService $splitService)
    {
        $this->splitService = $splitService;
    }

    /**
     * Creates a invoice on gateway
     *
     * @param BalanceRecord $record
     *
     * @return \Jetimob\Juno\Lib\Http\Charge\ChargeCreationResponse
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function create(BalanceRecord $record): ChargeCreationResponse
    {
        $chargeable = $record->chargeable;

        if (!$chargeable) {
            throw new \InvalidArgumentException('The provided record does not contains any chargeable');
        }

        $billing    = new Billing($chargeable->financier->user);
        $charge     = new Charge($record);
        $subsidiary = $record->subsidiary;
        $subToken   = $subsidiary->resource_token;
        $mainToken  = Juno::getConfig('private_token');

        $charge->split = $this->splitService->create($record, $mainToken, $subToken);

        $response = Juno::request(
            new ChargeCreationRequest($charge, $billing),
            $subsidiary->resource_token
        );

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    /**
     * @param \App\Core\Models\Financial\BalanceRecord $record
     *
     * @return \Jetimob\Juno\Lib\Http\Charge\ChargeConsultResponse
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function find(BalanceRecord $record): ChargeConsultResponse
    {
        $subsidiary = $record->subsidiary;

        $response = Juno::request(
            new ChargeConsultRequest($record->gateway_id),
            $subsidiary->resource_token
        );

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    /**
     * @param \App\Core\Models\Financial\BalanceRecord $record
     *
     * @return \Jetimob\Juno\Lib\Http\Charge\ChargeCancelResponse
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function cancel(BalanceRecord $record): ChargeCancelResponse
    {
        $subsidiary = $record->subsidiary;

        $response = Juno::request(
            new ChargeCancelRequest($record->gateway_id),
            $subsidiary->resource_token
        );

        if ($response instanceof ErrorResponse) {
            throw new ResponseErrorException($response);
        }

        return $response;
    }

    public function fetchPaymentDetails(BalanceRecord $record): Payment
    {
        return current($this->find($record)->payments);
    }
}
