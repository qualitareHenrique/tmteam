<?php


namespace App\Core\Services\Juno\V1;


use Carbon\Carbon;
use Jetimob\Juno\Lib\Model\Payment;

class PaymentDetailsResponse extends Payment
{

    public function __construct(object $paymentData)
    {
        $this->id           = $paymentData->payment->id;
        $this->amount       = (float)$paymentData->payment->amount;
        $this->fee          = (float)$paymentData->payment->fee;
        $this->type         = $paymentData->payment->type;
        $this->creditCardId = $paymentData->payment->creditCardId;
        $this->date         = Carbon::createFromFormat('d/m/Y', $paymentData->payment->date)
                                    ->format('Y-m-d');
    }

}