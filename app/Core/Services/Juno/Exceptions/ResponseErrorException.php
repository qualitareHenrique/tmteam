<?php


namespace App\Core\Services\Juno\Exceptions;

use Exception;
use Jetimob\Juno\Lib\Http\ErrorResponse;
use Throwable;

class ResponseErrorException extends Exception
{
    public array $errorBag = [];

    public function __construct(ErrorResponse $errorResponse, $code = 0, Throwable $previous = null)
    {
        $message = 'Houve um falha durante a sincronização dos dados com a Juno';

        $this->errorBag = array_map(function ($item) {
            return $item->getMessage();
        }, $errorResponse->getDetails());

        parent::__construct($message, $code, $previous);
    }
}
