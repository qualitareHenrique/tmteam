<?php


namespace App\Core\Services\Juno\Exceptions;

use Exception;
use Throwable;

class AccountNotVerifiedException extends Exception
{
    public function __construct($message = 'Conta digital ainda não foi verificada', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
