<?php

namespace App\Core\Services\Juno\Exceptions;

use Throwable;

class InvalidSplitDistribution extends \DomainException
{

    /**
     * InvalidSplitDistribution constructor.
     */
    public function __construct(
        $message = "Distribuição de valores inválida",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
