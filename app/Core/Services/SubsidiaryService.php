<?php


namespace App\Core\Services;

use App\Core\DTOs\SubsidiaryData;
use App\Core\Filters\SubsidiaryFilter;
use App\Core\Models\Subsidiary;
use App\Core\Services\Juno\AccountService;
use Auth;
use Illuminate\Http\UploadedFile;

class SubsidiaryService
{

    protected Subsidiary     $model;
    protected AccountService $accountService;

    /**
     * SubsidiaryService constructor.
     *
     * @param \App\Core\Models\Subsidiary $model
     * @param \App\Core\Services\Juno\AccountService $accountService
     */
    public function __construct(Subsidiary $model, AccountService $accountService)
    {
        $this->model          = $model;
        $this->accountService = $accountService;
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     * @param \Illuminate\Http\UploadedFile $file
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    protected function attachAvatar(
        Subsidiary $subsidiary,
        UploadedFile $file
    ) {
        $name      = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $filename = md5($name) . '.' . $extension;
        $subsidiary->addMedia($file)
            ->usingFileName($filename)
            ->toMediaCollection('avatar');
    }

    /**
     * @param \App\Core\DTOs\SubsidiaryData $data
     * @param bool $sync
     *
     * @return \App\Core\Models\Subsidiary|\Illuminate\Database\Eloquent\Model|null
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function createOrUpdate(SubsidiaryData $data, bool $sync = false)
    {
        $subsidiary = $this->model->make();

        if ($data->id) {
            $subsidiary = $this->find($data->id);
        }

        if ($data->avatar) {
            $this->attachAvatar($subsidiary, $data->avatar);
        }

        $subsidiary->fill($data->all());
        $subsidiary->save();

        $fresh = $subsidiary->fresh();

        $this->transferManager($subsidiary);

        if($sync) {
            $this->createOrUpdateAccount($fresh);
        }

        return $fresh;
    }

    /**
     * @param int $id
     *
     * @return \App\Core\Models\Subsidiary
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find(int $id): Subsidiary
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param string $column
     * @param string $value
     *
     * @return \App\Core\Models\Subsidiary|\Illuminate\Database\Eloquent\Model
     */
    public function findBy(string $column, string $value)
    {
        return $this->model->where($column, $value)->firstOrFail();
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param \App\Core\Filters\SubsidiaryFilter $filter
     * @param int|null $perPage
     * @param array|null $appends
     *
     * @return \App\Core\Models\Subsidiary[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function list(SubsidiaryFilter $filter, int $perPage = null, array $appends = null)
    {
        $builder = $this->model->filter($filter)->latest();

        if ($perPage) {
            return $builder->paginate($perPage)->appends($appends);
        }

        return $builder->get();
    }

    /**
     * @return \App\Core\Models\Subsidiary
     */
    public function current() : Subsidiary
    {
        return Auth::user()->subsidiary;
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return bool
     */
    private function transferManager(Subsidiary $subsidiary): bool
    {
        $manager = $subsidiary->manager;

        if (!$manager->hasRole('gestor')) {
            $manager->assignRole('gestor');
        }

        if ($manager->subsidiary_id == $subsidiary->id) {
            return true;
        }

        $manager->subsidiary_id = $subsidiary->id;
        return $manager->save();
    }

    /**
     * @param \App\Core\Models\Subsidiary $subsidiary
     *
     * @return bool
     * @throws \App\Core\Services\Juno\Exceptions\ResponseErrorException
     */
    public function createOrUpdateAccount(Subsidiary $subsidiary): bool
    {
        if(empty($subsidiary->resource_token)){
            $response = $this->accountService->create($subsidiary);
            $subsidiary->resource_token = $response->resourceToken;
            $subsidiary->gateway_id = $response->id;

            return $subsidiary->save();
        }

        $this->accountService->update($subsidiary);

        return true;
    }

}
