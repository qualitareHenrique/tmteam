<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;

class ModalityFilter extends Filter
{
    /**
     * Filter the modality records by the given name value.
     *
     * @param string $value
     * @return Builder
     */
    public function name(string $value)
    {
        return $this->builder->where('name', 'like', "%{$value}%");
    }

    /**
     * Filter the student records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->where('subsidiary_id', $value);
    }
}