<?php


namespace App\Core\Filters;

use App\Core\Models\TrainingCenter\Registration;
use App\Enums\PaymentSituationEnum;
use Illuminate\Database\Eloquent\Builder;

class InvoiceFilter extends Filter
{

    /**
     * Default filters data.
     *
     * @return array
     */
    public function defaultFilters(): array
    {
        return [
            'status' => 0
        ];
    }

    /**
     * Filter the balance records by the given name value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function name(string $value)
    {
        return $this->builder->whereHasMorph('chargeable', [Registration::class],
            function ($query, $type) use ($value) {
                if ($type === Registration::class) {
                    $query->join('students', 'registrations.student_id', '=', 'students.id')
                          ->join('users', 'students.user_id', '=', 'users.id')
                          ->where('users.name', 'like', "%$value%");
                }
            });
    }

    /**
     * Filter the invoice records by the given status value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function status(string $value)
    {
        if ($value == 2) {
            return $this->builder->whereNotNull('payment_date')
                                 ->whereNotNull('charge_code');
        }

        if ($value == 1) {
            return $this->builder->whereNull('payment_date');
        }

        return $this->builder->whereNull('payment_date')
                             ->whereNull('charge_code');
    }

    public function dueDate(string $value)
    {
        return $this->builder->where('due_date', 'like', "%$value%");
    }

    /**
     * Filter the student records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
}