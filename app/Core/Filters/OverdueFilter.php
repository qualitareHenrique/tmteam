<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;

class OverdueFilter extends Filter
{

    /**
     * Filter the user records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
    
    public function status(int $value)
    {
        return $this->builder->where('status', $value);
    }
    
    public function studentName(string $value)
    {
        return $this->builder->where('student_name', 'like', '%' . $value . '%');
    }
}