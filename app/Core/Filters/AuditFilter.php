<?php


namespace App\Core\Filters;

use Illuminate\Database\Eloquent\Builder;

class AuditFilter extends Filter
{

    /**
     * Filter the audits records by the given created_at value.
     *
     * @param string $value
     * @return Builder
     */
    public function createdAt($value)
    {
        return $this->builder->whereDate('created_at', $value);
    }

    /**
     * Filter the audits records by the given user_id value.
     *
     * @param string $value
     * @return Builder
     */
    public function userId($value)
    {
        return $this->builder->where('user_id', $value);
    }

    /**
     * Filter the audits records by the given type value.
     *
     * @param string $value
     * @return Builder
     */
    public function type($value)
    {
        return $this->builder->where('auditable_type', 'like', "%$value%");
    }

    /**
     * Filter the audits records by the given event(action) value.
     *
     * @param string $value
     * @return Builder
     */
    public function event($value){
        return $this->builder->where('event', $value);
    }

}