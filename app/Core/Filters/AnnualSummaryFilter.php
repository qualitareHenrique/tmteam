<?php

namespace App\Core\Filters;

use Illuminate\Database\Eloquent\Builder;

class AnnualSummaryFilter extends Filter
{
    /** @var string */
    private $year;

    /**
     * @param string $value
     * @return Builder
     */
    public function year(string $value)
    {
        $value      = '20'. $value;
        $this->year = $value;

        return $this->builder->where('referring', 'like', '%' . $value . '%');
    }

    /**
     * @param array $value
     * @return Builder
     */
    public function months(array $values)
    {
        $months = [];

        array_map(function ($key, $month) use (&$months) {
            $months[] = $this->year . '-' . $month;
        }, array_keys($values), $values);

        return $this->builder->whereIn('referring', $months);
    }
}