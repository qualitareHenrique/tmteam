<?php


namespace App\Core\Filters;

use App\Core\Models\TrainingCenter\Registration;
use App\Enums\PaymentSituationEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class BalanceRecordFilter extends Filter
{

    /**
     * Filter the balance records by the given competence value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function competence(string $value)
    {
        $date = Carbon::createFromFormat('m/Y', $value)->format('Y-m');
        return $this->builder->where('due_date', 'like', "%$date%");
    }

    /**
     * Filter the balance records by the given situation value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function situation(string $value)
    {
        if ($value == PaymentSituationEnum::PAID) {
            return $this->builder->whereNotNull('payment_date');
        }

        if($value == PaymentSituationEnum::OVERDUE) {
            return $this->builder->whereNull('payment_date')
                                 ->where('due_date', '<', date('Y-m-d', time()));
        }

        return $this->builder->whereNull('payment_date');
    }

    /**
     * Filter the balance records by the given name value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function name(string $value)
    {
        return $this->builder->whereHasMorph('chargeable', [Registration::class],
            function ($query, $type) use ($value) {
                if ($type === Registration::class) {
                    $query->join('students', 'registrations.student_id', '=', 'students.id')
                          ->join('users', 'students.user_id', '=', 'users.id')
                          ->where('users.name', 'like', "%$value%");
                }
            });
    }

    /**
     * Filter the balance records by the given type value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function type(string $value)
    {
        return $this->builder->where('type', $value);
    }

    /**
     * Filter the student records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
}