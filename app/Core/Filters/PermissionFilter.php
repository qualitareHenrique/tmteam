<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;

class PermissionFilter extends Filter
{

    /**
     * Filter the permission records by the given name value.
     *
     * @param string $value
     * @return Builder
     */
    public function name(string $value)
    {
        return $this->builder->where('name', 'like', "%$value%");
    }

    /**
     * Filter the permission records by the given guard value.
     *
     * @param string $value
     * @return Builder
     */
    public function guard(string $value)
    {
        return $this->builder->where('guard_name', 'like', "%$value%");
    }
}