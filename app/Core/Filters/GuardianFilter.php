<?php


namespace App\Core\Filters;

use Auth;
use Illuminate\Database\Eloquent\Builder;

class GuardianFilter extends Filter
{
    /**
     * Default filters data.
     *
     * @return array
     */
    public function defaultFilters(): array
    {
        return [
            'subsidiary_id' => Auth::user()->subsidiary_id
        ];
    }

    /**
     * Filter the audits records by the given name value.
     *
     * @param string $value
     * @return Builder
     */
    public function name(string $value)
    {
        return $this->builder->whereHas('user',
            function ($query) use ($value) {
                $query->where('name', 'LIKE', "%{$value}%");
                $query->orWhere('email', '=', $value);
            })->orderBy('guardians.id', 'DESC');
    }

    /**
     * Filter the audits records by the given status value.
     *
     * @param string $value
     * @return Builder
     */
    public function status(string $value)
    {
        return $this->builder->whereHas('user',
            function ($query) use ($value) {
                $query->where('active', $value);
            });
    }

    /**
     * Filter the student records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->whereHas('user',
            function ($query) use ($value) {
                $query->where('subsidiary_id', $value);
            });
    }
}