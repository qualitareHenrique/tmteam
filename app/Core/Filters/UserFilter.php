<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;

class UserFilter extends Filter
{

    /**
     * Filter the user records by the given email value.
     *
     * @param string $value
     * @return Builder
     */
    public function email(string $value)
    {
        return $this->builder->where('email', 'like', "%$value%");
    }

    /**
     * Filter the user records by the given status value.
     *
     * @param string $value
     * @return Builder
     */
    public function status(string $value)
    {
        return $this->builder->where('active', $value);
    }

    /**
     * Filter the user records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
}