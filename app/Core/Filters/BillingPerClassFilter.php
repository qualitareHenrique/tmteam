<?php


namespace App\Core\Filters;


use Illuminate\Database\Eloquent\Builder;

class BillingPerClassFilter extends Filter
{

    /**
     * Filter the billing per class records by the given classroom value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function classroom(string $value)
    {
        return $this->builder->where('id', $value);
    }

    /**
     * Filter the billing per class records by the given competence month value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function competenceMonth(string $value)
    {
        return $this->builder->where('competence_month', $value);
    }

    /**
     * Filter the user records by the given subsidiary id value.
     *
     * @param string $value
     * @return Builder
     */
    public function subsidiaryId(string $value)
    {
        return $this->builder->where('subsidiary_id', $value);
    }
}