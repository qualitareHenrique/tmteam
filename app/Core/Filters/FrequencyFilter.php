<?php


namespace App\Core\Filters;

use Illuminate\Database\Eloquent\Builder;

class FrequencyFilter extends Filter
{

    /**
     * Filter the frequency records by the given classroom value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function classroom(string $value): Builder
    {
        return $this->builder->where('classroom_id', $value);
    }

    /**
     * Filter the frequency records by the given registration value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function registrationId(string $value): Builder
    {
        return $this->builder->where('registration_id', $value);
    }

    /**
     * Filter the frequency records by the given status value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function status(string $value): Builder
    {
        return $this->builder->where('reviewed', $value);
    }

    /**
     * Filter the frequency records by the given date value.
     *
     * @param string $value
     *
     * @return Builder
     */
    public function date(string $value): Builder
    {
        return $this->builder->where('date', 'like', "%$value%");
    }
}