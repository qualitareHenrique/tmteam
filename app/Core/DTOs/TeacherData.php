<?php


namespace App\Core\DTOs;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TeacherData
 * @package App\Core\DTOs
 */
class TeacherData extends DataTransferObject
{

    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var int|null
     */
    public ?int $user_id;

    /**
     * @var \App\Core\DTOs\UserData|null
     */
    public ?UserData $user;

    /** @var array|null */
    public ?array $modalities;

    /** @var string|null */
    public $resource_token;

    /**
     * @param \Illuminate\Foundation\Http\FormRequest $request
     *
     * @return static
     */
    public static function fromRequest(FormRequest $request): self
    {
        $data = collect($request->validated());
        $user = collect($data->get('user'));

        return new self([
            'id'         => strict_cast('int', $data->get('id')),
            'user_id'    => strict_cast('int', $data->get('user_id')),
            'modalities' => $data->get('modalities'),
            'gateway_status' => $data->get('gateway_status'),
            'resource_token' => $data->get('resource_token'),
            'agency_number' => $data->get('agency_number'),
            'user'       => array_merge($data->get('user'), [
                'id'               => strict_cast('int', $data->get('user_id')),
                'name'             => $user->get('name'),
                'email'            => $user->get('email'),
                'phone'            => $user->get('phone'),
                'birthdate'        => strict_cast('date', $user->get('birthdate')),
                'sex'              => strict_cast('int', $user->get('sex')),
                'cep'              => $user->get('cep'),
                'street'           => $user->get('street'),
                'number'           => $user->get('number'),
                'neighborhood'     => $user->get('neighborhood'),
                'complement'       => $user->get('complement'),
                'city'             => $user->get('city'),
                'state'            => $user->get('state'),
                'password'         => $user->get('password'),
                'subsidiary_id'    => strict_cast('int', $user->get('subsidiary_id')),
                'avatar'           => $user->get('avatar')
            ])
        ]);
    }

}