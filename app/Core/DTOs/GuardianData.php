<?php

namespace App\Core\DTOs;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GuardianData
 * @package App\Core\DTOs
 */
class GuardianData extends DataTransferObject
{

    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var int|null
     */
    public ?int $user_id;

    /**
     * @var \App\Core\DTOs\UserData|null
     */
    public ?UserData $user;

    /**
     * @param \Illuminate\Foundation\Http\FormRequest $request
     *
     * @return static
     */
    public static function fromRequest(FormRequest $request): self
    {
        $data = collect($request->validated());
        $user = collect($data->get('user'));

        return new self([
            'id'         => strict_cast('int', $data->get('id')),
            'user_id'    => strict_cast('int', $data->get('user_id')),
            'user'       => array_merge($data->get('user'), [
                'id'               => strict_cast('int', $data->get('user_id')),
                'name'             => $user->get('name'),
                'email'            => $user->get('email'),
                'phone'            => $user->get('phone'),
                'birthdate'        => strict_cast('date', $user->get('birthdate')),
                'sex'              => strict_cast('int', $user->get('sex')),
                'cep'              => $user->get('cep'),
                'street'           => $user->get('street'),
                'number'           => $user->get('number'),
                'neighborhood'     => $user->get('neighborhood'),
                'complement'       => $user->get('complement'),
                'city'             => $user->get('city'),
                'state'            => $user->get('state'),
                'password'         => $user->get('password'),
                'subsidiary_id'    => strict_cast('int', $user->get('subsidiary_id')),
                'avatar'           => $user->get('avatar')
            ])
        ]);
    }

}