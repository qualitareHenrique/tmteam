<?php


namespace App\Core\DTOs;

use App\Core\Models\TrainingCenter\Student;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

/**
 * Class RegistrationData
 * @package App\Core\DTOs
 */
class RegistrationData extends DataTransferObject
{

    /**
     * @var int|null
     */
    public ?int $id;

    /**
     * @var \DateTime|null
     */
    public ?\DateTime $effective_date;

    /**
     * @var float|null
     */
    public ?float $acquisition;

    /**
     * @var float|null
     */
    public ?float $amount;

    /**
     * @var float|null
     */
    public ?float $advance_discount;

    /**
     * @var int|null
     */
    public ?int $payment_type;

    /**
     * @var int|null
     */
    public ?int $payment_day;

    /**
     * @var int|null
     */
    public ?int $status;

    /**
     * @var int|null
     */
    public ?int $student_id;

    /**
     * @var int|null
     */
    public ?int $subsidiary_id;

    /**
     * @var \Illuminate\Http\UploadedFile|null
     */
    public ?UploadedFile $fisic;

    /**
     * @var array|null
     */
    public ?array $classrooms;

    /**
     * @param Request $request
     * @param Student $student
     * 
     * @return $this
     */
    public function fromRequest(Request $request, Student $student): self
    {
        $registration = $request->get('registration');

        $this->effective_date   = strict_cast('date', $registration['effective_date']);
        $this->acquisition      = strict_cast('float', $registration['acquisition']);
        $this->amount           = strict_cast('float', $registration['amount']);
        $this->advance_discount = strict_cast('float', $registration['advance_discount']);
        $this->payment_type     = strict_cast('int', $registration['payment_type']);
        $this->payment_day      = strict_cast('int', $registration['payment_day']);
        $this->status           = strict_cast('int', $registration['status'] ?? 1);
        $this->subsidiary_id    = strict_cast('int', $registration['subsidiary_id']);
        $this->student_id       = $student->id;
        $this->classrooms       = $registration['classrooms'];

        return $this;
    }
}
