<?php

/*
|--------------------------------------------------------------------------
| View helpers
|--------------------------------------------------------------------------
|
| This file is the global helper of application. This functions can be used
| on application, but is highly recommended to use only on views.
|
*/

/**
 * Enums as options
 */
if (!function_exists('enumOptions')) {
    function enumOptions($enum)
    {
        switch ($enum) {
            case 'balance_record':
                return \App\Enums\BalanceRecordTypeEnum::options();
            case 'payment_type':
                return \App\Enums\PaymentTypesEnum::options();
            case 'registration_status':
                return \App\Enums\RegistrationStatusEnum::options();
            case 'period_types' :
                return \App\Enums\PeriodTypesEnum::options();
            case 'payment_situation':
                return \App\Enums\PaymentSituationEnum::options();
            default:
                return [];
        }
    }
}

/**
 * Active status
 */
if (!function_exists('isActiveBadge')) {
    function isActiveBadge($status)
    {
        switch ($status) {
            case 0:
                return '<span class="badge badge-danger -status">Desativado</span>';
            case 1:
                return '<span class="badge badge-success -status">Ativo</span>';
            default:
                return '';
        }
    }
}

/**
 * Review status
 */
if (!function_exists('isReviewedBadge')) {
    function isReviewedBadge($status)
    {
        switch ($status) {
            case 0:
                return '<span class="badge badge-pill badge-danger">Pendente</span>';
            case 1:
                return '<span class="badge badge-pill badge-success">Revisado</span>';
            default:
                return '';
        }
    }
}

/**
 * Paid status
 */
if (!function_exists('invoiceStatusBadge')) {
    function invoiceStatusBadge($status)
    {
        switch ($status) {
            case 1:
                return '<span class="badge badge-secondary">Criado</span>';
            case 2:
                return '<span class="badge badge-info">Faturado</span>';
            case 3:
                return '<span class="badge badge-success">Pago</span>';
            case 4:
                return '<span class="badge badge-danger">Atrasado</span>';
            case 5:
                return '<span class="badge badge-primary">Cancelado</span>';
            default:
                return '';
        }
    }
}

/**
 * Registration status
 */
if (!function_exists('registrationStatusBadge')) {
    function registrationStatusBadge($status)
    {
        switch ($status) {
            case 1:
                return '<span class="badge badge-success">Regular</span>';
            case 2:
                return '<span class="badge badge-warning">Pendência</span>';
            case 3:
                return '<span class="badge badge-danger">Cancelado</span>';
            default:
                return '<span class="badge badge-info">Não matriculado</span>';
        }
    }
}

/**
 * Released status
 */
if (!function_exists('isFrequencyReviewedBadge')) {
    function isFrequencyReviewedBadge($status)
    {
        switch ($status) {
            case 0:
                return '<span class="badge badge-danger">Não</span>';
            case 1:
                return '<span class="badge badge-success">Sim</span>';
            default:
                return '';
        }
    }
}

if (!function_exists('mediaValidationClass')) {
    function mediaValidationClass($status)
    {
        switch ($status) {
            case 3:
                return 'badge-success';
            case 2:
                return 'badge-danger';
            default:
                return 'badge-warning text-dark';
        }
    }
}

/**
 * Installment payment situation class
 */
if (!function_exists('installmentPaymentBadge')) {
    function installmentPaymentBadge($installment)
    {
        $today = \Carbon\Carbon::today();
        $today->setTime(0, 0, 0, 0);

        if (!empty($installment->payment_date)) {
            return '<span class="badge badge-success">Pago</span>';
        }

        if (empty($installment->payment_date)) {
            if ($today <= $installment->due_date) {
                return '<span class="badge badge-warning">Pendente</span>';
            }

            return '<span class="badge badge-danger">Atrasado</span>';
        }

        return '';
    }
}

/**
 * Set mask in string
 */
if (!function_exists('mask')) {
    function mask($mask, $value)
    {
        for ($i = 0; $i < strlen($value); $i++) {
            $mask[ strpos($mask, '#') ] = $value[ $i ];
        }

        return $mask;
    }
}

/**
 * Set mask phone
 */
if (!function_exists('maskPhone')) {
    function maskPhone($phone)
    {
        if (strlen($phone) === 10) {
            $phone = mask('(##) ####-####', $phone);
        } elseif (strlen($phone) === 11) {
            $phone = mask('(##) #####-####', $phone);
        }

        return $phone;
    }
}

/**
 * Set mask cep
 */
if (!function_exists('maskCep')) {
    function maskCep($cep)
    {
        return mask('#####-###', $cep);;
    }
}

/**
 * Transform age categories in data attributes
 */
if (!function_exists('ageCategoryToDataAttr')) {
    function ageCategoryToDataAttr($ageCategories)
    {
        $ageData = [];

        foreach ($ageCategories as $category) {
            $ageData[ $category->id ] = [
                'data-age-begin'  => $category->age_begin,
                'data-age-finish' => $category->age_finish
            ];
        }

        return $ageData;
    }
}

/**
 * Transform classrooms in data attributes
 */
if (!function_exists('classroomsToDataAttr')) {
    function classroomsToDataAttr($classrooms)
    {
        $data = [];

        foreach ($classrooms as $classroom) {
            $data[ $classroom->id ] = [
                'data-value'      => $classroom->modality->price,
                'data-subsidiary' => $classroom->subsidiary_id
            ];
        }

        return $data;
    }
}

/**
 * Transform bands in data attributes
 */
if (!function_exists('bandsToDataAttribute')) {
    function bandsToDataAttribute($bands)
    {
        $data = [];

        foreach ($bands as $band) {
            $data[ $band->id ] = [
                'data-modality' => $band->modality_id,
            ];
        }

        return $data;
    }
}

/**
 * Transform degrees in data attributes
 */
if (!function_exists('degreesToDataAttribute')) {
    function degreesToDataAttribute($degrees)
    {
        $data = [];

        foreach ($degrees as $degree) {
            $data[ $degree->id ] = [
                'data-band' => $degree->band_id,
            ];
        }

        return $data;
    }
}

/**
 * Color list available for bands
 */
if (!function_exists('colorOptions')) {
    function colorOptions()
    {
        return [
            1  => 'Branca',
            2  => 'Cinza',
            3  => 'Preta',
            4  => 'Amarela',
            5  => 'Marrom',
            6  => 'Laranja',
            7  => 'Verde',
            8  => 'Azul',
            9  => 'Roxa',
            10 => 'Coral',
            11 => 'Vermelha'
        ];
    }
}

if(!function_exists('activeTree')){
    function activeTree($tree){
        return array_search(
                Route::currentRouteName(),
                array_column($tree['options'], 'route')
            ) !== FALSE;
    }
}
