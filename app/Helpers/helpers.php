<?php

/*
|--------------------------------------------------------------------------
| View helpers
|--------------------------------------------------------------------------
|
| This file is the global helper of application.
|
*/

/**
 * Cast variables but keep NULL as valid return
 *
 * @param mixed $var
 * @param string $type
 *
 * @return mixed
 */
if(!function_exists('strict_cast')){
    function strict_cast(string $type, $var){
        if($var === NULL){
            return NULL;
        }

        if($type === 'datetime' || $type === 'date'){
            return new DateTime($var);
        }

        settype($var, $type);
        return $var;
    }
}

/**
 * Exactly the same as array_filter except this function
 * filters within multi-dimensional arrays
 *
 * @param array
 * @param string optional callback function name
 * @return array Filtered array
 */
if (!function_exists('array_filter_recursive')) {
    function array_filter_recursive($array)
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = array_filter_recursive($value);
            }
        }

        return array_filter($array);
    }
}

/**
 * Months as options
 */
if (!function_exists('months')) {
    function months()
    {
        return [
            1  => 'Janeiro',
            2  => 'Fevereiro',
            3  => 'Março',
            4  => 'Abril',
            5  => 'Maio',
            6  => 'Junho',
            7  => 'Julho',
            8  => 'Agosto',
            9  => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        ];
    }
}




