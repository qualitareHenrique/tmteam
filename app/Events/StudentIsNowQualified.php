<?php

namespace App\Events;

use App\Core\Models\TrainingCenter\Graduation;
use App\Core\Models\TrainingCenter\Registration;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class StudentIsNowQualified
{
    use Dispatchable, SerializesModels;

    public $registration;
    public $graduation;

    /**
     * Create a new event instance.
     *
     * @param Registration $registration
     * @param Graduation $graduation
     */
    public function __construct(Registration $registration, Graduation $graduation)
    {
        $this->registration = $registration;
        $this->graduation = $graduation;
    }


}
