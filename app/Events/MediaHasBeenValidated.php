<?php

namespace App\Events;

use App\Core\Models\Media;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class MediaHasBeenValidated
{
    use Dispatchable, SerializesModels;

    public $media;

    /**
     * Create a new event instance.
     *
     * @param Media $media
     */
    public function __construct(Media $media)
    {
        $this->media = $media;
    }
}
